<?php

// SPDX-FileCopyrightText: 2021 - 2022 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

/**
 * This file registers routes related to user profiles:
 * - registration
 * - authentication
 * - email verification
 * - managing profile (by user)
 * - password resets (forgot password)
 * - change password
 * - setup Multi Factor Authentication (MFA)
 * - deletion of other login sessions
 * - profile administration
 *
 */

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Profile\RegistrationController;
use App\Http\Controllers\Profile\AuthenticationController;
use App\Http\Controllers\Profile\EmailController;
use App\Http\Controllers\Profile\PasswordResetController;
use App\Http\Controllers\Profile\ProfileController;
use App\Http\Controllers\Profile\MfaController;
use App\Http\Controllers\Profile\UserController;

/**
 * Registration
 */
Route::group(['middleware' => 'guest', 'prefix' => 'profiel', 'as' => 'profile.'], function () {
	Route::get('/aanmelden', [RegistrationController::class, 'create'])->name('register');
	Route::post('/aanmelden', [RegistrationController::class, 'store'])->name('register.store')
		->middleware('throttle:registration');
});

/**
 * Authentication
 */
Route::group(['middleware' => 'guest', 'prefix' => 'profiel', 'as' => 'profile.'], function () {
	Route::get('/inloggen', [AuthenticationController::class, 'create'])->name('login');
	Route::post('/inloggen', [AuthenticationController::class, 'store'])->name('login.store')
		->middleware('throttle:login');
});
Route::group(['middleware' => 'auth', 'prefix' => 'profiel', 'as' => 'profile.'], function () {
	Route::delete('/uitloggen', [AuthenticationController::class, 'destroy'])->name('logout');

	Route::get('/bevestig', [AuthenticationController::class, 'confirmation'])->name('password.confirm');
	Route::post('/bevestig', [AuthenticationController::class, 'confirm'])->name('password.confirm.store')
		->middleware('throttle:confirm');
});

/**
 * email verification
 */
Route::group(['middleware' => 'auth', 'prefix' => 'profiel/email', 'as' => 'profile.verification.'], function () {
	Route::get('/controle', [EmailController::class, 'showNotice'])->name('notice');
	Route::middleware('throttle:verify')->group(function () {
		Route::get('/controle/{id}/{hash}', [EmailController::class, 'verify'])->name('verify')
			->middleware('signed');
		Route::post('/controleren', [EmailController::class, 'send'])->name('send');
	});
});

/**
 * managing profile (by user)
 */
Route::group(['prefix' => 'profiel', 'as' => 'profile.'], function () {
	Route::get('/', [ProfileController::class, 'show'])->name('show')
		->middleware('auth');
	Route::delete('/{userSession}/uitloggen', [ProfileController::class, 'destroyUserSession'])->name('session.delete')
		->middleware('user');
	Route::delete('/verwijderen', [RegistrationController::class, 'destroy'])->name('delete')
		->middleware('auth');
});

/**
 * password resets
 */
Route::put('/profiel/wachtwoord', [ProfileController::class, 'updatePassword'])->name('profile.password.update')
	->middleware('auth');
Route::group([
	'prefix' => 'profiel/wachtwoord',
	'as' => 'profile.password.reset.',
	'middleware' => 'guest'
], function () {
	Route::get('/aanvragen', [PasswordResetController::class, 'create'])->name('create');
	Route::get('/instellen/{token}', [PasswordResetController::class, 'edit'])->name('edit');
	// Route::group(['middleware' => 'throttle:login'], function () {
		Route::post('/aanvragen', [PasswordResetController::class, 'store'])->name('store');
		Route::post('/instellen', [PasswordResetController::class, 'update'])->name('update');
	// });
});

/**
 * change password
 */

/**
 * setup Multi Factor Authentication (MFA)
 */

// // Two Factor Authentication
// Route::group(['prefix' => 'profiel/mfa', 'as' => 'mfa.'], function () {
// 	Route::get('/controle', [MfaController::class, 'create'])->name('login')
// 		->middleware('guest');
// 	Route::post('/controle', [MfaController::class, 'store'])->name('login2')
// 		->middleware(['guest', 'throttle:mfa']);
// 	Route::middleware(['auth', 'password.confirm'])->group(function () {
// 		Route::post('/authentication', [MfaController::class, 'store'])->name('auth.store');
// 		Route::delete('/authentication', [MfaController::class, 'destroy'])->name('auth.destroy');
// 		Route::get('/qr-code', [MfaController::class, 'show'])->name('qr');
// 		Route::get('/noodcodes', [MfaController::class, 'index'])->name('recovery-codes.index');
// 		Route::post('/noodcodes', [MfaController::class, 'store'])->name('recovery-codes.store');
// 	});
// });


/**
 * deletion of other login sessions
 */

/**
 * profile administration
 */
Route::resource('profielen', UserController::class, ['names' => 'profiles'])
	->parameters(['profielen' => 'user'])
	->middleware('user');
