<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

use App\Http\Controllers\GuestController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dashboard', [UserController::class, 'index'])->name('user.home')->middleware('user');
require_once(__DIR__ . "/web.profile.php");
require_once(__DIR__ . "/web.cms.php");
require_once(__DIR__ . "/web.guest_forms.php");

Route::get('downloads/{filename}', [GuestController::class, 'download'])->name('download');

Route::get('/{slug?}', [GuestController::class, 'show'])->where('slug', '.*');
