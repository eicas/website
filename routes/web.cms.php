<?php

// SPDX-FileCopyrightText: 2021 - 2022 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

/**
 * This file registers routes related to the Content Management System:
 * - images
 *
 */

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Cms\CmsController;
use App\Http\Controllers\Cms\DatalimitController;
use App\Http\Controllers\Cms\DownloadController;
use App\Http\Controllers\Cms\ImageController;
use App\Http\Controllers\Cms\ImagePickerController;
use App\Http\Controllers\Cms\ImagesetController;
use App\Http\Controllers\Cms\MenuitemController;
use App\Http\Controllers\Cms\MultiImagePickerController;
use App\Http\Controllers\Cms\PageController;
use App\Http\Controllers\Cms\RedirectController;
use App\Http\Controllers\Cms\SwapMenuitemController;
use App\Http\Controllers\Cms\Imagesets\ImageImagesetController;
use App\Http\Controllers\Cms\Imagesets\SwapImageImagesetController;
use App\Http\Controllers\Cms\Pages\ContentController;
use App\Http\Controllers\Cms\Pages\SwapContentController;

Route::group(['middleware' => 'user', 'prefix' => 'cms', 'as' => 'cms.'], function () {
	Route::get('/', [CmsController::class, 'index'])->name('index');

	Route::resource('images', ImageController::class)->except(['show']);

	Route::resource('pages', PageController::class)->except(['create']);

	Route::resource('imagesets', ImagesetController::class)->except(['create']);
	Route::group(['middleware' => 'can:admin,imageset'], function () {
		Route::post('imagesets/{imageset}', [ImageImagesetController::class, 'store'])
			->name('imagesets.imageimagesets.store');
		Route::get('imagesets/{imageset}/{imageimageset}', [ImageImagesetController::class, 'edit'])
			->name('imagesets.imageimagesets.edit');
		Route::patch('imagesets/{imageset}/{imageimageset}', [ImageImagesetController::class, 'update'])
			->name('imagesets.imageimagesets.update');
		Route::delete('imagesets/{imageset}/{imageimageset}', [ImageImagesetController::class, 'destroy'])
			->name('imagesets.imageimagesets.destroy');
		Route::post('imagesets/{imageset}/swap/{imageimageset1}/{imageimageset2}', SwapImageImagesetController::class)
			->name('imagesets.imageimagesets.swap');
	});

	Route::get('image_picker', [ImagePickerController::class, 'index'])->name('imagepicker.index');
	Route::get('multi_image_picker', [MultiImagePickerController::class, 'index'])->name('multiimagepicker.index');

	Route::post('pages/{page}/contents/{content1}/{content2}', SwapContentController::class)
		->middleware('can:update,page')->name('pages.contents.swap');
	Route::patch('pages/{page}/contents/{content}/restore', [ContentController::class, 'restore'])
		->middleware('can:update,page')->name('pages.contents.restore');
	Route::resource('pages.contents', ContentController::class)->except(['create', 'show', 'edit'])
		->middleware('can:update,page');

	Route::post('menuitems/swap/{menuitem1}/{menuitem2}', SwapMenuitemController::class)
		->middleware('can:admin,' . \App\Models\Menuitem::class)->name('menuitems.swap');
	Route::resource('menuitems', MenuitemController::class)->except(['create', 'show', 'edit']);

	Route::resource('downloads', DownloadController::class)->except(['create', 'edit']);

	Route::resource('redirects', RedirectController::class)->except(['create', 'edit', 'show']);

	Route::get('/datalimit', DatalimitController::class)->name('datalimit');
});

