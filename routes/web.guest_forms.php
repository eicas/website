<?php

// SPDX-FileCopyrightText: 2023 Arnout Engelen <arnout@bzzt.net>
// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

/**
 * This file registers routes related to Guest Forms
 *
 */

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GuestForms\NewFriendController;

Route::group(['as' => 'guest_forms.'], function () {
	// user side
	Route::post('/guest_forms/new_friend', [NewFriendController::class, 'store'])->name('new_friend.store');
	Route::post('/webhooks/guest_forms/new_friend/mollie', [NewFriendController::class, 'webhook'])->name('new_friend.webhook');

	// admin side
	Route::group(['middleware' => 'user'], function () {
		Route::get('/guest_forms/new_friend', [NewFriendController::class, 'index'])->name('new_friend.index')
			->middleware('can:admin,' . \App\Models\GuestForms\NewFriend::class);
	});
});
