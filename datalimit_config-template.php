<?php

// SPDX-FileCopyrightText: 2022 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

// make sure to provide reasonable values here on the server (not in source control)
$db_name = '';
$db_user = '';
$db_pass = '';

// observe $period seconds on each run
// 900s = 15 min
$period = 900;
// suspend if data served during period exceeds $max_data bytes
//   50 MB / 15min
//  200 MB / hour
// 4800 MB / day
// 14.4 GB / month
$max_bytes = 20000000;
// if the site is suspended, unsuspend it after $suspend_duration seconds
$suspend_duration = 900;
