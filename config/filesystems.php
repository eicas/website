<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
// SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>
//
// SPDX-License-Identifier: MIT

return [

	/*
	|--------------------------------------------------------------------------
	| Default Filesystem Disk
	|--------------------------------------------------------------------------
	|
	| Here you may specify the default filesystem disk that should be used
	| by the framework. The "local" disk, as well as a variety of cloud
	| based disks are available to your application. Just store away!
	|
	*/

	'default' => env('FILESYSTEM_DRIVER', 'images'),

	/*
	|--------------------------------------------------------------------------
	| Filesystem Disks
	|--------------------------------------------------------------------------
	|
	| Here you may configure as many filesystem "disks" as you wish, and you
	| may even configure multiple disks of the same driver. Defaults have
	| been setup for each driver as an example of the required options.
	|
	| Supported Drivers: "local", "ftp", "sftp", "s3"
	|
	*/

	'disks' => [

		'images' => [
			'driver' => 'local',
			'root' => public_path('img/images'),
			'url' => env('APP_URL') . '/img/images',
			'visibility' => 'public',
		],

		'templates' => [
			'driver' => 'local',
			'root' => resource_path('views/templates'),
		],

		'downloads' => [
			'driver' => 'local',
			'root' => storage_path('app/downloads'),
		],

		'youtube_preview' => [
			'driver' => 'local',
			'root' => public_path('img/youtube_preview'),
			'url' => env('APP_URL') . '/img/youtube_preview',
			'visibility' => 'public',
		],

		'bandcamp_preview' => [
			'driver' => 'local',
			'root' => public_path('img/bandcamp_preview'),
			'url' => env('APP_URL') . '/img/bandcamp_preview',
			'visibility' => 'public',
		],
	],

	/*
	|--------------------------------------------------------------------------
	| Symbolic Links
	|--------------------------------------------------------------------------
	|
	| Here you may configure the symbolic links that will be created when the
	| `storage:link` Artisan command is executed. The array keys should be
	| the locations of the links and the values should be their targets.
	|
	*/

	'links' => [
		public_path('storage') => storage_path('app/public'),
	],
];
