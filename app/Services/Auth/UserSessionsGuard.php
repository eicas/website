<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Services\Auth;

use App\Models\User;
use App\Models\UserSession;
use Illuminate\Auth\SessionGuard;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

/**
 * This application uses an alternative implementation of the 'remember me' functionality in and between sessions.
 * For this, the Illuminate\Auth\SessionGuard is extended.
 *
 * A user login is maintained in the database via UserSession instances. Each instance represents a logged in user.
 * Multiple UserSessions may exist for any User. The instance stores the session id and, if the user requested that,
 * also a remember_me token.
 *
 * To be able to keep validating every user session, also the password hash is stored in the session and, if applicable,
 * in the remember me cookie. Upon each request, this hash is compared to the (current) user password hash and the
 * login is invalidated if there is no match.
 *
 * Note: the current user password hash is stored under the session guard 'name' in the session and therefore replaces
 * the user id value stored there in the original implementation!
 *
 * For each request, a user session is searched by 1) a session id or 2) a remember me token. If one of these is still
 * valid, a user is found.
 */
class UserSessionsGuard extends SessionGuard {
	/**
	 * Get the currently authenticated user.
	 *
	 * @return \Illuminate\Contracts\Auth\Authenticatable|null
	 */
	public function user() {
		if ($this->loggedOut) {
			// logger("[UserSessionsGuard::user] is logged out, return null");
			return;
		}

		// If we've already retrieved the user for the current request we can just
		// return it back immediately. We do not want to fetch the user data on
		// every call to this method because that would be tremendously slow.
		if (!is_null($this->user)) {
			// logger("[UserSessionsGuard::user] return user retrieved earlier, with userSession:");
			// logger($this->user->userSession);
			// logger("[UserSessionsGuard::user] (session id: '{$this->session->getId()}')");
			return $this->user;
		}

		if (is_null($this->user)) {
			// First try to find a UserSession based on the session id
			$session_id = $this->session->getId();
			// logger("[UserSessionsGuard::user] try to find userSession with session_id '$session_id'");
			if ($userSession = UserSession::nonExpiredSessionId($session_id)->first()) {
				// logger("[UserSessionsGuard::user] valid session found: '{$userSession->id}'");
				// logger($userSession);
				$this->user = $userSession->user;
				$this->checkPasswordHash($this->getHashedPassword() ?? '');
				if ($this->user) {
					// logger("[UserSessionsGuard::user] password from session matches, firing authenticated event.");
					$userSession->registerVisit();
					$this->fireAuthenticatedEvent($this->user);
				} else {
					// logger("[UserSessionsGuard::user] password from session does not match user password.");
				}
			}

			// if no user was found yet, try to decrypt a "recaller" cookie and attemt to get a user based on that.
			if (is_null($this->user) && !is_null($recaller = $this->recaller())) {
				// logger("[UserSessionsGuard::user] recaller found.");
				$this->user = $this->userFromRecaller($recaller);

				if ($recaller->valid()) {
					// logger("[UserSessionsGuard::user] recaller: '{$recaller->id()}|{$recaller->token()}|{$recaller->hash()}'");
					// logger("[UserSessionsGuard::user] user found: (id=" . optional($this->user)->id . ")");
					$this->checkPasswordHash($recaller->hash());
				} else {
					// an invalid recaller may not even contain a hash and could yield exceptions. Instead, check
					// with empty string: this will always fail and invalidate the user and session.
					$this->checkPasswordHash('');
				}

				if ($this->user) {
					// logger("[UserSessionsGuard::user] passwords matched, updating session.");
					$this->updateSession($this->user->getAuthPassword());
					// save the current session id into this UserSession so that it can be found next request
					$this->user->userSession->setSessionId($this->session->getId(), false); // (no save yet)
					$this->user->userSession->registerVisit(true); // (will also save instance)
					// logger("[UserSessionsGuard::user] updated userSession:");
					// logger($this->user->userSession);
					$this->fireLoginEvent($this->user, true);
					// logger("[UserSessionsGuard::user] login event fired.");
				} else {
					// logger("[UserSessionsGuard::user] no user or password mismatch.");
				}
			}
		}

		// logger("[UserSessionsGuard::user] ready, returning user (id=" . optional($this->user)->id . ").");
		return $this->user;
	}

	protected function checkPasswordHash(string $hash): bool {
		if (!$this->user) {
			// logger("[UserSessionsGuard::checkPasswordHash] no user, return");
			return false;
		}

		if (hash_equals($this->user->getAuthPassword(), $hash)) {
			// logger("[UserSessionsGuard::checkPasswordHash] hashes match, return true");
			return true;
		}

		// logger("[UserSessionsGuard::checkPasswordHash] hashes do not match, clear user data and logout");
		$this->clearUserDataFromStorage();
		$this->loggedOut = true;
		return false;
	}

	/**
	 * Get the name under which the hashed user password may be stored in the session.
	 *
	 * @return string
	 */
	public function getHashedPassword() {
		// logger("[UserSessionsGuard::getHashedPassword]");
		return $this->session->get($this->getName());
	}

	/**
	 * Get the ID for the currently authenticated user.
	 *
	 * @return int|string|null
	 */
	public function id() {
		if ($this->loggedOut || !$this->user) {
			// logger("[UserSessionsGuard::id] no user, return null");
			return;
		}

		// logger("[UserSessionsGuard::id] return '{$this->user()->getAuthIdentifier()}'");
		return $this->user()->getAuthIdentifier();
	}

	/**
	 * Log a user into the application.
	 *
	 * This method is overridden from Illuminate\Auth\SessionGuard and implements login with registration into the
	 * UserSession functionality.
	 *
	 * @param \Illuminate\Contracts\Auth\Authenticatable $user
	 * @param bool $remember
	 * @return void
	 */
	public function login(AuthenticatableContract $user, $remember = false) {
		// logger("[UserSessionsGuard::login] for user (id={$user->id}), remember: " . ($remember ? 'yes' : 'no'));
		$userSession = UserSession::create([
			'user_id' => $user->id,
			'session_id' => $this->session->getId(),
			'remember_token' => null, // for now!
			'ip' => $this->request->ip(),
			'user_agent' => $this->request->header('User-Agent'),
		]);
		// logger("[UserSessionsGuard::login] userSession created:");
		// logger($userSession);

		$this->updateSession($user->getAuthPassword());
		// logger("[UserSessionsGuard::login] session updated");

		$user->userSession = $userSession;
		$user->userSession->setSessionId($this->session->getId(), true);
		// logger("[UserSessionsGuard::login] userSession set on user:");
		// logger($userSession);

		// If the user should be permanently "remembered" by the application we will
		// queue a permanent cookie that contains the encrypted copy of the user
		// identifier. We will then decrypt this later to retrieve the users.
		if ($remember) {
			$this->ensureRememberTokenIsSet($user);
			// logger("[UserSessionsGuard::login] remember token surely set.");

			$this->queueRecallerCookie($user);
			// logger("[UserSessionsGuard::login] recaller cookie queued.");
		}

		// If we have an event dispatcher instance set we will fire an event so that
		// any listeners will hook into the authentication events and run actions
		// based on the login and logout events fired from the guard instances.
		$this->fireLoginEvent($user, $remember);
		// logger("[UserSessionsGuard::login] login event fired. (current session id: '{$this->session->getId()}')");

		$this->setUser($user);
		// logger("[UserSessionsGuard::login] user set on instance. Ready. Log user and its userSession");
		// logger($this->user);
		// logger($this->user->userSession);
	}

	// protected function updateSession($id) {
	// 	$return = parent::updateSession($id);
	// 	logger("[UserSessionsGuard::updateSession] updated session, new id: '{$this->session->getId()}'");
	// 	return $return;
	// }

	/**
	 * Invalidate other sessions for the current user.
	 *
	 * The application must be using the AuthenticateSession middleware.
	 *
	 * @param  string  $password
	 * @param  string  $attribute
	 * @return bool|null
	 *
	 * @throws \Illuminate\Auth\AuthenticationException
	 */
	public function logoutOtherDevices($password, $attribute = 'password') {
		logger("[UserSessionsGuard::logoutOtherDevices]");
		if (!$this->user()) {
			logger("[UserSessionsGuard::logoutOtherDevices] no current user, return null.");
			return;
		}
		if (!$this->user->userSession) {
			logger("[UserSessionsGuard::logoutOtherDevices] no current session, throw exception!");
			throw new Exception("Current user without session!");
		}

		$result = $this->rehashUserPassword($password, $attribute);
		$this->updateSession($user->getAuthPassword());
		logger("[UserSessionsGuard::logoutOtherDevices] user password rehashed and session updated.");

		if ($this->recaller() ||
			$this->getCookieJar()->hasQueued($this->getRecallerName())) {
			$this->queueRecallerCookie($this->user());
			logger("[UserSessionsGuard::logoutOtherDevices] recaller cookie queued.");
		}

		// also soft delete all UserSession for this user except the current UserSession.
		UserSession::userId($this->user->id)->where('id', '<>', $this->user->userSession->id)->delete();
		logger("[UserSessionsGuard::logoutOtherDevices] other UserSessions soft-deleted.");

		$this->fireOtherDeviceLogoutEvent($this->user());

		return $result;
	}
}
