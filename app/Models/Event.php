<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model {
	public $timestamps = false;

	protected $fillable = [
		'user_id',
		'model_type',
		'model_id',
		'action',
		'details',
	];
}
