<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Role extends Model {
	/**
	 * Roles are somewhat "fixed" and therefore do not need timestamps
	 */
	public $timestamps = false;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'key',
	];

	public function users() {
		return $this->belongsToMany(User::class);
	}

	public function getTranslatedNameAttribute() {
		// logger("[Role::getTranslatedName] {$this->key}: '" . __("role.{$this->key}.name") . "'");
		return __("role.{$this->key}.name");
	}

	public function getTranslatedDescriptionAttribute() {
		return __("role.{$this->key}.description");
	}

	/**
	 * Get the keys of all available roles.
	 * 
	 * @return string[]
	 */
	public static function getAllKeys(): array {
		return static::get()->pluck('key')->all();
	}
}
