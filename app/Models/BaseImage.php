<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
// SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>
//
// SPDX-License-Identifier: MIT

namespace App\Models;

// use App\Models\Content;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as IIMage;
use LogicException;
use Str;

abstract class BaseImage extends Model {
	use SoftDeletes;

	/**
	 * Optimized widths. Note: always order these from large to small!
	 */
	const OPTIMIZED_WIDTHS = [2048, 1536, 1024, 930, 768, 300, 150, 75];

	private static $storage_root;
	private static $url_root;

	protected $fillable = [
		'original_filename',
		'filename',
		'alt',
		'heights',
	];

	protected $casts = [
		'id' => 'integer',
		'heights' => 'array',
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
		'deleted_at' => 'datetime',
	];

	public static function getStorageDiskName(): string {
		// this will throw an "Access to undeclared static property" error if the extending class does not
		// define a value for 'protected static $storage_disk'
		if (!is_string(static::$storage_disk)) {
			throw new LogicException(sprintf(
				"invalid value for protected static \$storage_disk in %s",
				static::class
			));
		}
		return static::$storage_disk;
	}

	/**
	 * Return the subdirectory (within the storage) in which the files can be found.
	 * 
	 * The resulting string does *not* have leading or trailing slashes.
	 * 
	 * @return string
	 */
	public function getSubdirectoryAttribute() {
		return $this->created_at->format('Y/m/d') . "/{$this->id}";
	}

	/**
	 * Store the given file.
	 * 
	 * Storage of files is a process with two main tasks, explained below.
	 * 
	 * First, the raw image file should be stored on disk. This is done in a location which is accessible via the
	 * Storage facade. This class should define the (protected static string) $storage_disk, which refers to the
	 * Storage disk name.
	 * 
	 * Within this Storage, files are stored in year, month, day and time (H-m-s) named directories. I.e. images
	 * uploaded at different times (second resolution) end up in different subdirectories.
	 * 
	 * For each image, several 'formats' or 'resolutions' are stored by resizing the original image to widths as
	 * defined in the OPTIMIZED_WIDTHS class constant (with automatic heights, maintaining aspect ratio). These resized
	 * files are named as the base filename, with the dimension suffixed. E.g. for the image 'flower.jpg', the following
	 * files are stored:
	 * - flower.jpg (the original file as uploaded, not resized)
	 * - flower-2048xHHH.jpg
	 * - flower-1024xHHH.jpg
	 * - etc.
	 * where HHH is the height in pixels.
	 * 
	 * The original filename is the name as provided by the user and can therefore not be considered 'safe'. It needs
	 * to be checked. This is done in the normalizedBasename class function, which removes illegal characters and makes
	 * sure the filename is valid.
	 * 
	 * Second, a model instance needs to be created. This instance should contain various metadata about the image.
	 * As a minimum, this is the filename, the img tag 'alt' attribute value and the actual widths and heights stored.
	 * If any other parameters should be stored (implemented in an extending class), these can be provided in the
	 * $image_parameters array (optional).
	 * 
	 * This function returns the freshly created model instance or null if anything went wrong.
	 * 
	 * @param \Illuminate\Http\UploadedFile $file
	 * @param string $alt
	 * @return static|null
	 */
	public static function storeFile(UploadedFile $file, string $alt, array $image_parameters = []) {
		// Make sure to use the 'imagick' driver instead of the default
		// GD, since GD turns white backgrounds into grey when resizing.
		IImage::configure(['driver' => 'imagick']);

		// if anything goes wrong, just return null
		try {
			// load the file as an Intervention Image instance
			$img = IImage::make($file)->orientate();

			// determine the image type and name extension and normilize (png or jpg)
			$mime = $img->mime();
			if ($mime === 'image/png') {
				$extension = '.png';
			} else {
				$img->encode('jpg');
				$extension = '.jpg';
			}

			$basename = self::normalizedBasename($file->getClientOriginalName());

			$heights = array_fill_keys(self::OPTIMIZED_WIDTHS, 0);
			// always sort from large to small, as the following code will attempt to create various sizes. Making
			// an image smaller is better than making it larger.
			krsort($heights);
			// creating a model instance now is necessary to get a created_at timestamp and with that also the
			// subdirectory
			$image = Image::create(array_merge($image_parameters, [
				'original_filename' => $file->getClientOriginalName(),
				'filename' => $basename . $extension,
				'alt' => $alt,
				'heights' => $heights,
			]));

			$full_path = $image->getFullPath();
			if (!is_readable(dirname($full_path))) {
				mkdir(dirname($full_path), 0755, true);
			}
			$img->save($full_path);
			foreach ($heights as $width => &$height) {
				// resize and remember dimensions
				$img->resize($width, null, function ($constraint) {
					$constraint->aspectRatio();
					// $constraint->upsize(); do allow upsize, as the width is otherwise unpredictable (not stored)
				});
				$height = $img->height();
				$img->save($image->getFullPath($width, $height));
			}

			$image->heights = $heights;
			$image->save();

			return $image;

		} catch (\Exception $e) {
			logger($e);
			return null;
		}
	}

	/**
	 * Get the root directory on the filesystem for all images.
	 * 
	 * The root directory is cached internally.
	 * 
	 * @return string
	 */
	protected static function getStorageRoot(): string {
		if (is_null(self::$storage_root)) {
			self::$storage_root = config("filesystems.disks." . self::getStorageDiskName() . ".root");
		}
		return self::$storage_root;
	}

	/**
	 * Get the root url for all images.
	 * 
	 * The root url is cached internally.
	 * 
	 * @return string
	 */
	protected static function getUrlRoot(): string {
		if (is_null(self::$url_root)) {
			self::$url_root = config("filesystems.disks." . self::getStorageDiskName() . ".url");
		}
		return self::$url_root;
	}

	/**
	 * Get the full path on the filesystem to the current image.
	 * 
	 * Normally, the path to the original image is returned. If a $width is provided, the path to the image with that
	 * width is returned, automatically looking for the right height specifier. If there is no image version with the
	 * given width (the given width is not in the heights property of the current image), null is returned.
	 * 
	 * If $height is provided as well, the path to the image version with the given width and height is returned.
	 * 
	 * This function does not check whether a file exists for the path returned.
	 * 
	 * @param integer|null $width (Default null) Width in pixels
	 * @param integer|null $height (Default null) Height in pixels
	 * @return string|null
	 */
	protected function getFullPath(int $width = null, int $height = null): ?string {
		$path = $this->getPath($width, $height);
		if (is_null($path)) {
			return null;
		}
		return self::getStorageRoot() . "/{$path}";
	}

	/**
	 * Get the url path to the current image.
	 * 
	 * Normally, the path to the original image is returned. If a $width is provided, the path to the image with that
	 * width is returned, automatically looking for the right height specifier. If there is no image version with the
	 * given width (the given width is not in the heights property of the current image), null is returned.
	 * 
	 * This function does not check whether a file exists for the path returned.
	 * 
	 * @param integer|null $width (Default null) Width in pixels
	 * @return string|null
	 */
	public function getUrl(int $width = null): ?string {
		$path = $this->getPath($width, null);
		if (is_null($path)) {
			return null;
		}
		return self::getUrlRoot() . "/{$path}";
	}

	/**
	 * Get the url path to the current image with width and height specifier placeholders.
	 * 
	 * The path to the image is returned with a width and height specifier, but these are fixed strings: %w and %h
	 * for width and height respectively. The general form is:
	 * 
	 * https://domain.tld/path/to/image-%wx%h.ext
	 * 
	 * This function does not check whether a file exists for the path returned.
	 * 
	 * @return string|null
	 */
	public function getUrlTemplate(): string {
		// logger("BaseImage::getUrlTemplate: '" . self::getUrlRoot() . "/{$this->getPathTemplate()}'");
		return self::getUrlRoot() . "/{$this->getPathTemplate()}";
	}

	/**
	 * Get the relative path to the current image.
	 * 
	 * The path does not start with a slash.
	 * 
	 * Normally, the path to the original image is returned. If a $width is provided, the path to the image with that
	 * width is returned, automatically looking for the right height specifier. If there is no image version with the
	 * given width (the given width is not in the heights property of the current image), null is returned.
	 * 
	 * If $height is provided as well, the path to the image version with the given width and height is returned.
	 * 
	 * This function does not check whether a file exists for the path returned.
	 * 
	 * @param integer|null $width (Default null) Width in pixels
	 * @param integer|null $height (Default null) Height in pixels
	 * @return string|null
	 */
	public function getPath(int $width = null, int $height = null): ?string {
		$original_filename = "{$this->subdirectory}/{$this->filename}";
		if (is_null($width)) {
			return $original_filename;
		}
		if (is_null($height)) {
			$calculated_height = $this->heights[$width] ?? null;
			if (is_null($calculated_height)) {
				return null;
			}
			$height = $calculated_height;
		}
		$pathinfo = pathinfo($original_filename);
		return sprintf(
			"%s/%s-%dx%d.%s",
			$pathinfo['dirname'], // a leading path is assumed to be always present
			$pathinfo['filename'],
			$width, $height,
			$pathinfo['extension'] // an extension is assumed to be always present
		);
	}

	/**
	 * Return the relative path to the current image with width and height specifier placeholders.
	 * 
	 * The path does not start with a slash.
	 * 
	 * The path to the image is returned with a width and height specifier, but these are fixed strings: %w and %h
	 * for width and height respectively. The general form is:
	 * 
	 * path/to/image-%wx%h.ext
	 * 
	 * This function does not check whether a file exists for the path returned.
	 * 
	 * @return string
	 */
	public function getPathTemplate(): string {
		$pathinfo = pathinfo("{$this->subdirectory}/{$this->filename}");
		return sprintf(
			"%s/%s-%%wx%%h.%s",
			$pathinfo['dirname'], // a leading path is assumed to be always present
			$pathinfo['filename'],
			$pathinfo['extension'] // an extension is assumed to be always present
		);
	}

	/**
	 * Determine the smallest registered width (in pixels) for this image.
	 * 
	 * Returns null if no widths are registered.
	 * 
	 * @return integer|null
	 */
	public function getSmallestWidth(): int {
		if (!$this->heights) {
			return null;
		}
		return min(array_keys($this->heights));
	}

	/**
	 * Determine the largest registered width (in pixels) for this image.
	 * 
	 * Returns null if no widths are registered.
	 * 
	 * @return integer|null
	 */
	public function getLargestWidth(): int {
		if (!$this->heights) {
			return null;
		}
		return max(array_keys($this->heights));
	}

	/**
	 * Get the width to height ratio of the image.
	 * 
	 * The ratio is defined as the width (in pixels) divided by the height (in pixels). Thus, a square image has a
	 * ratio of 1; a "landscape" image has a ratio > 1 and a "portret" image has a ratio < 1.
	 * 
	 * @return float
	 */
	public function getRatio(): float {
		$w = $this->getLargestWidth();
		$h = $this->heights[$w];
		return $w / $h;
	}

	/**
	 * Given some potentially unsafe filename, return the normilized base of this name.
	 * 
	 * Any non-ascii characters are transliterated to a fitting ascii counterpart. Than, only the name part of the
	 * filename (i.e. no leading paths and no extension) is taken. In this, only lowercase letters, digits, dot and
	 * dash are allowed: upper case letters are 'lowered', any other characters are replaced by dash. Any double
	 * dashes and combinations of dashes and dots are replaced by a single dash and dashes and dots are trimmed from
	 * the beginning and end of the string.
	 * Finally, if this results in an empty string, the md5-hash of the original suggested name is used as filename.
	 * 
	 * @param string $suggested the name as suggested by the user
	 * @return string
	 */
	protected static function normalizedBasename(string $suggested): string {
		$filename = Str::ascii($suggested);
		$parts = pathinfo($filename);
		$filename = strtolower($parts['filename']);
		$filename = preg_replace('/[^a-z0-9.-]/', '-', $filename);
		$filename = preg_replace(['/--+/', '/\.-/', '/-\./'], ['-', '.', '.'], $filename);
		$filename = trim($filename, '-.');
		if ($filename === '') {
			$filename = md5($suggested);
		}
		return $filename;
	}

	/**
	 * Delete the current image from the database.
	 * 
	 * If there are no other references to the files, also delete the files.
	 */
	public function delete() {
		$path = $this->subdirectory;
		Storage::disk(self::getStorageDiskName())->deleteDirectory($path);
		$path_parts = explode("/", $path);
		while (array_pop($path_parts)) {
			$path = implode("/", $path_parts);
			if ($path === "") {
				break;
			}
			if (count(Storage::disk(self::getStorageDiskName())->allFiles($path)) === 0) {
				logger("deleting storage dir '$path'");
				Storage::disk(self::getStorageDiskName())->deleteDirectory($path);
			} else {
				break;
			}
		}

		parent::delete();
	}
}
