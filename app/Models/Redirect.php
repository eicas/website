<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Models;

use App\Models\Traits\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Redirect extends Model {
	/**
	 * The maximum number of hops per redirect. Limit situations where one redirect leads to another and another, etc.
	 * 
	 * @var integer
	 */
	const MAX_REDIRECTS = 5;

	use SoftDeletes;

	protected $fillable = ['from', 'to', 'comment'];

	public function scopeFrom($query, string $from) {
		return $query->where('from', $from);
	}

	/**
	 * Return a response instance, redirecting the client to the intended url, with a http 302 status code.
	 * 
	 * Note: a 301 Moved Permanently would sometimes be better, but not if a new page is added later on the old
	 * (from) url. Therefore, we use status 302.
	 */
	public function redirect() {
		$this->usage = $this->usage + 1;
		$this->save();
		return redirect($this->to, 302); // 302 Found = default
	}

	/**
	 * Check whether the redirects are still valid, i.e. check that no chain of redirects is too long.
	 * 
	 * If $new_from and $new_to are defined (sting), they are added to the set of redirects to check. In this case,
	 * it is also checked whether $new_from would collide with an existing redirect.
	 * 
	 * This function returns true if all redirects are OK, including the optional new redirect. False otherwise.
	 * 
	 * @param string string|null $new_from
	 * @param string string|null $new_to
	 * @return boolean
	 */
	public static function checkAll(string $new_from = null, string $new_to = null, bool $override_collision = false): bool {
		$redirects = static::orderBy('from')->get()->pluck('to', 'from')->all();
		if (!is_null($new_from) && !is_null($new_to)) {
			if (!$override_collision && array_key_exists($new_from, $redirects)) {
				return false;
			}
			$redirects[$new_from] = $new_to;
		}
		// logger("checking redirects for too many hops. Current set:");
		// logger($redirects);
		$counts = [];
		foreach (array_keys($redirects) as $from) {
			// logger("counting redirects for '$from'");
			$count = 0;
			$check = $from;
			// logger("- checking redirect $count from '$check'");
			while (array_key_exists($check, $redirects) && $count <= static::MAX_REDIRECTS) {
				$count++;
				$check = $redirects[$check];
				if (array_key_exists($check, $counts)) {
					$count += $counts[$check];
					// logger("found counted redirect '$check'");
					break;
				}
				// logger("- checking redirect $count from '$check'");
			}
			if ($count > static::MAX_REDIRECTS) {
				return false;
			}
			$counts[$from] = $count;
			// logger("counts:");
			// logger($counts);
		}
		return true;
	}
}
