<?php

// SPDX-FileCopyrightText: 2021 - 2022 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Contracts\Auth\MustVerifyEmail as MustVerifyEmailContract;
use Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Profile\CanResetPassword;
use App\Traits\Profile\MfaAuthenticatable;
use App\Traits\Profile\MustVerifyEmail;
use App\Models\UserSession;
use App\Models\Role;

class User extends Model implements
	AuthorizableContract,
	AuthenticatableContract,
	CanResetPasswordContract,
	MustVerifyEmailContract {

	use
		Authenticatable, Authorizable, CanResetPassword, MustVerifyEmail,
		Notifiable, SoftDeletes, MfaAuthenticatable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'email',
		'password',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'mfa_secret',
		'mfa_recovery_codes',
		// 'remember_token',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'email_verified_at' => 'datetime',
		'mfa_confirmed_at' => 'datetime',
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
		'deleted_at' => 'datetime',
	];

	protected $userSession = null;

	public function roles() {
		return $this->belongsToMany(Role::class);
	}

	public function userSessions() {
		return $this->hasMany(UserSession::class)->orderBy('updated_at', 'desc');
	}

	public function setUserSessionAttribute(UserSession $userSession) {
		$this->userSession = $userSession;
	}

	public function getUserSessionAttribute() {
		return $this->userSession;
	}

	public function loadUserSession() {
		$session_id = session()->getId();
		foreach ($this->userSessions as $userSession) {
			if ($userSession->session_id === $session_id) {
				$this->userSession = $userSession;
				break;
			}
		}
	}

	/**
	 * Get the token value for the "remember me" session.
	 * 
	 * This method is overridden from Illuminate\Auth\Authenticatable.
	 * 
	 * A User model does not have a remember token. Instead, it has multiple sessions (UserSession model) associated
	 * which in turn can have a remember token. Therefore, getting the token depends on the actual session.
	 * 
	 * This method returns the remember token for the current session, if one is available, or null otherwise.
	 *
	 * @return string|null
	 */
	public function getRememberToken() {
		// logger("[User::getRememberToken] return token '" . optional($this->userSession)->remember_token . "'.");
		return optional($this->userSession)->remember_token;
	}

	/**
	 * Set the token value for the "remember me" session.
	 *
	 * This method is overridden from Illuminate\Auth\Authenticatable.
	 * 
	 * A User model does not have a remember token. Instead, it has multiple sessions (UserSession model) associated
	 * which in turn can have a remember token. Therefore, setting the token depends on the actual session.
	 * 
	 * This method sets the remember token on the current session, if one is available.
	 *
	 * @param  string  $value
	 * @return void
	 */
	public function setRememberToken($value) {
		// logger("[User] setRememberToken('$value')");
		if ($this->userSession) {
			// logger("[User] current userSession: id={$this->userSession->id}; set the token");
			$this->userSession->setRememberToken($value); // will also save
		}
	}

	/**
	 * Determine whether the user has a specific role.
	 *
	 * To determine whether the user is allowed to do administrative tasks on a specific subject, use the admin-method
	 * on the policy: User::can('admin', <the class>), as this also takes into account whether the user has the 'admin'
	 * role (except for the user policy, which is slightly more complicated).
	 *
	 * @param \App\Models\Role|string $role
	 * @return boolean
	 */
	public function hasRole($role): bool {
		$role = $role->key ?? $role;
		foreach ($this->roles as $user_role) {
			if ($user_role->key === $role) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Determine whether the user has at least one of the specified roles.
	 *
	 * To determine whether the user is allowed to do administrative tasks on a specific subject, use the admin-method
	 * on the policy: User::can('admin', <the class>), as this also takes into account whether the user has the 'admin'
	 * role (except for the user policy, which is slightly more complicated).
	 *
	 * @param \App\Models\Role[]|string[] $roles
	 * @return boolean
	 */
	public function hasAnyRole(array $roles): bool {
		foreach ($roles as &$role) {
			$role = $role->key ?? $role;
		}
		foreach ($this->roles as $user_role) {
			if (in_array($user_role->key, $roles)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Return a list of id's of users with at least one admin-like role, i.e. the EICAS users.
	 *
	 * @return integer[]
	 */
	public static function getAdminUserIds(): array {
		return
			// get all users which have at least one 'admin' like role
			// list their id's and return the id's as an array
			User::whereHas('roles', function ($q) {
				$q->where('key','like','%admin%');
			})->get()->pluck('id')->all();
	}
}
