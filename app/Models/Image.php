<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
// SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>
//
// SPDX-License-Identifier: MIT

namespace App\Models;

use Exception;
use App\Models\BaseImage;
use App\Models\Page;
use App\Models\Pivots\ImageImageset;

class Image extends BaseImage {
	protected static $storage_disk = 'images';

	protected $fillable = [
		'original_filename',
		'filename',
		'alt',
		'heights',
		'title',
		'artist',
		'year',
		'info',
		'image_id',
	];

	protected $casts = [
		'id' => 'integer',
		'heights' => 'array',
		'year' => 'integer',
		'image_id' => 'integer',
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
		'deleted_at' => 'datetime',
	];

	/**
	 * Get the large version of this image (if defined).
	 */
	public function large() {
		return $this->belongsTo(Image::class, 'image_id');
	}

	/**
	 * Get all small versions of this image.
	 */
	public function small() {
		return $this->hasMany(Image::class);
	}

	/**
	 * Delete the current image from the database.
	 *
	 * Does not clean up the image from the filesystem.
	 * 
	 * @throws Exception when an imageset with this image still exists.
	 */
	public function delete() {
		$sets = $this->belongsToMany(Imageset::class)
			->using(ImageImageset::class)
			->withPivot('id')
			->get()
			->map(function ($set) { return $set->id; });
		if($sets->isNotEmpty()) {
			throw new Exception("Image $this->id is still part of set(s): $sets");
		}
		Image::where('image_id', $this->id)->update(['image_id' => null]);
		parent::delete();
	}
}
