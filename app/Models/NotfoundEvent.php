<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Models;

use App\Models\Notfound;
use Illuminate\Database\Eloquent\Model;

class NotfoundEvent extends Model {
	public $timestamps = false;

	protected $fillable = [
		'notfound_id',
		'ip',
		'referer',
		'created_at'
	];

	protected $casts = [
		'created_at' => 'datetime',
	];

	public function notfound() {
		return $this->belongsTo(Notfound::class);
	}
}
