<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Models\Pivots;

use App\Models\Image;
use App\Models\Imageset;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ImageImageset extends Pivot {
	public $incrementing = true;
	protected $table = 'image_imageset';

	protected $fillable = [
		'id',
		'image_id',
		'imageset_id',
		'order',
		'href',
		'alt',
		'created_at',
		'updated_at',
	];

	protected $casts = [
		'id' => 'integer',
		'image_id' => 'integer',
		'imageset_id' => 'integer',
		'order' => 'integer',
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
	];

	public function image() {
		return $this->belongsTo(Image::class);
	}

	public function page() {
		return $this->belongsTo(Imageset::class);
	}
}
