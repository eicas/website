<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\SoftDeletes as EloquentSoftDeletes;
use Illuminate\Support\Facades\Route;

/**
 * Override the SoftDeletes trait from Eloquent to implement route model binding of soft-deleted models.
 * 
 * Since Laravel 8.55 routes support 'withTrashed', but still not 'onlyTrashed'. This trait supports binding
 * specifically trashed (i.e. 'only trashed') models if, and only if, the route name ends with '.restore'.
 * 
 * For the rest, this trait is a drop-in replacement of the Illuminate\Database\Eloquent\SoftDeletes trait.
 */
trait SoftDeletes {
	use EloquentSoftDeletes;

	/**
	 * Retrieve the model for a bound value.
	 * 
	 * Modified from \Illuminate\Database\Eloquent\Model::resolveRouteBinding()
	 *
	 * @param mixed $value
	 * @param string|null $field
	 * @return \Illuminate\Database\Eloquent\Model|null
	 */
	public function resolveRouteBinding($value, $field = null) {
		$route_name_parts = explode(".", Route::currentRouteName());
		if (end($route_name_parts) === 'restore') {
			return $this->onlyTrashed()->where($field ?? $this->getRouteKeyName(), $value)->first();
		}
		return $this->where($field ?? $this->getRouteKeyName(), $value)->first();
	}
}
