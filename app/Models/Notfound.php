<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Models;

use App\Models\NotfoundEvent;
use Illuminate\Database\Eloquent\Model;

class Notfound extends Model {
	protected $fillable = ['url'];

	protected $casts = [
		'id' => 'integer',
		'ignore_since' => 'datetime',
		'created_at' => 'datetime',
		'deleted_at' => 'datetime',
	];

	public function notfoundEvents() {
		return $this->hasMany(NotfoundEvent::class)->orderBy('created_at', 'desc');
	}

	/**
	 * A new request was received which should result into a 404. Register it.
	 * 
	 * This function checks whether this url was requested earlier. If not, a new Notfound entry is created. Otherwise,
	 * the instance found is used. Then, a new notfoundEvent is added with the given ip and referer for the current
	 * datetime.
	 * 
	 * A Notfound can have the 'ignore_since' field set (to a datetime). If this is the case, the event is not further
	 * registered and null is returned by this function. If it is still unset, a new event is created and returned.
	 * 
	 * @param string $url
	 * @param string $ip
	 * @param string|null $referer
	 * @return NotfoundEvent|null
	 */
	public static function add(string $url, string $ip, string $referer = null) {
		$notfound = static::firstOrCreate(['url' => $url]);
		if (is_null($notfound->ignore_since)) {
			return $notfound->notfoundEvents()->create([
				'url' => $url,
				'ip' => $ip,
				'referer' => $referer,
				'created_at' => now(),
			]);
		}
		return null;
	}
}
