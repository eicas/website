<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Models\GuestForms;

use App\Models\Traits\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Note: this model is tightly coupled with the Mollie payment service provider.
 */
class NewFriend extends Model {
	use SoftDeletes;

	/**
	 * Valid 'word vriend' donation amounts in euro-cents
	 */
	const AMOUNTS = [
		2500,
		5000,
		7500,
		10000,
	];

	protected $fillable = [
		'name',
		'email',
		'telephone',
		'address',
		'postalcode',
		'city',
		'country',
		'amount',
		'payment_test',
		'payment_method',
		'payment_reference',
		'payment_id',
		'payment_status',
	];

	protected $casts = [
		'id' => 'integer',
		'amount' => 'decimal:2',
		'payment_test' => 'boolean',
		'paid_at' => 'datetime',
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
		'deleted_at' => 'datetime',
	];

	/**
	 * Scope the search query by the Mollie payment id.
	 */
	public function scopePaymentId($query, string $payment_id) {
		return $query->where('payment_id', $payment_id);
	}

	/**
	 * Set the paid_at attribute to the current time for this model and save it (unless $save === false).
	 * 
	 * @param boolean $save
	 */
	public function setPaid(bool $save = true) {
		$this->paid_at = now();
		if ($save) {
			$this->save();
		}
	}

	/**
	 * Determine all payment methods supported by the application.
	 * 
	 * This list is fetched from Mollie and stored as an array with the method id's as the keys and the
	 * translated descriptions as values.
	 * 
	 * Normally, the answer from Mollie is stored in the session. If the $refresh parameter is set to true, the
	 * list is fetched from Mollie again.
	 * 
	 * @param bool $refresh
	 * @return array
	 */
	public static function getPaymentMethods(bool $refresh = false) {
		$payment_methods = session('mollie.payment_methods', []);
		if ($refresh || count($payment_methods) === 0) {
			$methods = mollie()->methods->allActive([
				'locale' => 'nl_NL',
			]);
			$payment_methods = [];
			foreach ($methods as $method) {
				$payment_methods[$method->id] = $method->description;
			}
			session(['mollie.payment_methods' => $payment_methods]);
		// } else {
		// 	logger("Mollie payment methods in session:");
		// 	foreach ($payment_methods as $method => $label) {
		// 		logger("[$method] $label");
		// 	}
		}
		return $payment_methods;
	}
}
