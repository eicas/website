<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Models;

use App\Models\Image;
use App\Models\Page;
use App\Models\Pivots\ImageImageset;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Imageset extends Model {
	use SoftDeletes;

	protected $fillable = ['name'];

	protected $casts = [
		'id' => 'integer',
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
		'deleted_at' => 'datetime',
	];

	public function imageImagesets() {
		return $this->hasMany(ImageImageset::class)->orderBy('order');
	}

	public function images() {
		return $this->belongsToMany(Image::class)
			->using(ImageImageset::class)
			->withPivot('id', 'order', 'href', 'alt', 'created_at', 'updated_at')
			->withTimestamps()
			->orderBy('order');
	}

	public function pages() {
		return $this->hasMany(Page::class);
	}
}
