<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Models;

use App\Helpers\Misc;
use App\Models\Traits\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Download extends Model {
	use SoftDeletes;

	const MIMETYPES = [
		'application/pdf' => ['pdf'],
		'application/zip' => ['zip'],
	];

	protected $fillable = ['filename', 'size'];

	public function getExtensionAttribute(): string {
		return pathinfo($this->filename, PATHINFO_EXTENSION);
	}

	public function getStoragePathAttribute(): string {
		return sprintf(
			'%s-%d.%s',
			$this->created_at->format('YmdHis'),
			$this->id,
			$this->extension
		);
	}

	public function getFormattedSizeAttribute(): string {
		return Misc::filesize($this->size);
	}

	public function scopeFilename($query, string $filename) {
		return $query->where('filename', $filename);
	}

	public function downloadResponse() {
		return Storage::disk('downloads')->download(
			$this->getStoragePathAttribute(),
			$this->filename
		);
	}
}
