<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
// SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>
//
// SPDX-License-Identifier: MIT

namespace App\Models;

use App\Models\Page;
use App\Models\Traits\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Prunable;

class Content extends Model {
	use Prunable, SoftDeletes;

	/**
	 * All available types. The translation directory should contain a file with translations of the type (a label)
	 * and a description.
	 */
	public const TYPES = [
		'text',
		'image',
		'youtube',
		'bandcamp',
		'lvp-tickets',
		'mc-newsletterform',
		'form-new-friend',
		'shop-item',
	];

	protected $fillable = ['page_id', 'order', 'type', 'content'];

	protected $casts = [
		'id' => 'integer',
		'page_id' => 'integer',
		'order' => 'integer',
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
		'deleted_at' => 'datetime',
	];

	public function page() {
		return $this->belongsTo(Page::class);
	}

	/**
	 * Return a list of Content instances which may be automatically pruned from the DB.
	 */
	public function prunable() {
		return static::onlyTrashed()->where('deleted_at', '<', now()->subWeek());
	}
}
