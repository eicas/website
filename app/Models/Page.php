<?php

// SPDX-FileCopyrightText: 2021 - 2022 Marten Koetsier <eicas@koetsierengineering.nl>
// SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>
//
// SPDX-License-Identifier: MIT

namespace App\Models;

use App\Models\Image;
use App\Models\Imageset;
use App\Models\Pivots\ImagePage;
use App\Models\Redirect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\MessageBag;
use Str;

class Page extends Model {
	use SoftDeletes;

	public const VISIBILITY = [
		0 => 'review',
		1 => 'public',
	];

	protected $fillable = ['language', 'slug', 'title', 'description', 'template', 'imageset_id', 'visibility'];

	protected $casts = [
		'id' => 'integer',
		'imageset_id' => 'integer',
		'visibility' => 'integer',
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
		'deleted_at' => 'datetime',
	];

	/**
	 * @return string[]
	 */
	public static function getAvailableTemplates() {
		static $templates = null;
		if (is_null($templates)) {
			$templates = [];
			foreach (Storage::disk('templates')->allFiles('') as $filename) {
				if (Str::endsWith($filename, '.blade.php')) {
					$templates[] = str_replace('/', '.', Str::replaceLast('.blade.php', '', $filename));
				}
			}
		}
		return $templates;
	}

	/**
	 * Return a string representation of the visibility.
	 * 
	 * @return string
	 */
	public function getVisibilityLabelAttribute() {
		return self::VISIBILITY[$this->attributes['visibility']] ?? '?';
	}

	public function scopePublic($query) {
		return $query->where('visibility', '>=', 1);
	}

	public function getRelativeUrlAttribute() {
		// perhaps later, the language (ISO-639-1) should also become part of the url.
		return str_replace('//', '/', "/{$this->attributes['slug']}");
	}

	public function scopeRelativeUrl($query, $url) {
		return $query->where('slug', $url);
	}

	public function imagePages() {
		return $this->hasMany(ImagePage::class)->orderBy('order');
	}

	public function images() {
		return $this->belongsToMany(Image::class)
			->using(ImagePage::class)
			->withPivot('id', 'order', 'href', 'alt', 'created_at', 'updated_at')
			->withTimestamps()
			->orderBy('order');
	}

	public function imageset() {
		return $this->belongsTo(Imageset::class);
	}

	public function contents() {
		return $this->hasMany(Content::class)->orderBy('order', 'asc');
	}

	public function trashedContents() {
		return $this->hasMany(Content::class)->onlyTrashed()->orderBy('deleted_at', 'desc');
	}

	protected static function booted() {
		static::updated(function ($page) {
			if ($page->isDirty('slug')) {
				$from = $page->getOriginal('slug');
				$to = $page->slug;
				// logger("dirty slug: '$to' (was: '$from')");
				if (Redirect::checkAll($from, $to)) {
					Redirect::create([
						'from' => $from,
						'to' => $to,
						'comment' => 'automatisch gegenereerd bij aanpassen pagina-url',
					]);
					// logger("redirect created.");
					session()->flash('status', "Er is een redirect gegenereerd voor de oude url: '$from' > '$to'");
				} else {
					$messages = new MessageBag();
					$messages->add(
						'slug',
						'Er is geen redirect gegenereerd voor deze wijziging omdat dat zou leiden tot' .
							' te veel opeenvolgende redirects of omdat er al een redirect bestond voor' .
							' de originele url.'
					);
					session()->flash('errors', $messages);
				}
			}
		});
	}

	/**
	 * Get the url to the current page.
	 *
	 * @return string
	 */
	public function getUrlAttribute(): string {
		return config("app.url") . $this->attributes['slug'];
	}

}
