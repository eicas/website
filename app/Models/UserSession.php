<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;

class UserSession extends Model {
	use SoftDeletes;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'user_id',
		'session_id',
		'remember_token',
		'ip',
		'user_agent',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		// 'session_id',
		// 'remember_token',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'user_id' => 'integer',
		'number_of_visits' => 'integer',
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
		'deleted_at' => 'datetime',
	];

	public function user() {
		return $this->belongsTo(User::class);
	}

	/**
	 * Register a new visit to this UserSession and return the UserSession for chaining.
	 * 
	 * Registering is implemented as incrementing a counter within the UserSession.
	 * 
	 * Unless told otherwise, this function also saves the instance to the store.
	 * 
	 * @param bool $save Whether to save after incrementing.
	 * @return \App\Models\UserSession
	 */
	public function registerVisit(bool $save = true) {
		$this->number_of_visits++;
		// logger("[UserSession::registerVisit] incremented to {$this->number_of_visits}.");
		if ($save) {
			$this->save();
			// logger("[UserSession::registerVisit] saved.");
		}
		return $this;
	}

	/**
	 * Set the remember token on this instance from the given value and return the UserSession for chaining.
	 * 
	 * Unless told otherwise, this function also saves the instance to the store.
	 * 
	 * @param string $token The token to store.
	 * @param bool $save Whether to save after incrementing.
	 * @return \App\Models\UserSession
	 */
	public function setRememberToken(string $token, bool $save = true) {
		$this->remember_token = $token;
		// logger("[UserSession::setRememberToken] token set: '$token'");
		if ($save) {
			// logger("[UserSession::setRememberToken] saved.");
			$this->save();
		}
		return $this;
	}

	/**
	 * Set the session id on this instance from the given value and return the UserSession for chaining.
	 * 
	 * Unless told otherwise, this function also saves the instance to the store.
	 * 
	 * @param string $id The session id to store.
	 * @param bool $save Whether to save after incrementing.
	 * @return \App\Models\UserSession
	 */
	public function setSessionId(string $id, bool $save = true) {
		$this->session_id = $id;
		// logger("[UserSession::setSessionId] session id set: '{$id}'");
		if ($save) {
			$this->save();
			// logger("[UserSession::setSessionId] saved.");
		}
		return $this;
	}

	/**
	 * Return the time in seconds since expiration of this session or 0 if it is not expired.
	 * 
	 * Note that a session may be expired but revived if there is a remember token.
	 * 
	 * @return integer
	 */
	public function getExpiredAttribute(): int {
		$threshold = now()->subMinutes(config('session.lifetime'));
		$expired = $this->updated_at->diffInSeconds($threshold, false);
		return max(0, $expired);
	}

	/**
	 * Return whether a remember token has been stored for this session.
	 * 
	 * @return boolean
	 */
	public function getCanRememberAttribute(): bool {
		return !is_null($this->remember_token);
	}

	/**
	 * Scope a query to the given session id and that the record was last updated after the current time minus the
	 * session lifetime (in minutes).
	 */
	public function scopeNonExpiredSessionId($query, $session_id) {
		$threshold = now()->subMinutes(config('session.lifetime'));
		// logger("[UserSession] scope to session_id '$session_id' and updated after $threshold");
		return $query->where([
			['session_id', '=', $session_id],
			['updated_at', '>=', $threshold],
		]);
	}

	/**
	 * Scope the query to records that were expired beyond the session soft lifetime.
	 * 
	 * For this, the setting in /config/session.php:soft_lifetime (in minutes) is used.
	 * Also, this query scopes to records without a remember-token.
	 */
	public function scopeHardExpired($query) {
		$threshold = now()->subMinutes(config('session.hard_lifetime'));
		return $query->whereNull('remember_token')->where('updated_at', '<', $threshold);
	}

	/**
	 * Scope a query to the given remember token.
	 */
	public function scopeRememberToken($query, $token) {
		// logger("[UserSession] scope to remember_token '$token'");
		return $query->where('remember_token', '=', $token);
	}

	/**
	 * Scope the query to the given user id.
	 */
	public function scopeUserId($query, int $user_id) {
		// logger("[UserSession] scope to user_id '$user_id'");
		return $query->where('user_id', '=', $user_id);
	}
}
