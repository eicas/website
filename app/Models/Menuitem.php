<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menuitem extends Model {
	/**
	 * Define how deeply nested the menu can be. Level 0 is the root level.
	 */
	const DEEPEST_LEVEL = 2;

	protected $fillable = ['menuitem_id', 'order', 'label', 'href'];

	protected $casts = [
		'id' => 'integer',
		'menuitem_id' => 'integer',
		'order' => 'integer',
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
	];

	public function menuitem() {
		return $this->belongsTo(Menuitem::class);
	}

	public function menuitems() {
		return $this->hasMany(Menuitem::class)->orderBy('order');
	}
}
