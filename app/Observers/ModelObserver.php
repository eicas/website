<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Observers;

use App\Models\Event;
use App\Models\Contracts\ReportsDiff;
use Illuminate\Database\Eloquent\Model;

class ModelObserver {
	protected function storeEvent(Model $model, string $action) {
		$details = [];
		switch ($action) {
			case 'created':
				foreach ($model->getFillable() as $key) {
					$details[$key] = $model->$key;
				}
			break;
			case 'updated':
				$details = array_diff_key($model->getChanges(), ['updated_at' => null]);
			break;
			case 'deleted':
			break;
		}
		$event = Event::create([
			'user_id' => request()->user()->id ?? null,
			'model_type' => get_class($model),
			'model_id' => $model->id,
			'action' => $action,
			'details' => json_encode($details),
		]);
	}

	public function created(Model $model) {
		$this->storeEvent($model, 'created');
	}

	public function updated(Model $model) {
		$this->storeEvent($model, 'updated');
	}

	public function deleted(Model $model) {
		$this->storeEvent($model, 'deleted');
	}
}
