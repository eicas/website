<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Console\Commands;

use Illuminate\Foundation\Console\RouteListCommand as VendorCommand;
use Illuminate\Routing\Route;
use Illuminate\Routing\Router;

class RouteListCommand extends VendorCommand {
	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'List all registered routes with dedicated format';

	/**
	 * The table headers for the command.
	 *
	 * @var string[]
	 */
	protected $headers = ['Method', 'URI', 'Name', 'Action', 'Middleware'];

	/**
	 * Define the minimum width of any column in the output.
	 *
	 * The minimum width is defined as the width guaranteed to be reserved for any column.
	 * A column can be narrower if content allows.
	 */
	protected $min_width = 8;

	/**
	 * Specify maximum width of each column.
	 * If a column is missing, a default of 64 characters is used.
	 * Columns must be specified in lower case.
	 *
	 * @var int[]
	 */
	protected $max_widths = [
		'uri' => 32,
		'name' => 32,
		'action' => 66,
		'middleware' => 30,
	];

	/**
	 * Regexp filters by which (named) routes should be filtered.
	 * 
	 * Only NON-matching routes are listed.
	 */
	protected $name_filters = [
		'Barryvdh\Debugbar' => '/^debugbar\./',
	];

	/**
	 * Display notes below the route list table
	 * 
	 * @var string[]
	 */
	protected $notes = [];

	/**
	 * Create a new route command instance.
	 *
	 * @param \Illuminate\Routing\Router $router
	 * @return void
	 */
	public function __construct(Router $router) {
		parent::__construct($router);

		$total_width = self::getTerminalWidth();
		if ($total_width > 0) {
			$this->resetMaxWidths($total_width);
		}
		// set some overly large maximum values to ensure that every key is used
		$this->max_widths = array_merge([
			'method' => 128,
			'uri' => 128,
			'name' => 128,
			'action' => 128,
			'middleware' => 128,
		], $this->max_widths);
		// logger("maximum column widths:");
		// logger($this->max_widths);
	}

	protected static function getTerminalWidth(): int {
		try {
			$output = `stty -a`;
			// logger($output);
			$output = explode("\n", $output);
			// logger($output[0]);
			$output = explode(";", $output[0]);
			// logger("'" . $output[2] . "'");
			$output = intval(preg_replace('/[^0-9]/', '', $output[2]), 10);
			// logger("'" . $output . "'");
		} catch (\Exception $e) {
			return 0;
		}

		return $output;
	}

	/**
	 * Reset the values in the max_widths array to accomodate for the given terminal size.
	 *
	 * Each of the 5 cols require additional space for 'padding' and table borders:
	 * 1 space left of the content
	 * 1 space right of the content
	 * 1 'pipe' as border
	 *
	 * The minimum column width should be 8 positions for the value. Thus the minimum space
	 * needed is the sum of the widths of the values, plus (5 cols) * (2 padding) + 6 lines = 16.
	 *
	 */
	protected function resetMaxWidths(int $total_width) {
		$available = $total_width - 16;
		$priority = array_map(function ($width) {return $width - 8;}, $this->max_widths);
		$total = array_sum($priority);
		$priority = array_map(function ($width) use ($total) {
			return $width / $total;
		}, $priority);
		$priority = array_merge([
			'method' => 0,
			'uri' => 0,
			'name' => 0,
			'action' => 0,
			'middleware' => 0,
		], $priority);

		$this->max_widths = [
			'method' => $this->min_width,
			'uri' => $this->min_width,
			'name' => $this->min_width,
			'action' => $this->min_width,
			'middleware' => $this->min_width,
		];

		$used = array_sum($this->max_widths);
		// logger("available: $available; used: $used; prio:");
		// logger($priority);

		if ($used < $available) {
			foreach ($this->max_widths as $key => &$width) {
				// logger(sprintf(
				// 	"adding %.3f * %d = %.3f to current %d",
				// 	$priority[$key],
				// 	$available,
				// 	$available * $priority[$key],
				// 	$width
				// ));
				$width += ((int) floor(($available - $used) * $priority[$key]));
			}
		}
		// deal the remainder space
		$priority = array_filter($priority, function ($prio) {
			return $prio > 0;
		});
		arsort($priority);
		reset($priority);
		// logger("available: $available; used: " . array_sum($this->max_widths));
		// logger($this->max_widths);
		// logger("filtered priority order: " . implode(", ", array_keys($priority)));
		while (array_sum($this->max_widths) < $available) {
			$this->max_widths[key($priority)] += 1;
			// logger("added 1 space width for column '" . key($priority) . "'");
			if (!next($priority)) {
				reset($priority);
			}
			// logger("available: $available; used: " . array_sum($this->max_widths));
		}
		// logger($this->max_widths);
	}

	/**
	 * Execute the console command.
	 * 
	 * This is a copy from the extended class, with some added functionality.
	 *
	 * @return void
	 */
	public function handle() {
		if (empty($this->router->getRoutes())) {
			return $this->error("Your application doesn't have any routes.");
		}

		if (empty($routes = $this->getRoutes())) {
			return $this->error("Your application doesn't have any routes matching the given criteria.");
		}

		$this->displayRoutes($routes);

		$this->notes = array_values($this->notes);
		foreach ($this->notes as $index => $note) {
			$this->info(sprintf("Note %d: %s", $index + 1, $note));
		}
	}

	/**
	 * Compile the routes into a displayable format.
	 * 
	 * Overridden from Illuminate\Foundation\Console\RouteListCommand.
	 *
	 * @return array
	 */
	protected function getRoutes() {
		$routes = collect($this->filterRoutesByName($this->router->getRoutes()))->map(function ($route) {
			return $this->getRouteInformation($route);
		})->filter()->all();

		if ($sort = $this->option('sort')) {
			$routes = $this->sortRoutes($sort, $routes);
		}

		if ($this->option('reverse')) {
			$routes = array_reverse($routes);
		}

		return $this->pluckColumns($routes);
	}

	protected function filterRoutesByName($routes) {
		$filtered = [];
		foreach ($routes as $route) {
			$route_name = $this->getRouteName($route);
			$allow = true;
			foreach ($this->name_filters as $name => $filter) {
				if (preg_match($filter, $route_name)) {
					$allow = false;
					$this->notes["route-filtered-$name"] =
						"Some routes are not displayed because of filter '$name' ($filter)";
					break;
				}
			}
			if ($allow) {
				$filtered[] = $route;
			// 	logger("allowed '$route_name'");
			// } else {
			// 	logger("blocked '$route_name'");
			}
		}
		// logger("returning " . count($filtered) . " routes");
		return $filtered;
	}

	/**
	 * Get the route information for a given route.
	 *
	 * @param  \Illuminate\Routing\Route  $route
	 * @return array
	 */
	protected function getRouteInformation(Route $route) {
		return $this->filterRoute([
			'method' => implode('|', $route->methods()),
			'uri' => $this->getRouteUri($route),
			'name' => $this->getRouteName($route),
			'action' => $this->getRouteAction($route),
			'middleware' => $this->getRouteMiddleware($route),
		]);
	}

	/**
	 * Get a string representation of the route uri.
	 *
	 * The string is limited in length (wrapped if necessary).
	 *
	 *
	 */
	protected function getRouteUri($route) {
		return wordwrap(
			$route->uri(),
			$this->max_widths['uri'],
			"\n",
			true
		);
	}

	/**
	 * Get a string representation of the route name.
	 *
	 * The string is limited in length (wrapped if necessary).
	 *
	 *
	 */
	protected function getRouteName($route) {
		$name = $route->getName();
		if (!$name) {
			$name = '';
		}
		if (strpos($name, "generated::") === 0) {
			$name = "*" . substr($name, 11);
			$this->notes['name-generated'] = "Names starting with asterisk (*) were generated by route caching.";
		}
		return wordwrap(
			$name,
			$this->max_widths['name'],
			"\n",
			true
		);
	}

	/**
	 * Get a string representation of the route action.
	 *
	 * The string is limited in length (wrapped if necessary).
	 *
	 *
	 */
	protected function getRouteAction($route) {
		return wordwrap(
			ltrim($route->getActionName(), '\\'),
			$this->max_widths['action'],
			"\n",
			true
		);
	}

	/**
	 * Get before filters.
	 *
	 * @param  \Illuminate\Routing\Route  $route
	 * @return string
	 */
	protected function getRouteMiddleware($route) {
		return wordwrap(
			// collect($this->router->gatherRouteMiddleware($route))->map(function ($middleware) {
			collect($route->gatherMiddleware())->map(function ($middleware) {
				$middleware = (
					gettype($middleware) === 'object' && get_class($middleware) === 'Closure'
				) ? 'Closure' : $middleware;
				return str_replace('App\\Models\\', '', $middleware);
			})->implode(","),
			$this->max_widths['middleware'],
			"\n",
			true
		);
	}
}
