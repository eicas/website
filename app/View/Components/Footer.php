<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\View\Components;

use Illuminate\View\Component;

class Footer extends Component {
	/**
	 * Whether or not to show the Main footer
	 */
	public $showMain = true;

	/**
	 * Whether or not to show the User footer
	 */
	public $showUser = false;

	/**
	 * Create a new component instance.
	 *
	 * @return void
	 */
	public function __construct($showMain = 1, $showUser = 0) {
		$this->showMain = filter_var($showMain, FILTER_VALIDATE_BOOLEAN);
		$this->showUser = filter_var($showUser, FILTER_VALIDATE_BOOLEAN);
	}

	/**
	 * Get the view / contents that represent the component.
	 *
	 * @return \Illuminate\Contracts\View\View|\Closure|string
	 */
	public function render() {
		return view('components.footer');
	}
}
