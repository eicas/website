<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\View\Components;

use App\Helpers\Misc;
use App\Models\Image;
use Illuminate\View\Component;
use Str;

class MultiImagePicker extends Component {
	public $multi_image_picker_id;
	public $name;
	public $image_ids;
	public $images;

	/**
	 * Create a new component instance.
	 *
	 * @param string $name The name of the form input to use
	 * @param string $value The value of the form input, which is a comma-separated list of image ids
	 * @return void
	 */
	public function __construct($name, $value, $id = null) {
		if (is_null($id)) {
			$id = Str::uuid();
		}
		$this->multi_image_picker_id = $id;
		$this->name = $name;
		$this->images = Image::whereIn('id', Misc::commaSeparatedToIntegerArray($value))->get();
		$this->image_ids = $this->images->pluck('id')->all();
	}

	/**
	 * Get the view / contents that represent the component.
	 *
	 * @return \Illuminate\Contracts\View\View|\Closure|string
	 */
	public function render() {
		return view('components.multi-image-picker');
	}
}
