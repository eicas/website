<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\View\Components;

use App\Models\Image;
use Illuminate\View\Component;
use Str;

class ImagePicker extends Component {
	public $image_picker_id;
	public $name;
	public $image_id;
	public $image;

	/**
	 * Create a new component instance.
	 *
	 * @return void
	 */
	public function __construct($name, $value) {
		$this->image_picker_id = Str::uuid();
		$this->name = $name;
		$this->image_id = $value ? intval($value) : null;
		if ($this->image_id) {
			$this->image = Image::find($this->image_id);
			if (!$this->image) {
				$this->image_id = null;
			}
		}
	}

	/**
	 * Get the view / contents that represent the component.
	 *
	 * @return \Illuminate\Contracts\View\View|\Closure|string
	 */
	public function render() {
		return view('components.image-picker');
	}
}
