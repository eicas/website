<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
// SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>
//
// SPDX-License-Identifier: MIT

namespace App\View\Components;

use App\Models\Image as ImageModel;
use App\View\Components\Traits\ImageHandler;
use Illuminate\View\Component;

/**
 * This component is tightly connected to the Content component. It makes use of the same structure, specifically
 * the same view (components.content.image).
 */
class Image extends Component {
	use ImageHandler;

	public $extra = [];

	/**
	 * Create a new component instance.
	 *
	 * @param \App\Models\ImageModel $image
	 * @param array $options
	 * @return void
	 */
	public function __construct(ImageModel $image, array $options = []) {
		$this->extra['image'] = $image;
		$this->extra['image-classes'] = ['image-preview'];
		if (isset($options['image-before'])) {
			$this->extra['image-before'] = $options['image-before'];
		}
		if (isset($options['image-after'])) {
			$this->extra['image-after'] = $options['image-after'];
		}
		if (isset($options['image-popup-art'])) {
			$this->setupImagePopupArt($options['image-popup-art-parent']??[]);
		}
		if (isset($options['embed'])) {
			$this->extra['embed'] = $options['embed'];
		}
		// logger("loaded image component");
		// $logged = array_filter($this->extra, function ($k) { return $k !== 'image'; }, ARRAY_FILTER_USE_KEY);
		// if (count($logged)) {
		// 	logger('extra:');
		// 	logger($logged);
		// }
	}

	/**
	 * Get the view / contents that represent the component.
	 *
	 * @return \Illuminate\Contracts\View\View|\Closure|string
	 */
	public function render() {
		return view("components.content.image");
	}
}
