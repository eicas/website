<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\View\Components;

use App\Models\Menuitem;
use Illuminate\View\Component;
use Illuminate\Support\Str;

class Menu extends Component {
	public $menu = [];

	/**
	 * Create a new component instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$with = [];
		for ($i = 0; $i < Menuitem::DEEPEST_LEVEL; $i++) {
			$with[] = implode(".", array_fill(0, $i + 1, 'menuitems'));
		}
		$this->menu = Menuitem::with($with)->orderBy('order')->whereNull('menuitem_id')->get();
	}

	/**
	 * Get the view / contents that represent the component.
	 *
	 * @return \Illuminate\Contracts\View\View|\Closure|string
	 */
	public function render() {
		return view('components.menu');
	}
}
