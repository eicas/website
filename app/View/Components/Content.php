<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
// SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>
//
// SPDX-License-Identifier: MIT

namespace App\View\Components;

use App\Helpers\Misc;
use App\Models\Content as ContentModel;
use App\Models\Download;
use App\Models\GuestForms\NewFriend;
use App\Models\Image;
use App\View\Components\Traits\ImageHandler;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\Component;

/**
 * The constructor takes into account the component type. These types are defined in the file(s)
 * /resources/lang/<language-code>/content.php
 */
class Content extends Component {
	use ImageHandler;

	/**
	 * The content model instance
	 */
	public $content;

	/**
	 * Optional extra information, depending on the content type
	 */
	public $extra;

	/**
	 * Some components are session-aware and require different sub-views depending on state
	 */
	protected $sub_view = null;
	
	/**
	 * Create a new component instance.
	 *
	 * @param \App\Models\Content $content
	 * @return void
	 */
	public function __construct(ContentModel $content, array $options = []) {
		$this->content = $content;
		$this->extra = [];
		switch ($this->content->type) {
			case 'text':
			break;
			case 'image':
				$this->extra['image'] = Image::find(intval($this->content->content, 10));
				$this->extra['image-classes'] = ['image-preview'];
				if (isset($options['image-before'])) {
					$this->extra['image-before'] = $options['image-before'];
				}
				if (isset($options['image-after'])) {
					$this->extra['image-after'] = $options['image-after'];
				}
				if (isset($options['image-popup-art'])) {
					$this->setupImagePopupArt();
				}
			break;
			case 'youtube':
				$details = explode(",", $this->content->content);
				if (count($details) !== 1) {
					$this['error'] = "De Youtube-widget kan niet worden geladen.";
				} else {
					$this->extra['youtube_id'] = trim($details[0]);
				}
				if ($this->extra['youtube_id']) {
					$this->ensureYoutuebPreviewImage($this->extra['youtube_id']);
				}
			break;
			case 'bandcamp':
				$details = explode(",", $this->content->content);
				if (count($details) !== 3) {
					$this['error'] = "De Bandcamp-widget kan niet worden geladen.";
				} else {
					$this->extra['artist'] = trim($details[0]);
					$this->extra['album'] = trim($details[1]);
				}
				$image_url = trim($details[2]);
				if ($image_url) {
					$this->ensureBandcampPreviewImage($this->extra['album'], $image_url);
				}
			break;
			case 'lvp-tickets':
				$details = explode(",", $this->content->content);
				if (count($details) !== 3) {
					$this['error'] = "De kalender-widget kan niet worden geladen.";
				} else {
					$this->extra['server_id'] = trim($details[0]);
					$this->extra['contentholder_id'] = trim($details[1]);
					$this->extra['production_id'] = trim($details[2]);
				}
			break;
			case 'mc-newsletterform':
				$details = explode(",", $this->content->content);
				if (count($details) !== 3) {
					$this['error'] = "De nieuwsbrief-widget kan niet worden geladen.";
				} else {
					$this->extra['action'] =
						'https://' . trim($details[0]) . '.list-manage.com/subscribe/post?u=' .
						trim($details[1]) . '&id=' . trim($details[2]);
					$this->extra['hidden_name'] = 'b_' . trim($details[1]) . '_' . trim($details[2]);
				}
			break;
			case 'form-new-friend':
				$status = session('forms.new-friend', [
					'step' => 'start',
				]);
				switch ($status['step']) {
					case 'mollie':
						// user returned from the mollie payment
						// check the payment (function might flash errors to the session)
						if (self::checkMolliePayment(
							$status['newFriend_id'] ?? 0,
							$status['payment_reference'] ?? '',
							$status['payment_id'] ?? '',
						)) {
							$this->sub_view = 'thanks';
						} else {
							$this->sub_view = 'failed';
						}
						session()->forget('forms.new-friend');
					break;
					default: // (also: step==='start')
						$this->sub_view = 'form';
				}
			break;
			case 'shop-item':
				$image_id = 0;
				if (preg_match('/^\[(.*)\]/', $this->content->content, $matches)) {
					$image_id = intval($matches[1], 10);
					if ($image_id > 0) {
						$this->extra['image'] = Image::find($image_id);
					}
				}
			break;
		}
		// logger("loaded content component for type='{$this->content->type}'");
	}

	/**
	 * Get the view / contents that represent the component.
	 *
	 * @return \Illuminate\Contracts\View\View|\Closure|string
	 */
	public function render() {
		$view = "components.content.{$this->content->type}";
		if ($this->sub_view) {
			$view .= ".{$this->sub_view}";
		}
		return view($view);
	}

	public function getTextContent(): string {
		$text = $this->content->content ?? '';
		switch ($this->content->type) {
			case 'shop-item':
				$text = preg_replace('/^\[.*\]/', '', $text);
			break;
		}
		$downloads = [];
		if (preg_match('/\[DOWNLOAD:[0-9]+\]/', $text, $matches)) {
			$downloads = Download::get()->keyBy('id');
		}
		$text = preg_replace_callback(
			'/<a href=\"([^"]+)"/',
			function ($matches) {
				if (str_starts_with($matches[1], '/')
					|| str_starts_with($matches[1], 'https://www.eicas.nl')
					|| str_starts_with($matches[1], 'mailto')) {
					return sprintf('<a href="%s"', $matches[1]);
				} else if (str_starts_with(strtolower($matches[1]), 'https://eicas.nl')) {
					// Specific case which changes 'https://eicas.nl' to 'https://www.eicas.nl'
					$pos = strpos(strtolower($matches[1]), 'eicas.nl');
					// The last argument, the 0, in substr_replace() means the string 'www.' is inserted instead 
					// of replacing other characters
					$updated_url = substr_replace($matches[1], 'www.', $pos, 0);				
					return sprintf('<a href="%s"', $updated_url);
				} else {
					return sprintf('<a href="%s" target="_blank"', $matches[1]);
				}
			},
			$text
		);
		$text = preg_replace_callback(
			'/\[DOWNLOAD:([0-9]+)\]/',
			function ($matches) use ($downloads) {
				if (!$downloads->has($matches[1])) {
					return '<em>download niet beschikbaar</em>';
				}
				return sprintf(
					'<a href="%s" target="_blank">%s (%s, %s)</a>',
					route('download', [
						'filename' => $downloads[$matches[1]]->filename
					]),
					$downloads[$matches[1]]->filename,
					$downloads[$matches[1]]->extension,
					$downloads[$matches[1]]->formatted_size
				);
			},
			$text
		);
		return $text;
	}

	/**
	 * Cache the youtube image previews so that they can be served from this application instead of from the Youtube
	 * servers, which would be a privacy issue.
	 * 
	 * First, check whether the preview already exists and is young enough
	 */
	protected function ensureYoutuebPreviewImage(string $youtube_id) {
		// logger("ensuring youtube preview image for id='{$youtube_id}'");
		$max_age = config('cache.settings.youtube_preview.max_age', 86400);
		$age = PHP_INT_MAX;
		if (Storage::disk('youtube_preview')->exists("{$youtube_id}.jpg")) {
			$age = time() - Storage::disk('youtube_preview')->lastModified("{$youtube_id}.jpg");
			// logger("youtube preview image found ('{$youtube_id}.jpg'), age {$age}s");
		}

		if ($age > $max_age) {
			$url = sprintf(config('cache.settings.youtube_preview.preview_url'), $youtube_id);
			// logger("youtube preview is too old or not present, fetch it from YT server: '$url'");
			$response = Http::timeout(3)->get($url);
			if ($response->successful()) {
				$image = $response->body();
				Storage::disk('youtube_preview')->put("{$youtube_id}.jpg", $image);
				// logger("image stored");
			}
		}
	}

	/**
	 * Cache the bandcamp image previews so that they can be served from this application instead of from the Bandcamp
	 * servers, which would be a privacy issue.
	 *
	 * First, check whether the preview already exists and is young enough
	 */
	protected function ensureBandcampPreviewImage(string $album, string $url) {
		$max_age = config('cache.settings.bandcamp_preview.max_age', 86400);
		$age = PHP_INT_MAX;
		if (Storage::disk('bandcamp_preview')->exists("{$album}.jpg")) {
			$age = time() - Storage::disk('bandcamp_preview')->lastModified("{$album}.jpg");
		}

		if ($age > $max_age) {
			$response = Http::timeout(3)->get($url);
			if ($response->successful()) {
				$image = $response->body();
				Storage::disk('bandcamp_preview')->put("{$album}.jpg", $image);
			}
		}
	}


	/**
	 * This function checks a Mollie payment.
	 * 
	 * Three identifiers should be provided:
	 * - db-id of the newFriend instance
	 * - payment reference
	 * - id of Mollie payment
	 * 
	 * The newFriend instance and Mollie payment are fetched and the three-fold information is matched. If anything
	 * could not be fully matched, false is returned.
	 * 
	 * Finally, this function returns wether the payment was fulfilled ('paid').
	 * 
	 * @param integer $newFriend_id
	 * @param string $reference
	 * @param string $payment_id
	 * @return boolean
	 */
	protected static function checkMolliePayment(int $newFriend_id, string $reference, string $payment_id): bool {
		if ($newFriend_id === 0 || $reference === '' || $payment_id === '') {
			info("Could not check Mollie payment. nFr: $newFriend_id; ref: '$reference'; p_id: '$payment_id'");
			session()->flash('error', 'De juiste referenties konden niet worden gevonden.');
			return false;
		}
		// all information is present

		$newFriend = NewFriend::find($newFriend_id);
		if (!$newFriend) {
			info("Could not find newFriend instance with id=$newFriend_id");
			session()->flash('error', 'Uw registratie kon niet worden gevonden.');
			return false;
		}
		$mollie = mollie();
		if ($newFriend->payment_test) {
			$mollie->setApiKey(config('mollie.key_test'));
		}
		$payment = $mollie->payments->get($payment_id);
		if (!$payment) {
			info("Could not find Mollie payment with id='$payment_id'");
			session()->flash('error', 'Uw betaling kon niet worden gevonden.');
			return false;
		}
		// objects are fetched

		if ($newFriend->payment_reference !== $reference) {
			info("Could not match reference (found: '{$newFriend->payment_reference}')");
			session()->flash('error', 'De betalings-referentie is onjuist.');
			return false;
		}
		if ($newFriend->payment_id !== $payment_id) {
			info("Could not match payment id (found: '{$newFriend->payment_id}')");
			session()->flash('error', 'De betalings-identifier is onjuist.');
			return false;
		}
		if ($payment->metadata->reference !== $reference) {
			info("Could not match payment reference with payment (found: '{$payment->metadata['reference']}')");
			session()->flash('error', 'De betalings-referentie komt niet overeen.');
			return false;
		}
		// references match all

		return $payment->isPaid();
	}
}
