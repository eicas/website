<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\View\Components\Traits;

trait ImageHandler {
	protected function setupImagePopupArt(array $parent = []) {
		$this->extra['image-classes'][] = 'image-popup-art';
		$this->extra['image']->load('large');
		if (!($popupImage = $this->extra['image']->large)) {
			$popupImage = $this->extra['image'];
		}
		$image = $this->extra['image'];
		$title = $image->pivot->alt ?? implode(", ", array_filter([$image->title, $image->artist, $image->year]));
		$this->extra['image-popup-art'] = [
			'hs' => str_replace('"', "'", json_encode($popupImage->heights, JSON_FORCE_OBJECT + JSON_NUMERIC_CHECK)),
			'pt' => $popupImage->getUrlTemplate(),
			'alt' => $popupImage->alt,
			'title' => $title,
			'extra' => $popupImage->info,
		];
		if (count($parent)) {
			$this->extra['image-popup-art']["parent-selector"] = true;
			foreach ($parent as $key => $value) {
				$this->extra['image-popup-art']["parent-$key"] = $value;
			}
		}
	}
}