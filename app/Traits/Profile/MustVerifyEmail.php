<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Traits\Profile;

use App\Notifications\Profile\VerifyEmail;
use Illuminate\Auth\MustVerifyEmail as BaseTrait;

trait MustVerifyEmail {
	use BaseTrait;

	/**
	 * Send the email verification notification.
	 * 
	 * This function overrides the one in the base trait. The difference is that another VerifyEmail is used.
	 *
	 * @return void
	 */
	public function sendEmailVerificationNotification() {
		$this->notify(new VerifyEmail);
	}
}
