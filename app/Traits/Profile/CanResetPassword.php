<?php

// SPDX-FileCopyrightText: 2022 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Traits\Profile;

use App\Notifications\Profile\PasswordResetLinkEmail;

/**
 * Copied from \Illuminate\Auth\Passwords\CanResetPassword
 * Use a custom notifier
 */
trait CanResetPassword {
	/**
	 * Get the e-mail address where password reset links are sent.
	 *
	 * @return string
	 */
	public function getEmailForPasswordReset() {
		return $this->email;
	}

	/**
	 * Send the password reset notification.
	 *
	 * @param string $token
	 * @return void
	 */
	public function sendPasswordResetNotification($token) {
		$this->notify(new PasswordResetLinkEmail($token));
	}
}
