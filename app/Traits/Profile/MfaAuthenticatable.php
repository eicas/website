<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Traits\Profile;

use BaconQrCode\Renderer\Color\Rgb;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\Image\SvgImageBackEnd;
use BaconQrCode\Renderer\RendererStyle\Fill;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use PragmaRX\Google2FA\Google2FA;

trait MfaAuthenticatable {
	/**
	 * The underlying library providing two factor authentication helper services.
	 *
	 * @var \PragmaRX\Google2FA\Google2FA
	 */
	protected $engine;

	// /**
	//  * Create a new two factor authentication provider instance.
	//  *
	//  * @param \PragmaRX\Google2FA\Google2FA $engine
	//  * @return void
	//  */
	// public function __construct(Google2FA $engine) {
	// 	$this->engine = $engine;
	// }
	/**
	 *
	 * @return void
	 */
	public function __construct($attributes = []) {
		parent::__construct($attributes);
		$this->engine = app()->make(Google2FA::class);
	}

	/**
	 * Get the user's two factor authentication recovery codes.
	 *
	 * @return array
	 */
	public function recoveryCodes() {
		return json_decode(decrypt($this->mfa_recovery_codes), true);
	}

	/**
	 * Replace the given recovery code with a new one in the user's stored codes.
	 *
	 * @param string $code
	 * @return void
	 */
	public function replaceRecoveryCode($code) {
		$this->forceFill([
			'mfa_recovery_codes' => encrypt(str_replace(
				$code,
				self::generateRecoveryCode(),
				decrypt($this->mfa_recovery_codes)
			)),
		])->save();
	}

	/**
	 * Generate a new recovery code.
	 *
	 * @return string
	 */
	public static function generateRecoveryCode(): string {
		return Str::random(10) . '-' . Str::random(10);
	}

	/**
	 * Get the QR code SVG of the user's MFA QR code URL.
	 *
	 * @return string
	 */
	public function getQrCodeSvg() {
		$svg = (new Writer(
			new ImageRenderer(
				new RendererStyle(192, 0, null, null, Fill::uniformColor(new Rgb(255, 255, 255), new Rgb(1, 1, 1))),
				new SvgImageBackEnd
			)
		))->writeString($this->twoFactorQrCodeUrl());

		return trim(substr($svg, strpos($svg, "\n") + 1));
	}

	/**
	 * Get the MFA QR code URL.
	 *
	 * @return string
	 */
	public function getQrCodeUrl() {
		return $this->qrCodeUrl(
			config('app.name') . ' website',
			$this->email,
			decrypt($this->mfa_secret)
		);
	}

	/**
	 * Generate a new secret key.
	 *
	 * @return string
	 */
	public function generateSecretKey() {
		return $this->engine->generateSecretKey();
	}

	/**
	 * Get the two factor authentication QR code URL.
	 *
	 * @param string $companyName
	 * @param string $companyEmail
	 * @param string $secret
	 * @return string
	 */
	protected function qrCodeUrl($companyName, $companyEmail, $secret) {
		return $this->engine->getQRCodeUrl($companyName, $companyEmail, $secret);
	}

	/**
	 * Verify the given code.
	 *
	 * @param string $secret
	 * @param string $code
	 * @return bool
	 */
	public function verify($secret, $code) {
		return $this->engine->verifyKey($secret, $code);
	}
}
