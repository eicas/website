<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Helpers;

class Misc {
	/**
	 * Binary prefix symbols (in order, in steps of a factor 1024).
	 */
	const BINARY_PREFIXES = [
		'',
		'Ki',
		'Mi',
		'Gi',
		'Ti',
		'Pi',
		'Ei',
		'Zi',
		'Yi',
	];

	/**
	 * Decimal prefix symbols (in order, in steps of a factor 1000).
	 */
	const DECIMAL_PREFIXES = [
		'',
		'K',
		'M',
		'G',
		'T',
		'P',
		'E',
		'Z',
		'Y',
	];

	/**
	 * Return a string representation of the given filesize.
	 * 
	 * Using the precision parameter (default: 1), the number of decimals can be varied between 0 and 3.
	 * 
	 * The boolean $metric is used to choose between binary prefixes (Ki, Mi, etc.) (false) or decimal prefixes
	 * (K, M, etc.) (true).
	 * 
	 * A $separator can be provided which will be placed (verbatim) between the number and the unit.
	 * 
	 * The default unit is "B" (bytes). This may be changed by providing a different $unit.
	 * 
	 * @param integer $bytes the size to represent
	 * @param integer $precision (Default: 1)
	 * @param boolean $metric (Default: false)
	 * @param string $separator (Default: "")
	 * @param string $unit (Default: "B")
	 * @return string
	 */
	public static function filesize(int $bytes, int $precision = 1, bool $metric = false, string $separator = "", string $unit = "B") {
		// input validation
		// precision should be between 0 and 3. Lower bound is implicit in the implementation. Force upper bound.
		if ($precision > 3) {
			$precision = 3;
		}
		// convert to float to aid precision
		$value = (float) $bytes;
		$prefix = 0;
		$base = $metric ? 1000 : 1024;
		while ($prefix < 8 && $value >= $base) {
			$prefix++;
			$value /= $base;
		}
		if ($precision < 1) {
			return sprintf(
				"%d%s%s",
				$value,
				$metric ? self::DECIMAL_PREFIXES[$prefix] : self::BINARY_PREFIXES[$prefix],
				$unit
			);
		}
		return sprintf(
			"%.{$precision}f%s%s%s",
			$value,
			$separator,
			$metric ? self::DECIMAL_PREFIXES[$prefix] : self::BINARY_PREFIXES[$prefix],
			$unit
		);
	}

	/**
	 * Convert the given comma-separated list of id-values into an array of integers.
	 * 
	 * Only valid ids are returned, other 'values' are discarded. Note that any integer value of 1 or larger is
	 * accepted as a valid value. Conversion is performed with the intval function (base 10).
	 * 
	 * @param string $value The comma-separated list of ids
	 * @return integer[] Zero or more integers.
	 */
	public static function commaSeparatedToIntegerArray(string $value): array {
		$result = [];
		$values = explode(",", $value);
		foreach ($values as $value) {
			$value = intval($value, 10);
			if ($value > 0) {
				$result[] = $value;
			}
		}
		return $result;
	}

	/**
	 * Aid nextInteger function.
	 */
	private static $next_integer = 1;

	/**
	 * Get a subsequent integer on each call (within one process), starting at 1.
	 * 
	 * @return integer
	 */
	public static function nextInteger(): int {
		static $next_integer = 1;
		return $next_integer++;
	}

	/**
	 * Return a time representation of the given number of seconds.
	 * 
	 * Depending on the amount of time, the format is:
	 * DdHH:MM:SS
	 * H:MM:SS
	 * MM:SS
	 * S
	 * where capital letters represent the numbers of days, hours, minutes and seconds. Where double letters are used,
	 * the number is zero-left-padded to two positions where necessary.
	 * 
	 * If the number of seconds is negative, a minus sign is prefixed to the output.
	 * 
	 * @param integer $seconds
	 * @return string
	 */
	public static function timeFromSeconds(int $seconds): string {
		$sign = ($seconds < 0) ? -1 : 1;
		$seconds = abs($seconds);
		$s = $seconds % 60;
		$m = intdiv($seconds, 60);
		$h = intdiv($m, 60);
		$m = $m % 60;
		$d = intdiv($h, 24);
		$h = $h % 24;
		if ($d > 0) {
			return sprintf("%dd%02d:%02d:%02d", $sign * $d, $h, $m, $s);
		}
		if ($h > 0) {
			return sprintf("%d:%02d:%02d", $sign * $h, $m, $s);
		}
		if ($m > 0) {
			return sprintf("%02d:%02d", $sign * $m, $s);
		}
		return sprintf("%ds", $sign * $s);
	}

	/**
	 * Bluntly remove various parts of the given user agent string, so that it becomes much shorter.
	 * 
	 * @param string User agent string
	 * @return string
	 */
	public static function shortUserAgent(string $ua): string {
		$ua = preg_replace('/[^a-zA-Z(), ]/', '', $ua);
		$ua = preg_replace('/\b[a-zA-Z]{1,3}\b/', '', $ua);
		$ua = preg_replace('/\s\s+/', ' ', $ua);
		$ua = str_replace(['( ', ' )'], ['(', ')'], $ua);
		return $ua;
	}

	/**
	 * Combine array elements using a separator, but use a different separator between the last two elements.
	 * 
	 * This function can be used as a drop-in replacement of the PHP core function implode.
	 * 
	 * @param array
	 * @return string
	 */
	public static function implode(string $glue, array $parts, $last_glue = null): string {
		if (!$last_glue) {
			$last_glue = $glue;
		}
		if (count($parts) === 0) {
			return "";
		}
		if (count($parts) <= 2) {
			return implode($last_glue, $parts);
		}
		$last_part = array_pop($parts);
		return implode($glue, $parts) . $last_glue . $last_part;
	}

	/**
	 * Modify a path in the public area of the website by appending a cache busting parameter.
	 * 
	 * Example: '/img/logo-eicas.png' yields: '/img/logo-eicas.png?v=1676972847'
	 * 
	 * The version ('v') parameter value is simply the mtime of the given file as found in the public dir.
	 * 
	 * @param string $path
	 * @return string
	 */
	public static function getCacheablePublicPath(string $path): string {
		// Note: filemtime emits a E_WARNING if the file is not found but does not throw an exception (php8)
		return $path . "?v=" . filemtime(public_path(ltrim($path, '/')));
	}
}
