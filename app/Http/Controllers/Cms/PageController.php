<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers\Cms;

use App\Models\Imageset;
use App\Models\Page;
use App\Http\Controllers\Controller;
use App\Http\Requests\Cms\PageRequest;

class PageController extends Controller {
	public function __construct() {
		$this->authorizeResource(Page::class, 'page');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$pages = Page::with('imageset')->get();
		$imagesets = Imageset::get();

		return view('cms.pages.index', ['pages' => $pages, 'imagesets' => $imagesets]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \App\Http\Requests\Cms\PageRequest $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(PageRequest $request) {
		$page = Page::create($request->validated());
		return redirect()->route('cms.pages.edit', ['page' => $page]);
	}

	/**
	 * Display the specified resource.
	 * 
	 * This route is used for previewing a page by editors.
	 *
	 * @param \App\Models\Page $page
	 * @return \Illuminate\Http\Response
	 */
	public function show(Page $page) {
		$template = $page->template;
		if (!view()->exists("templates.$template")) {
			$template = 'story';
		}
		$page->load('imageset');
		return view("templates.$template", ['page' => $page]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \App\Models\Page $page
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Page $page) {
		$page->load('imageset');
		$imagesets = Imageset::withCount('images')->get();
		return view('cms.pages.edit', ['page' => $page, 'imagesets' => $imagesets]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \App\Http\Requests\Cms\PageRequest $request
	 * @param \App\Models\Page $page
	 * @return \Illuminate\Http\Response
	 */
	public function update(PageRequest $request, Page $page) {
		$page->fill($request->validated());
		$page->save();
		return redirect()->route('cms.pages.edit', ['page' => $page]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\Models\Page $page
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Page $page) {
		$page->delete();
		return redirect()->route('cms.pages.index');
	}
}
