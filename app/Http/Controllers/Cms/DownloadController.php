<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Http\Requests\Cms\StoreDownloadRequest;
use App\Http\Requests\Cms\UpdateDownloadRequest;
use App\Models\Download;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DownloadController extends Controller {
	public function __construct() {
		$this->authorizeResource(Download::class, 'download');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return view('cms.downloads.index', ['downloads' => Download::orderBy('created_at', 'desc')->get()]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \App\Http\Requests\Cms\StoreDownloadRequest $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreDownloadRequest $request) {
		$download = Download::create([
			'filename' => $request->validated()['file']->getClientOriginalName(),
			'size' => $request->validated()['file']->getSize(),
		]);
		$request->validated()['file']->storeAs(
			'', // root path
			$download->getStoragePathAttribute(), // generate filename, based on timestamp and id
			'downloads' // disk
		);

		return redirect()->route('cms.downloads.index', ['uploaded_id' => $download->id]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \App\Models\Download $download
	 * @return \Illuminate\Http\Response
	 */
	public function show(Download $download) {
		return $download->downloadResponse();
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \App\Http\Requests\Cms\UpdateDownloadRequest $request
	 * @param \App\Models\Download $download
	 * @return \Illuminate\Http\Response
	 */
	public function update(UpdateDownloadRequest $request, Download $download) {
		$download->filename = $request->validated()['filename'];
		$download->save();

		return redirect()->route('cms.downloads.index', ['uploaded_id' => $download->id]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\Models\Download $download
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Download $download) {
		Storage::disk('downloads')->delete($download->getStoragePathAttribute());
		$download->delete();
		return redirect()->route('cms.downloads.index');
	}
}
