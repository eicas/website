<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers\Cms\Imagesets;

use App\Http\Controllers\Controller;
use App\Models\Imageset;
use App\Models\Pivots\ImageImageset;

class SwapImageImagesetController extends Controller {
	/**
	 * Swap the given imageImagesets (for the given Imageset).
	 *
	 * Note that the ImageImageset is a pivot model which doesn't load properly from the service container. Therefore,
	 * it is inserted as integer and resolved in this function.
	 * 
	 * @param \App\Models\Imageset $imageset
	 * @param integer $imageImageset1
	 * @param integer $imageImageset2
	 * @return \Illuminate\Http\Response
	 */
	public function __invoke(Imageset $imageset, int $imageImageset1, int $imageImageset2) {
		$imageImageset1 = ImageImageset::where('imageset_id', $imageset->id)->findOrFail($imageImageset1);
		$imageImageset2 = ImageImageset::where('imageset_id', $imageset->id)->findOrFail($imageImageset2);

		$tmp_order = $imageImageset1->order;
		$imageImageset1->order = $imageImageset2->order;
		$imageImageset2->order = $tmp_order;
		$imageImageset1->save();
		$imageImageset2->save();

		return redirect()->route('cms.imagesets.edit', ['imageset' => $imageset]);
	}
}
