<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers\Cms\Imagesets;

use App\Helpers\Misc;
use App\Http\Controllers\Controller;
use App\Http\Requests\Cms\AddImagesToImagesetRequest;
use App\Http\Requests\Cms\UpdateImageOnImagesetRequest;
use App\Models\Image;
use App\Models\Imageset;
use App\Models\Pivots\ImageImageset;
use Illuminate\Http\Request;

class ImageImagesetController extends Controller {
	/**
	 * Handle the incoming request.
	 *
	 * @param \App\Http\Requests\Cms\AddImagesToImagesetRequest $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(AddImagesToImagesetRequest $request, Imageset $imageset) {
		$imageset->load('images');
		$max_order = $imageset->images->last()->pivot->order ?? 0;
		$image_ids = Image::whereIn(
			'id',
			Misc::commaSeparatedToIntegerArray($request->image_ids ?? '')
		)->get()->pluck('id')->all();
		if (count($image_ids)) {
			$attach = [];
			foreach ($image_ids as $image_id) {
				$attach[$image_id] = [
					'order' => ++$max_order,
				];
			}
			$imageset->images()->attach($attach);
		}
		return redirect($request->redirect_url ?? route('cms.imagesets.edit', ['imageset' => $imageset]));
	}

	/**
	 * Show an edit form for the given imageset and imageimageset.
	 * 
	 * Note that the ImageImageset is a pivot model which doesn't load properly from the service container. Therefore,
	 * it is inserted as integer and resolved in this function.
	 * 
	 * @param \App\Models\Imageset $imageset
	 * @param integer $imageImageset
	 */
	public function edit(Imageset $imageset, int $imageImageset) {
		$imageImageset = ImageImageset::with('image')->findOrFail($imageImageset);
		return view('cms.imagesets.imageimagesets.edit', ['imageset' => $imageset, 'imageImageset' => $imageImageset]);
	}

	/**
	 * Update the given imageimageset.
	 * 
	 * Note that the ImageImageset is a pivot model which doesn't load properly from the service container. Therefore,
	 * it is inserted as integer and resolved in this function.
	 * 
	 * @param \App\Http\Requests\Cms\UpdateImageOnImagesetRequest $request
	 * @param \App\Models\Imageset $imageset
	 * @param integer $imageImageset
	 */
	public function update(UpdateImageOnImagesetRequest $request, Imageset $imageset, int $imageImageset) {
		$imageImageset = ImageImageset::findOrFail($imageImageset);
		$imageImageset->update([
			'href' => $request->href ?? null,
			'alt' => $request->alt ?? null,
		]);
		$imageImageset->save();
		return redirect()->route('cms.imagesets.imageimagesets.edit', ['imageset' => $imageset, 'imageimageset' => $imageImageset]);
	}

	/**
	 * Delete the given imageimageset.
	 * 
	 * Note that the ImageImageset is a pivot model which doesn't load properly from the service container. Therefore,
	 * it is inserted as integer and resolved in this function.
	 * 
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Models\Imageset $imageset
	 * @param integer $imageImageset
	 */
	public function destroy(Request $request, Imageset $imageset, int $imageImageset) {
		$imageImageset = ImageImageset::findOrFail($imageImageset);
		$imageImageset->delete();

		return redirect()->route('cms.imagesets.edit', ['imageset' => $imageset]);
	}
}
