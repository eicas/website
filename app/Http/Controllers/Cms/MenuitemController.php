<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Http\Requests\Cms\StoreMenuitemRequest;
use App\Http\Requests\Cms\UpdateMenuitemRequest;
use App\Http\Requests\Cms\DeleteMenuitemRequest;
use App\Models\Menuitem;

class MenuitemController extends Controller {
	public function __construct() {
		$this->authorizeResource(Menuitem::class, 'menuitem');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$with = [];
		for ($i = 0; $i < Menuitem::DEEPEST_LEVEL; $i++) {
			$with[] = implode(".", array_fill(0, $i + 1, 'menuitems'));
		}
		$menu = Menuitem::with($with)->orderBy('order')->whereNull('menuitem_id')->get();

		return view('cms.menuitems.index', ['menu' => $menu]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \App\Http\Requests\Cms\StoreMenuitemRequest $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreMenuitemRequest $request) {
		Menuitem::create(array_merge(
			$request->validated(),
			[
				'order' => 1 + Menuitem::where('menuitem_id', $request->validated()['menuitem_id'])->max('order'),
			]
		));
		return redirect()->route('cms.menuitems.index');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \App\Http\Requests\Cms\UpdateMenuitemRequest $request
	 * @param \App\Models\Menuitem $menuitem
	 * @return \Illuminate\Http\Response
	 */
	public function update(UpdateMenuitemRequest $request, Menuitem $menuitem) {
		$menuitem->fill($request->validated());
		$menuitem->save();
		return redirect()->route('cms.menuitems.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\Http\Requests\Cms\DeleteMenuitemRequest $request
	 * @param \App\Models\Menuitem $menuitem
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(DeleteMenuitemRequest $request, Menuitem $menuitem) {
		$menuitem->delete();
		return redirect()->route('cms.menuitems.index');
	}
}
