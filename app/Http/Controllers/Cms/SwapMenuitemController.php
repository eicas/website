<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers\Cms;

use App\Http\Requests\Cms\SwapMenuitemsRequest;
use App\Models\Menuitem;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SwapMenuitemController extends Controller {
	/**
	 * Handle the incoming request.
	 *
	 * @param \App\Http\Requests\SwapMenuitemsRequest $request
	 * @param \App\Models\Menuitem $menuitem1
	 * @param \App\Models\Menuitem $menuitem2
	 * @return \Illuminate\Http\Response
	 */
	public function __invoke(SwapMenuitemsRequest $request, Menuitem $menuitem1, Menuitem $menuitem2) {
		$tmp_order = $menuitem1->order;
		$menuitem1->order = $menuitem2->order;
		$menuitem2->order = 0;
		$menuitem2->save();
		$menuitem1->save();
		$menuitem2->order = $tmp_order;
		$menuitem2->save();

		return redirect()->route('cms.menuitems.index');
	}
}
