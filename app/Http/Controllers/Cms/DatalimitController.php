<?php

// SPDX-FileCopyrightText: 2022 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class DatalimitController extends Controller {
	/**
	 * Handle the incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function __invoke(Request $request) {
		if (!$request->user()->hasRole('admin')) {
			return response('niet toegestaan', 403);
		}
		require base_path('datalimit_config.php');
		$settings = [
			'period' => $period,
			'max_bytes' => $max_bytes,
			'suspend_duration' => $suspend_duration,
		];
		$start_suspends = (new Carbon())->subMonths(6);
		$suspends = DB::connection('datalimit')->select(
			"select `starts_at`, `ends_at`, `comment` from `suspends` where `ends_at` > ? order by `id` desc",
			[$start_suspends->format('Y-m-d H:i:s')]
		);
		$start_visits = (new Carbon())->subDays(14);
		$start_visits->setSecond(0);
		$start_visits->setMinute(15 * (floor($start_visits->minute / 15)));
		$visits = [];
		foreach (DB::connection('datalimit')->select(
			"select" .
			" from_unixtime(900 * floor(unix_timestamp(`visited_at`) / 900)) p," .
			" count(size) n," .
			" sum(size) d" .
			" from visits" .
			" where `visited_at` >= '{$start_visits->format('Y-m-d H:i:s')}'" .
			" group by p" .
			" order by date(`visited_at`) desc, `visited_at`"
		) as $visit) {
			$day = substr($visit->p, 0, 10);
			if (!array_key_exists($day, $visits)) {
				$visits[$day] = [];
			}
			$visits[$day][] = $visit;
		}

		return view('cms.datalimit.index', [
			'settings' => collect($settings),
			'suspends' => collect($suspends),
			'visits' => collect($visits),
		]);
	}
}
