<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Http\Requests\Cms\StoreRedirectRequest;
use App\Http\Requests\Cms\UpdateRedirectRequest;
use App\Models\Redirect;
use Illuminate\Http\Request;

class RedirectController extends Controller {
	public function __construct() {
		$this->authorizeResource(Redirect::class, 'redirect');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return view('cms.redirects.index', ['redirects' => Redirect::orderBy('from')->get()]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \App\Http\Requests\Cms\StoreRedirectRequest $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreRedirectRequest $request) {
		$redirect = Redirect::create($request->validated());
		return redirect()->route('cms.redirects.index', ['updated_id' => $redirect->id]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \App\Http\Requests\Cms\UpdateRedirectRequest $request
	 * @param \App\Models\Redirect $redirect
	 * @return \Illuminate\Http\Response
	 */
	public function update(UpdateRedirectRequest $request, Redirect $redirect) {
		$redirect->fill($request->validated());
		$redirect->save();
		return redirect()->route('cms.redirects.index', ['updated_id' => $redirect->id]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\Models\Redirect $redirect
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Redirect $redirect) {
		$redirect->delete();
		return redirect()->route('cms.redirects.index');
	}
}
