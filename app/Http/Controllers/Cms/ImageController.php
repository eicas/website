<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Http\Requests\Cms\DeleteImageRequest;
use App\Http\Requests\Cms\StoreImageRequest;
use App\Http\Requests\Cms\UpdateImageRequest;
use App\Models\Image;
use Illuminate\Http\Request;

class ImageController extends Controller {
	public function __construct() {
		$this->authorizeResource(Image::class, 'image');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$images = Image::with(['large', 'small'])->orderBy('created_at', 'desc')->get();

		return view('cms.images.index', ['images' => $images]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view('cms.images.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \App\Http\Requests\Cms\StoreImageRequest $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreImageRequest $request) {
		$image = Image::storeFile($request->validated()['image'], $request->validated()['alt']);

		if (!$image) {
			return redirect()->back()->withErrors(['image' => 'Fout bij het opslaan van het bestand.']);
		}

		$request->session()->flash('updated_id', $image->id);
		return redirect()->route('cms.images.index');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \App\Models\Image $image
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Image $image) {
		return view('cms.images.edit', ['image' => $image]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \App\Http\Requests\Cms\UpdateImageRequest $request
	 * @param \App\Models\Image $image
	 * @return \Illuminate\Http\Response
	 */
	public function update(UpdateImageRequest $request, Image $image) {
		$validated = $request->validated();
		if (isset($validated['image_id']) && !$validated['image_id']) {
			$validated['image_id'] = null;
		}
		$image->fill($validated);
		$image->save();
		$request->session()->flash('status', "Omschrijving opgeslagen.");
		$request->session()->flash('updated_id', $image->id);
		return redirect()->route('cms.images.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\Http\Requests\Cms\DeleteImageRequest $request
	 * @param \App\Models\Image $image
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(DeleteImageRequest $request, Image $image) {
		$image->delete();
		return redirect()->route('cms.images.index');
	}
}
