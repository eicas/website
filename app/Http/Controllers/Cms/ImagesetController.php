<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Http\Requests\Cms\StoreImagesetRequest;
use App\Http\Requests\Cms\UpdateImagesetRequest;
use App\Models\Imageset;
use Illuminate\Http\Request;

class ImagesetController extends Controller {
	public function __construct() {
		$this->authorizeResource(Imageset::class, 'imageset');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$imagesets = Imageset::withCount(['images', 'pages'])->get();

		return view('cms.imagesets.index', ['imagesets' => $imagesets]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \App\Http\Requests\Cms\StoreImagesetRequest $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreImagesetRequest $request) {
		$imageset = Imageset::create($request->validated());
		return redirect()->route('cms.imagesets.index', ['updated_id' => $imageset->id]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \App\Models\Imageset $imageset
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Imageset $imageset) {
		$imageset->load('images')->loadCount('pages');
		return view('cms.imagesets.edit', ['imageset' => $imageset]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \App\Http\Requests\Cms\UpdateImagesetRequest $request
	 * @param \App\Models\Imageset $imageset
	 * @return \Illuminate\Http\Response
	 */
	public function update(UpdateImagesetRequest $request, Imageset $imageset) {
		$imageset->fill($request->validated());
		$imageset->save();
		return redirect()->route('cms.imagesets.index', ['updated_id' => $imageset->id]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\Models\Imageset $imageset
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Imageset $imageset) {
		$imageset->delete();
		return redirect()->route('cms.imagesets.index');
	}
}
