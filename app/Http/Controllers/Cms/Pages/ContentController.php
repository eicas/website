<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers\Cms\Pages;

use App\Http\Controllers\Controller;
use App\Http\Requests\Cms\ContentStore;
use App\Http\Requests\Cms\ContentUpdate;
use App\Models\Content;
use App\Models\Page;
use Illuminate\Http\Request;

class ContentController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @param \App\Models\Page $page
	 * @return \Illuminate\Http\Response
	 */
	public function index(Page $page) {
		$page->load(['contents', 'trashedContents']);
		return view('cms.pages.contents.index', ['page' => $page]);
	}

	/**
	 * Handle the incoming request.
	 *
	 * @param \App\Http\Requests\Cms\ContentStore $request
	 * @param \App\Models\Page $page
	 * @return \Illuminate\Http\Response
	 */
	public function store(ContentStore $request, Page $page) {
		logger(array_merge($request->validated(), [
			'page_id' => $page->id,
			'order' => 1 + ($page->contents->last()->order ?? 0),
		]));
		Content::create(array_merge($request->validated(), [
			'page_id' => $page->id,
			'order' => 1 + ($page->contents->last()->order ?? 0),
		]));
		return redirect()->route('cms.pages.contents.index', ['page' => $page]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \App\Http\Requests\Cms\ContentUpdate $request
	 * @param \App\Models\Page $page
	 * @param \App\Models\Content $content
	 * @return \Illuminate\Http\Response
	 */
	public function update(ContentUpdate $request, Page $page, Content $content) {
		$content->content = $this->generateContent($content->type, $request->validated());
		$content->save();
		return redirect()->route('cms.pages.contents.index', ['page' => $page]);
	}

	/**
	 * Generate the content as it should be stored in the model. How this is done from the validated input, depends
	 * on the content type.
	 * 
	 * @param string $type
	 * @param string[] $validated
	 * @return string
	 */
	protected function generateContent($type, $validated) {
		switch ($type) {
			case 'shop-item':
				$image_id = intval($validated['other']);
				return "[" . ($image_id > 0 ? $image_id : '') . "]" . $validated['content'];
		}
		return $validated['content'];
	}

	/**
	 * Delete the given content.
	 * 
	 * @param \App\Models\Page $page
	 * @param \App\Models\Pivots\ContentPage $contentPage
	 */
	public function destroy(Page $page, Content $content) {
		$this->authorize('delete', $content);
		$content->delete();

		return redirect()->route('cms.pages.contents.index', ['page' => $page]);
	}

	/**
	 * Restore the given content.
	 * 
	 * @param \App\Models\Page $page
	 * @param \App\Models\Pivots\ContentPage $contentPage
	 */
	public function restore(Page $page, Content $content) {
		$this->authorize('restore', $content);

		$content->restore();

		return redirect()->route('cms.pages.contents.index', ['page' => $page]);
	}
}
