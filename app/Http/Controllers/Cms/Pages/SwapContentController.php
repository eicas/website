<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers\Cms\Pages;

use App\Http\Controllers\Controller;
use App\Models\Content;
use App\Models\Page;

class SwapContentController extends Controller {
	/**
	 * Swap the given Contents (for the given Page).
	 *
	 * @param \App\Models\Page $page
	 * @param \App\Models\Content $content1
	 * @param \App\Models\Content $content2
	 * @return \Illuminate\Http\Response
	 */
	public function __invoke(Page $page, Content $content1, Content $content2) {
		$tmp_order = $content1->order;
		$content1->order = $content2->order;
		$content2->order = $tmp_order;
		$content1->save();
		$content2->save();

		return redirect()->route('cms.pages.contents.index', ['page' => $page]);
	}
}
