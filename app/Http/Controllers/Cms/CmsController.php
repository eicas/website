<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Models\Download;
use App\Models\Image;
use App\Models\Imageset;
use App\Models\Page;
use App\Models\Redirect;
use Illuminate\Http\Request;

class CmsController extends Controller {
	public function index() {
		return view('cms.index', [
			'pages_count' => Page::count(),
			'images_count' => Image::count(),
			'imagesets_count' => Imageset::count(),
			'downloads_count' => Download::count(),
			'redirects_count' => Redirect::count(),
			// 'profiles_count' => Profile::count(),
			// 'users_count' => User::count(),
		]);
	}
}
