<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Models\Image;
use Illuminate\Http\Request;

class ImagePickerController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$data = $request->validate([
			'image_picker_id' => 'required|string|UUID',
		]);
		$images = Image::get();

		return view('components.image-picker.index', [
			'images' => $images,
			'image_picker_id' => $data['image_picker_id'],
		]);
	}
}
