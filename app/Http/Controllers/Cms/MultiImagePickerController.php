<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers\Cms;

use App\Helpers\Misc;
use App\Http\Controllers\Controller;
use App\Models\Image;
use Illuminate\Http\Request;

class MultiImagePickerController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$data = $request->validate([
			'multi_image_picker_id' => 'required|string',
			'selected_image_ids' => 'nullable|string|regex:/[0-9,]*/',
		]);
		$images = Image::get();
		$selected_image_ids = Misc::commaSeparatedToIntegerArray($data['selected_image_ids'] ?? '');

		return view('components.multi-image-picker.index', [
			'images' => $images,
			'multi_image_picker_id' => $data['multi_image_picker_id'],
			'selected_image_ids' => $selected_image_ids,
		]);
	}
}
