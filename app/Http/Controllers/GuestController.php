<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
// SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers;

use App\Models\Download;
use App\Models\Notfound;
use App\Models\Page;
use App\Models\Redirect;
use Illuminate\Http\Request;

class GuestController extends Controller {
	/**
	 * Find a page based on the request and show it.
	 * 
	 * @param \Illuminate\Http\Request $request
	 * @param string $slug
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request, string $slug = null) {
		$slug = str_replace("//", "/", "/$slug");

		$page = Page::public()->relativeUrl($slug)->first();

		if (!$page) {
			if ($redirect = $this->findRedirect($slug)) {
				return $redirect->redirect();
			}
			Notfound::add($slug, $request->ip(), $request->server('HTTP_REFERER') ?? null);
			abort(404);
		}

		$page->load(['contents', 'imageset', 'imageset.images']);

		$template = $page->template;
		if (!view()->exists("templates.$template")) {
			$template = 'story';
		}
		$imageset = $page->imageset;
		$images = collect();
		$social_image_url = null;
		$social_image_alt = null;
		$social_image_width = null;
		$social_image_height = null;
		$social_image_type = null;
		// arrays containing image ids:
		// bigger screens: two columns
		$imageimagesets_left = [];
		$imageimagesets_right = [];
		// smaller screens (mobile): three rows
		$width_top = 0;
		$width_bottom = 0;
		$imageimagesets_top = [];
		$imageimagesets_bottom = [];
		if ($imageset) {
			$images = $imageset->images;
			$images = $images->keyBy('id');
			// logger($images);
			switch ($template) {
				case 'home':
					// left column: 50%; right column: 45%
					// also add gap of 5% of width
					// 5% of width on left col (50%): 5 * 100/50 = 10%
					// 5% of width on right col (50%): 5 * 100/45 = 11.111%
					// see also /resources/css/_guest_home.scss
					$height_left = 0;
					$height_right = 0;
					$factor_left = 1.1 * 50;
					$factor_right = 1.11111 * 45;
				break;
				default:
					// make space for logo
					$height_left = 0.5106;
					$height_right = 0;
					$factor_left = 1.055;
					$factor_right = 1.055;
			}
			// 2 strips (46.25% each), 3 stripes (2% each)
			// factor: 48.25 / 46.25
			$factor_horizontal = 1.043;
			// first image always in left column / top row
			$first = true;
			foreach ($imageset->images as $image) {
				// logger($image);
				if (!$first && ($height_left > $height_right)) {
					$imageimagesets_right[] = $image->pivot->id;
					$height_right += $factor_right / $image->getRatio();
					// logger(sprintf(
					// 	"added image right with relative height %.3f; left: %5.1f; right: %5.1f",
					// 	$image->getRatio(),
					// 	$height_left,
					// 	$height_right
					// ));
				} else {
					$imageimagesets_left[] = $image->pivot->id;
					$height_left += $factor_left / $image->getRatio();
					// logger(sprintf(
					// 	"added image  left with relative height %.3f; left: %5.1f; right: %5.1f",
					// 	$image->getRatio(),
					// 	$height_left,
					// 	$height_right
					// ));
				}
				if (!$first && $width_bottom < $width_top) {
					$imageimagesets_bottom[] = $image->pivot->id;
					$width_bottom += $factor_horizontal * $image->getRatio();
				} else {
					$imageimagesets_top[] = $image->pivot->id;
					$width_top += $factor_horizontal * $image->getRatio();
				}
				if ($first) {
					// Facebook prefers social images to be <8MB
					// signal-desktop even prefers social images to be <1MB
					// taking the variant where width=1024 seems to be a
					// good heuristic in practice for jpg's (and we should use
					// jpg's for their size/quality tradeoff here):
					$social_image_url = $image->getUrl(1024);
					$social_image_width = 1024;
					$social_image_height = $image->heights[1024];
					$social_image_alt = $image->alt;
					if (str_ends_with($social_image_url, 'jpg')) {
						$social_image_type = "image/jpeg";
					} elseif  (str_ends_with($social_image_url, 'png')) {
						$social_image_type = "image/png";
					}
				}
				$first = false;
				// logger(sprintf(
				// 	"top: %5.1f  bottom: %5.1f",
				// 	$width_top,
				// 	$width_bottom
				// ));
			}
			// logger($imageimagesets_top);
			// logger($imageimagesets_bottom);
		}

		return view("templates.$template", [
			'page' => $page,
			'social_image_url' => $social_image_url,
			'social_image_width' => $social_image_width,
			'social_image_height' => $social_image_height,
			'social_image_alt' => $social_image_alt,
			'social_image_type' => $social_image_type,
			'images' => $images,
			'imageimagesets_left' => $imageimagesets_left,
			'imageimagesets_right' => $imageimagesets_right,
			'imageimagesets_top' => $imageimagesets_top,
			'imageimagesets_bottom' => $imageimagesets_bottom,
		]);
	}

	/**
	 * Find a redirect for the given slug. Return the redirect url or null if there is none.
	 * 
	 * The slug should always start with a slash.
	 * 
	 * @param string $slug
	 * @return \App\Models\Redirect|null
	 */
	protected function findRedirect(string $slug): ?Redirect {
		$redirect = Redirect::from($slug)->first();
		if ($redirect) {
			return $redirect;
		}
		return null;
	}

	/**
	 * Find a page based on the request and show it.
	 * 
	 * @param string $slug
	 * @return \Illuminate\Http\Response
	 */
	public function download(string $filename) {
		$download = Download::filename($filename)->first();
		if (!$download) {
			abort(404);
		}
		return $download->downloadResponse();
	}
}
