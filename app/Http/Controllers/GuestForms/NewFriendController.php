<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers\GuestForms;

use App\Http\Controllers\Controller;
use App\Http\Requests\GuestForms\NewFriend\StoreRequest;
use App\Http\Requests\GuestForms\NewFriend\WebhookRequest;
use App\Models\GuestForms\NewFriend;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

/**
 * For this controller, permissions via the policy are assigned in web.guest_forms.php, with the route definitions.
 */
class NewFriendController extends Controller {
	//  GGGG  UU   UU EEEEEE  SSSSS TTTTTT
	// GG     UU   UU EE     SS       TT
	// GG GGG UU   UU EEEEE   SSSS    TT
	// GG  GG UU   UU EE         SS   TT
	//  GGGGG  UUUUUU EEEEEE SSSSS    TT
	//
	// Routes accessible to guests:
	// - store (visitor submits new friend form)
	// - webhook (Mollie payment service calls the app to notify about payment changes)

	/**
	 * Handle a form submission.
	 * 
	 * @param \App\Http\Requests\GuestForms\NewFriend\StoreRequest $request
	 * @return
	 */
	public function store(StoreRequest $request) {
		$reference = substr(Str::uuid(), 0, 8);
		$validated = $request->validated();
		$testMode = $this->checkForTestMode($validated['postalcode']);

		$mollie = mollie();
		if ($testMode) {
			$mollie->setApiKey(config('mollie.key_test'));
		}

		$payment = $mollie->payments->create([
			'amount' => [
				'currency' => 'EUR',
				'value' => sprintf("%.2f", $validated['amount'] / 100),
			],
			'description' => "Uw EICAS vrienden-donatie, referentie $reference",
			'redirectUrl' => $validated['redirect_url'],
			'method' => $validated['payment_method'],
			'webhookUrl' => config('app.env') === 'local' ? null : route('guest_forms.new_friend.webhook'),
			'metadata' => [
				'reference' => $reference,
			],
		]);

		$newFriend = NewFriend::create([
			'name' => $validated['name'],
			'email' => $validated['email'],
			'telephone' => $validated['telephone'] ?? null,
			'address' => $validated['address'] ?? null,
			'postalcode' => $validated['postalcode'] ?? null,
			'city' => $validated['city'] ?? null,
			'country' => $validated['country'] ?? null,
			'amount' => $validated['amount'] / 100,
			'payment_test' => $testMode, //Str::startsWith(env('MOLLIE_KEY'), 'test_'),
			'payment_method' => $validated['payment_method'],
			'payment_reference' => $reference,
			'payment_id' => $payment->id,
			'payment_status' => 'open',
		]);
		// logger("stored new NewFriend payment:");
		// logger($newFriend);
		// logger("redirecting to: " . $payment->getCheckoutUrl());

		session(['forms.new-friend' => [
			'step' => 'mollie',
			'newFriend_id' => $newFriend->id,
			'payment_id' => $payment->id,
			'payment_reference' => $reference,
		]]);


		return redirect($payment->getCheckoutUrl(), 303);
	}

	private function checkForTestMode(string $overrideCode = null): bool {
		$testKey = config('mollie.override_test', false);
		return $testKey && $overrideCode && ($testKey === $overrideCode);
	}

	/**
	 * Handle a webhook call from the Mollie service.
	 * 
	 * @param \App\Http\Requests\GuestForms\NewFriend\WebhookRequest $request
	 */
	public function webhook(WebhookRequest $request) {
		$payment_id = $request->validated()['id'];
		logger("webhook called! id=$payment_id");
		if ($newFriend = NewFriend::paymentId($payment_id)->first()) {
			logger("newFriend found, id={$newFriend->id}");

			$mollie = mollie();
			if ($newFriend->payment_test) {
				$mollie->setApiKey(config('mollie.key_test'));
			}
			if ($payment = $mollie->payments->get($payment_id)) {
				$newFriend->payment_status = $payment->status;
				if ($payment->isPaid()) {
					$newFriend->setPaid(false);
					logger("set paid");
				}
				$newFriend->save();
				logger("updated DB (status={{ $newFriend->payment_status }})");
			} else {
				logger("Mollie payment not found.");
			}
		} else {
			logger("newFriend not found.");
		}

		$url = $request->validated()['redirect_url'] ?? "";
		return $url ? redirect()->to($url) : "OK";
	}

	//   AAA   DDDDDD  MM   MM IIII NN   NN
	//  AA AA  DD   DD MMM MMM  II  NNN  NN
	// AA   AA DD   DD MM M MM  II  NN N NN
	// AAAAAAA DD   DD MM   MM  II  NN  NNN
	// AA   AA DDDDDD  MM   MM IIII NN   NN
	//
	// Routes only for administrators
	// - index (show a list of all new friend submissions and payments)

	/**
	 * Show a list of friends and their payments.
	 * 
	 * 
	 */
	public function index() {
		$new_friends = NewFriend::orderBy('created_at', 'desc')->get();
		return view('guest_forms.new_friends.index', ['new_friends' => $new_friends]);
	}
}
