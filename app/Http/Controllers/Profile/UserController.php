<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Profile\UpdateUserRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

/**
 * This UserController handles actions around user management. I.e. creating/updating/deleting users.
 */
class UserController extends Controller {
	public function __construct() {
		$this->authorizeResource(User::class, 'user');
	}

	/**
	 * Display a listing of the resource.
	 * 
	 * The request may include an extra GET parameter: 'admin' with possible values 0 or 1.
	 * 0: manage only non-EICAS users, i.e. friends and subscribers.
	 * 1: manage only EICAS users, i.e. admin, user-admin, text-admin, art-admin, and menu-admin.
	 * This setting is stored in the users session (key: 'user-admin.admin') and only reset if a new value is set here.
	 * Default value: 1 (show EICAS users only)
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		// get the id's of the users with at least one admin-like role, i.e. the EICAS users:
		// // note: resulting records are not Roles
		// $admin_user_ids = Role::join('role_user','role_user.role_id','=','roles.id')
		// 	// find only admin-like roles
		// 	->where('roles.key','like','%admin%')
		// 	// get the user id's via the joined role_user table
		// 	->select('role_user.user_id')
		// 	// group to prevent duplicate users
		// 	->groupBy('role_user.user_id')
		// 	// put the id's into an array
		// 	->get()->pluck('user_id')->all();
		// // use the id-list to pre-select the users
		$admin_user_ids = User::getAdminUserIds();

		// determine whether EICAS-users or non-EICAS users are requested
		$admin = $request->session()->get('user-admin.admin', 1) ? 1 : 0;
		if ($request->has('admin')) {
			$admin = $request->admin ? 1 : 0;
			$request->session()->put('user-admin.admin', $admin);
		}
		if ($admin) {
			$users = User::whereIn('id', $admin_user_ids);
		} else {
			$users = User::whereNotIn('id', $admin_user_ids);
		}
		$users = $users->with('roles')->withCount('userSessions')->get();
		return view('users.index', ['users' => $users]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 * 
	 * This route makes use of the edit blade, with a special variable 'show' set to true to disable forms.
	 *
	 * @param \App\Models\User $user
	 * @return \Illuminate\Http\Response
	 */
	public function show(User $user) {
		$user->load(['userSessions', 'roles']);
		$roles = Role::get();
		return view('users.edit', ['show' => true, 'user' => $user, 'roles' => $roles]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \App\Models\User $user
	 * @return \Illuminate\Http\Response
	 */
	public function edit(User $user) {
		$user->load(['userSessions', 'roles']);
		$roles = Role::get();
		return view('users.edit', ['user' => $user, 'roles' => $roles]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \App\Http\Requests\Profile\UpdateUserRequest $request
	 * @param \App\Models\User $user
	 * @return \Illuminate\Http\Response
	 */
	public function update(UpdateUserRequest $request, User $user) {
		$user->fill($request->safe()->only(['name', 'email']));
		$user->save();
		$user->roles()->sync(
			Role::whereIn(
				'key',
				$request->safe()->only(['roles'])['roles'] // keys of selected roles
			)->get()->pluck('id')->all() // get id's of those selected roles
		);
		return redirect()->route('profiles.edit', ['user' => $user]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\Models\User $user
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(User $user) {
		$user->delete();
		return redirect()->route('profiles.index');
	}
}
