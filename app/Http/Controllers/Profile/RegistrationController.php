<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Profile\RegistrationRequest;
use App\Models\User;
use App\Providers\AuthServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegistrationController extends Controller {
	/**
	 * Show the registration view.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function create(Request $request) {
		return view('profile.register');
	}

	/**
	 * Create a new registered user.
	 *
	 * @param \App\Http\Requests\Profile\RegistrationRequest $request
	 * @param \Laravel\Fortify\Contracts\CreatesNewUsers $creator
	 * @return \Illuminate\Http\Response
	 */
	public function store(RegistrationRequest $request) {
		$user_data = collect($request->validated())->except('password_confirmation');
		$user = User::create($user_data->replace(['password' => Hash::make($user_data['password'])])->all());

		event(new Registered($user));

		Auth::login($user);

		return redirect(AuthServiceProvider::userHome());
	}

	/**
	 * Remove the registered user.
	 *
	 * This should also immediately logout the user.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request) {
		if (!Hash::check($request->password, $request->user()->password)) {
			return back()->withErrors([
				'password' => ['Dit wachtwoord komt niet overeen met onze gegevens.'],
			]);
		}

		$user = Auth::user();
		Auth::logout();
		$user->delete();
		session()->invalidate();
		session()->regenerateToken();
		return redirect('/');
	}
}
