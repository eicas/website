<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Profile\VerifyEmailRequest;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\Request;

class EmailController extends Controller {
	/**
	 * Show a notification suggesting to the user that a verification email was sent.
	 */
	public function showNotice() {
		return view('profile.verify-email');
	}

	/**
	 * Mark the authenticated user's email address as verified.
	 *
	 * @param \App\Http\Requests\Profile\VerifyEmailRequest $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function verify(VerifyEmailRequest $request) {
		// Was already verified? Go to profile page.
		if ($request->user()->hasVerifiedEmail()) {
			return redirect()->intended(route('profile.show'));
		}

		if ($request->user()->markEmailAsVerified()) {
			event(new Verified($request->user()));
		}

		return redirect()->intended(route('profile.show'));
	}

	/**
	 * Send a verification email.
	 */
	public function send(Request $request) {
		$request->user()->SendEmailVerificationNotification();

		return back()->with('message', 'Verzonden!');
	}
}
