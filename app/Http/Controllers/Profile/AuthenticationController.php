<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Providers\AuthServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthenticationController extends Controller {
	/**
	 * Show the login view.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view('profile.login');
	}

	/**
	 * Handle an authentication attempt.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$credentials = $request->only('email', 'password');
		$remember = $request->has('remember_me');

		if (Auth::attempt($credentials, $remember)) {
			$request->session()->regenerate();
			$request->user()->userSession->setSessionId($request->session()->getId());

			return redirect()->intended(AuthServiceProvider::userHome());
		}

		return back()->withErrors([
			'email' => 'The provided credentials do not match our records.',
		]);
	}

	/**
	 * Show a password confirmation form.
	 * 
	 * @return \Illuminate\Http\Response
	 */
	public function confirmation() {
		return view('profile.confirm-password');
	}

	/**
	 * Confirm the user password.
	 * 
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function confirm(Request $request) {
		if (!Hash::check($request->password, $request->user()->password)) {
			return back()->withErrors([
				'password' => ['Dit wachtwoord komt niet overeen met onze gegevens.']
			]);
		}
	
		$request->session()->passwordConfirmed();
	
		return redirect()->intended();
	
	}

	/**
	 * Logout the current user.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy() {
		Auth::logout();
		session()->invalidate();
		session()->regenerateToken();
		return redirect('/');
	}
}
