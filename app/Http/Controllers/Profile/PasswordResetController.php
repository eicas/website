<?php

// SPDX-FileCopyrightText: 2022 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Profile\PasswordResetStoreRequest;
use App\Http\Requests\Profile\PasswordResetUpdateRequest;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

class PasswordResetController extends Controller {
	public function create() {
		return view('profile.password-reset.create');
	}

	/**
	 * 
	 * @param \App\Http\Requests\Profile\PasswordResetStoreRequest $request
	 */
	public function store(PasswordResetStoreRequest $request) {
		$status = Password::sendResetLink($request->safe(['email']));
	
		return $status === Password::RESET_LINK_SENT
					? back()->with(['status' => __($status)])
					: back()->withErrors(['email' => __($status)]);
	}

	public function edit(string $token) {
		return view('profile.password-reset.edit', ['token' => $token]);
	}

	/**
	 * 
	 * @param \App\Http\Requests\Profile\PasswordResetUpdateRequest $request
	 */
	public function update(PasswordResetUpdateRequest $request) {
		$status = Password::reset(
			$request->safe(['email', 'password', 'password_confirmation', 'token']),
			function ($user, $password) {
				$user->forceFill([
					'password' => Hash::make($password)
				])->setRememberToken(Str::random(60));
	
				$user->save();
	
				event(new PasswordReset($user));
			}
		);
	
		return $status === Password::PASSWORD_RESET
			? redirect()->route('profile.show')->with('status', __($status))
			: back()->withErrors(['email' => [__($status)]]);
	}
}
