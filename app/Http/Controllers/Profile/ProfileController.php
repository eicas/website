<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Profile\UpdatePasswordRequest;
use App\Models\User;
use App\Models\UserSession;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

/**
 * This controller handles actions around the user's personal profile.
 */
class ProfileController extends Controller {

	/**
	 * @return \Illuminate\Http\Response
	 */
	public function show() {
		$user_id = Auth::id();
		UserSession::userId($user_id)->hardExpired()->delete();

		$user = User::with(['userSessions', 'roles'])->findOrFail($user_id);
		$user->loadUserSession();
		return view('profile.show', ['user' => $user]);
	}

	public function destroyUserSession(UserSession $userSession) {
		if ($userSession->user_id === Auth::id() && $userSession->canRemember) {
			$userSession->delete();
		}
		return redirect()->route('profile.show', ['user' => $user]);
	}

	/**
	 * Update the user's password.
	 *
	 * @param \App\Http\Requests\Profile\UpdatePasswordRequest $request
	 * @return \Illuminate\Http\Response
	 */
	public function updatePassword(UpdatePasswordRequest $request) {
		$user = $request->user();
		$user->fill([
			'password' => Hash::make($request->password),
		])->save();

		// make sure to re-login the user
		Auth::login($user, !!$user->getRememberToken());

		$request->session()->flash('status', 'Het wachtwoord is vernieuwd.');
		return redirect()->route('profile.show');
	}

}
