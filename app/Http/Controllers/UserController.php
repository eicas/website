<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * This UserController handles actions for regular users which do not belong to a certain group of actions.
 * I.e. "other" actions.
 */
class UserController extends Controller {
	/**
	 * Show the user dashboard view (user.index).
	 * 
	 * The blade file checks the current user's roles to determine what links to show.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$parameters = [
			'internal_users_count' => 0,
			'external_users_count' => 0,
		];
		if ($request->user()->hasAnyRole(['admin', 'user-admin'])) {
			$parameters['internal_users_count'] = count(User::getAdminUserIds());
			$parameters['external_users_count'] = User::count() - $parameters['internal_users_count'];
		}

		return view('user.index', $parameters);
	}
}
