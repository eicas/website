<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Auth\AuthenticationException;

class Authenticate extends Middleware {
	/**
	 * Determine if the user is logged in to any of the given guards.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param array $guards
	 * @return void
	 *
	 * @throws \Illuminate\Auth\AuthenticationException
	 */
	protected function authenticate($request, array $guards) {
		if (empty($guards)) {
			$guards = [null];
		}

		foreach ($guards as $guard) {
			if ($this->auth->guard($guard)->check()) {
				return $this->auth->shouldUse($guard);
			}
		}

		$this->unauthenticated($request, $guards);
	}

	/**
	 * Handle an unauthenticated user.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param array $guards
	 * @return void
	 *
	 * @throws \Illuminate\Auth\AuthenticationException
	 */
	protected function unauthenticated($request, array $guards) {
		// logger("[Authenticate::unauthenticated] throwing AuthenticationException");
		throw new AuthenticationException(
			'Unauthenticated.', $guards, $this->redirectTo($request)
		);
	}

	/**
	 * Get the path the user should be redirected to when they are not authenticated.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return string|null
	 */
	protected function redirectTo($request) {
		if (!$request->expectsJson()) {
			return route('profile.login');
		}
	}
}
