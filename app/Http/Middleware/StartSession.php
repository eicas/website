<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Session\Middleware\StartSession as BaseStartSession;

/**
 * This class extends the default Laravel StartSession middleware.
 *
 * The handleStatefulRequest function is overwritten to also maintain a visit counter per session.
 */
class StartSession extends BaseStartSession {
	/**
	 * Handle the given request within session state.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Illuminate\Contracts\Session\Session  $session
	 * @param  \Closure  $next
	 * @return mixed
	 */
	protected function handleStatefulRequest(Request $request, $session, Closure $next) {
		// If a session driver has been configured, we will need to start the session here
		// so that the data is ready for an application. Note that the Laravel sessions
		// do not make use of PHP "native" sessions in any way since they are crappy.
		$request->setLaravelSession(
			$this->startSession($request, $session)
		);

		$this->collectGarbage($session);

		// maintain simple per-session visit counter
		$session->put('visitcounter', $session->get('visitcounter', 0) + 1);

		$response = $next($request);

		$this->storeCurrentUrl($request, $session);

		$this->addCookieToResponse($response, $session);

		// Again, if the session has been configured we will need to close out the session
		// so that the attributes may be persisted to some storage medium. We will also
		// add the session identifier cookie to the application response headers now.
		$this->saveSession($request);

		return $response;
	}

}
