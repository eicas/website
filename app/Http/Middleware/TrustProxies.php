<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Middleware;

use Fideloper\Proxy\TrustProxies as Middleware;
use Illuminate\Http\Request;
use Closure;

class TrustProxies extends Middleware {
	/**
	 * The trusted proxies for this application.
	 *
	 * @var array|string|null
	 */
	protected $proxies;

	/**
	 * The headers that should be used to detect proxies.
	 *
	 * @var int
	 */
	protected $headers = Request::HEADER_X_FORWARDED_FOR | Request::HEADER_X_FORWARDED_HOST | Request::HEADER_X_FORWARDED_PORT | Request::HEADER_X_FORWARDED_PROTO | Request::HEADER_X_FORWARDED_AWS_ELB;

	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure                 $next
	 *
	 * @throws \Symfony\Component\HttpKernel\Exception\HttpException
	 *
	 * @return mixed
	 */
	public function handle(Request $request, Closure $next) {
		$request::setTrustedProxies([], $this->getTrustedHeaderNames()); // Reset trusted proxies between requests
		$this->setTrustedProxyIpAddresses($request);

		return $next($request);
	}
}
