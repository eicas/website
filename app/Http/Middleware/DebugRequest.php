<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Str;

class DebugRequest {
	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure $next
	 * @return mixed
	 */
	public function handle(Request $request, Closure $next) {
		$debug = config('app.debug');
		if ($debug) {
			$info = [];
			// route info
			$info[] = "{$request->method()} /{$request->path()} -> route: '" . Route::currentRouteName() . "'";
			foreach (Route::getCurrentRoute()->parameterNames() as $key) {
				$info[] = "route($key) " . $request->route($key);
			}
			// session info
			$info[] = sprintf(
				"sid:%s (%d) user:%s",
				$request->session()->getId(),
				$request->session()->get('visitcounter', 0),
				$request->user()->id ?? 'none'
			);
			// request info
			foreach ($request->all() as $key => $value) {
				if ($key[0] === '_') {
					continue;
				}
				if (stripos($key, 'password') !== false) {
					$info[] = "[$key] " . str_repeat("*", strlen(var_export($value, true)));
				} else {
					$info[] = "[$key] " . var_export($value, true);
				}
			}
			$this->logInfo($info);
		}

		$return = $next($request);

		if ($debug && $request->session()->has('errors')) {
			$info = [];
			$info[] = "error(s) set in session";
			$info[] = "BR";
			$errors = $request->session()->get('errors');
			foreach ($errors->keys() as $key) {
				foreach ($errors->get($key) as $index => $value) {
					$info[] = "[$key][$index] " . var_export($value, true);
				}
			}
			$this->logInfo($info);
		}

		return $return;
	}

	protected function logInfo(array $info, int $min_width = 40, int $max_width = 120) {
		foreach ($info as &$line) {
			$line = explode("\n", wordwrap($line, $max_width, "\n", true));
		}
		unset($line);
		$width = $min_width;
		foreach ($info as $lines) {
			foreach ($lines as $line) {
				$width = max($width, Str::length($line));
			}
		}
		logger("╔" . str_repeat("═", $width + 2) . "╗");
		foreach ($info as $lines) {
			foreach ($lines as $line) {
				if ($line === "BR") {
					logger("╟" . str_repeat("─", $width + 2) . "╢");
				} else {
					// note: sprintf is not multi-byte safe. Therefore, the width specifier needs to be compensated
					// with the difference between the byte length and the character length.
					logger(sprintf("║ %-" . ($width + strlen($line) - Str::length($line)) . "s ║", $line));
				}
			}
		}
		logger("╚" . str_repeat("═", $width + 2) . "╝");

	}
}
