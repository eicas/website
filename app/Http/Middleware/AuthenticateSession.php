<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Auth\Factory as AuthFactory;
use Illuminate\Http\Request;

/**
 * This class is a modified version of Illuminate\Session\Middleware\AuthenticateSession. It provides the same
 * functionality, but with an implementation suited for the User and UserSession implementation of this project.
 */
class AuthenticateSession {
	/**
	 * The authentication factory implementation.
	 *
	 * @var \Illuminate\Contracts\Auth\Factory
	 */
	protected $auth;

	/**
	 * Create a new middleware instance.
	 *
	 * @param \Illuminate\Contracts\Auth\Factory $auth
	 * @return void
	 */
	public function __construct(AuthFactory $auth) {
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		// logger("[AuthenticateSession] start handling");
		if (!$request->hasSession()) {
			// logger("[AuthenticateSession] no session, ready.");
			return $next($request);
		}
		// logger("[AuthenticateSession] session found: '" . $request->session()->getId() . "'");
		if (!$request->user()) {
			// logger("[AuthenticateSession] no user, ready.");
			return $next($request);
		}
		// logger("[AuthenticateSession] user found: '{$request->user()->id}'");

		if ($this->guard()->viaRemember()) {
			// logger("[AuthenticateSession] user found via remember cookie");
			$passwordHash = explode('|', $request->cookies->get($this->auth->getRecallerName()))[2] ?? null;

			if (!$passwordHash || $passwordHash != $request->user()->getAuthPassword()) {
				// logger("[AuthenticateSession] password hashes do not match, logout");
				$this->logout($request);
			}
		}

		if (!$request->session()->has('password_hash_' . $this->auth->getDefaultDriver())) {
			// logger("[AuthenticateSession] no password hash stored.");
			$this->storePasswordHashInSession($request);
		}

		$session_hash = $request->session()->get('password_hash_' . $this->auth->getDefaultDriver());
		$request_hash = $request->user() ? $request->user()->getAuthPassword() : null;
		if ($session_hash !== $request_hash) {
			// logger("[AuthenticateSession] password hash in session does not match user's, logout.");
			$this->logout($request);
		}

		return tap($next($request), function () use ($request) {
			if (!is_null($this->guard()->user())) {
				// logger("[AuthenticateSession] storing password hash in session for user {$this->guard()->user()->id}.");
				$this->storePasswordHashInSession($request);
			}
			// logger("[AuthenticateSession] ready.");
		});
	}

	/**
	 * Store the user's current password hash in the session.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return void
	 */
	protected function storePasswordHashInSession($request) {
		if (!$request->user()) {
			return;
		}

		// logger("[AuthenticateSession] storing 'password_hash_{$this->auth->getDefaultDriver()}'");
		$request->session()->put([
			'password_hash_' . $this->auth->getDefaultDriver() => $request->user()->getAuthPassword(),
		]);
	}

	/**
	 * Log the user out of the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return void
	 *
	 * @throws \Illuminate\Auth\AuthenticationException
	 */
	protected function logout($request) {
		$this->guard()->logoutCurrentDevice();

		$request->session()->flush();

		throw new AuthenticationException(
			'Unauthenticated.',
			[$this->auth->getDefaultDriver()],
			route('profile.login')
		);
	}

	/**
	 * Get the guard instance that should be used by the middleware.
	 *
	 * @return \Illuminate\Contracts\Auth\Factory|\Illuminate\Contracts\Auth\Guard
	 */
	protected function guard() {
		return $this->auth;
	}
}
