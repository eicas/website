<?php

// SPDX-FileCopyrightText: 2021 - 2022 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Middleware;

use Illuminate\Http\Middleware\TrustHosts as Middleware;

class TrustHosts extends Middleware {
	/**
	 * Get the host patterns that should be trusted.
	 *
	 * @return array
	 */
	public function hosts() {
		return [
			'tmp.eicas.nl',
			$this->allSubdomainsOfApplicationUrl(),
		];
	}
}
