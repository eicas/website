<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Requests\Profile;

use App\Models\User;
use App\Rules\ValidPassword;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegistrationRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'name' => ['required', 'string', 'max:255'],
			'email' => [
				'required',
				'string',
				'email:rfc', // ,dns (?)
				'max:255',
				Rule::unique(User::class),
			],
			'password' => ['required', 'string', new ValidPassword(), 'confirmed'],
		];
	}
}
