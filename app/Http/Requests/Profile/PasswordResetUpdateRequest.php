<?php

// SPDX-FileCopyrightText: 2022 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Requests\Profile;

use App\Rules\ValidPassword;
use Illuminate\Foundation\Http\FormRequest;

class PasswordResetUpdateRequest extends FormRequest {
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'token' => 'required|string',
			'email' => 'required|email:rfc',
			'password' => ['required', 'string', new ValidPassword(), 'confirmed'],
		];
	}
}
