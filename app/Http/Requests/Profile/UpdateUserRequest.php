<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Requests\Profile;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest {
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'name' => 'required|string|max:255',
			'email' => [
				'required',
				'string',
				'max:255',
				'email:rfc',
				Rule::unique(User::class)->ignore($this->route('user')->id),
			],
			'roles' => 'nullable|array',
			'roles.*' => 'string|in:' . implode(",", Role::getAllKeys()),
		];
	}

	/**
	 * As an after-hook, check whether the user may alter the admin-role.
	 */
	public function withValidator($validator) {
		if (!$this->user()->hasRole('admin')) {
			if ($this->route('user')->hasRole('admin') !== in_array('admin', $this->roles)) {
				$validator->errors()->add('roles', 'U mag de beheerders-rol niet aanpassen.');
			}
		}
	}
}
