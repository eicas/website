<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Requests\Profile;

use Illuminate\Foundation\Http\FormRequest;

class VerifyEmailRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * The request should have been called with an 'id' and 'hash'. This function checks:
	 * 
	 * 1. whether the given id equals the id of the current user;
	 * 
	 * 2. whether the given hash equals the registered email address.
	 * 
	 * Both comparisons are done using the 'hash_equals' function to prevent timing attacks. (Btw, this route should
	 * also be throttled!)
	 * 
	 * @return bool
	 */
	public function authorize() {
		if (!hash_equals((string) $this->route('id'), (string) $this->user()->getKey())) {
			return false;
		}

		// getEmailForVerification() normally returns the user->email property, see also
		// Illuminate\Auth\MustVerifyEmail::getEmailForVerification()
		if (!hash_equals((string) $this->route('hash'), sha1($this->user()->getEmailForVerification()))) {
			return false;
		}

		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [];
	}
}
