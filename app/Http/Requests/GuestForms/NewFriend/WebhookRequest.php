<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Requests\GuestForms\NewFriend;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Represent a request as called by the Mollie service.
 * 
 * A typical request looks like:
 * 
 * POST /payments/webhook HTTP/1.0
 * Host: webshop.example.org
 * Via: 1.1 tinyproxy (tinyproxy/1.8.3)
 * Content-Type: application/x-www-form-urlencoded
 * Accept: <star slash star> (redacted to not interfere with php comment)
 * Accept-Encoding: deflate, gzip
 * Content-Length: 16
 * 
 * id=tr_d0b0E3EA3v
 * 
 * 
 */
class WebhookRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'id' => [
				'required',
				'string',
				'alpha_dash',
				'starts_with:tr_',
			],
			'redirect_url' => 'nullable|string',
		];
	}
}
