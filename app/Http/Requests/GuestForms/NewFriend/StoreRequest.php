<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Requests\GuestForms\NewFriend;

use App\Models\GuestForms\NewFriend;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'name' => 'required|string|max:255',
			'email' => 'required|string|max:255|email:rfc',
			'telephone' => 'nullable|string|max:255',
			'address' => 'nullable|string|max:255',
			'postalcode' => 'nullable|string|max:255',
			'city' => 'nullable|string|max:255',
			'country' => 'nullable|string|max:255',
			'amount' => 'required|integer|in:' . implode(",", array_merge([101], NewFriend::AMOUNTS)),
			'payment_method' => 'required|string|in:' . implode(",", array_keys(NewFriend::getPaymentMethods())),
			'redirect_url' => 'required|string|url',
		];
	}
}
