<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Requests\Cms;

use App\Models\Download;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class UpdateDownloadRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'filename' => 'required|string|max:255|ends_with:.' . implode(",.", Arr::flatten(Download::MIMETYPES)),
		];
	}
}
