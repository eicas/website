<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Requests\Cms;

class StoreRedirectRequest extends BaseRedirectRequest {
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'from' => 'required|string|starts_with:/|between:2,255|unique:redirects',
			'to' => 'required|string|starts_with:/|max:255',
			'comment' => 'nullable|string|max:255',
		];
	}
}
