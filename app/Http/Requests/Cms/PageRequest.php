<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
// SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Requests\Cms;

use App\Models\Imageset;
use App\Models\Page;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class PageRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	protected function prepareForValidation() {
		$this->merge([
			'slug' => str_replace('//', '/', "/" . $this->slug ?? ''),
		]);
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'language' => 'required|string|size:2|in:' . implode(",", config('app.site_languages')),
			'slug' => [
				'nullable',
				'string',
				'max:128',
				'startsWith:/',
				Rule::unique('pages')->where(function ($query) {
					$query->whereNull('deleted_at');
					if ($language = request()->input('languages', null)) {
						$query->where('language', $language);
					}
					if ($page = request()->route('page')) {
						$query->where('id', '<>', $page->id);
					}
					
					return $query;
				}),
			],
			'title' => 'required|string|max:255',
			'description' => 'required|string|max:2048',
			'template' => 'required|string|max:255|in:' . implode(",", Page::getAvailableTemplates()),
			'imageset_id' => 'nullable|integer|in:' . implode(",", Imageset::get()->pluck('id')->all()),
			'visibility' => 'required|integer|in:' . implode(",", array_keys(Page::VISIBILITY)),
		];
	}
}
