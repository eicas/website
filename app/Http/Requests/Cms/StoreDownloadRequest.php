<?php

// SPDX-FileCopyrightText: 2021 - 2022 Marten Koetsier <eicas@koetsierengineering.nl>
// SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Requests\Cms;

use App\Models\Download;
use Illuminate\Foundation\Http\FormRequest;

class StoreDownloadRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'file' => 'required|file|max:32768|mimetypes:' . implode(",", array_keys(Download::MIMETYPES)),
		];
	}
}
