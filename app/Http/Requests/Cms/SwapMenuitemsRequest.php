<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Requests\Cms;

use App\Models\Menuitem;
use Illuminate\Foundation\Http\FormRequest;

class SwapMenuitemsRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return $this->user()->can('admin', Menuitem::class);
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [];
	}

	/**
	 * Make sure both menuitems are in the same group.
	 */
	public function withValidator($validator) {
		$validator->after(function ($validator) {
			if ($this->menuitem1->menuitem_id !== $this->menuitem1->menuitem_id) {
				$validator->errors()->add('menuitem2', 'Something is wrong with this field!');
			}
		});
	}
}
