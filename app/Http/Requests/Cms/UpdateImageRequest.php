<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Requests\Cms;

use App\Models\Image;
use Illuminate\Foundation\Http\FormRequest;

class UpdateImageRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'alt' => 'required|string|max:256',
			'title' => 'nullable|string|max:256',
			'artist' => 'nullable|string|max:256',
			'year' => 'nullable|integer|between:1900,3000',
			'info' => 'nullable|string|max:32767',
			// note: no worries about the comma after "in:0" if there are no images because in that case, this
			// request can neven be reached.
			'image_id' => 'nullable|integer|in:0,' . implode(
				',',
				Image::select('id')->get()->pluck('id')->all()
			),
		];
	}
}
