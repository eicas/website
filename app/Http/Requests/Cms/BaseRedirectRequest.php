<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Requests\Cms;

use App\Models\Redirect;
use Illuminate\Foundation\Http\FormRequest;

abstract class BaseRedirectRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	public function attributes() {
		return [
			'comment' => 'opmerking',
		];
	}

	public function messages() {
		return [
			'from.starts_with' => 'From en To moeten altijd met een slash (/) beginnen.',
			'to.starts_with' => 'From en To moeten altijd met een slash (/) beginnen.',
			'from.unique' => 'Voor From ":input" bestaat al een redirect.'
		];
	}

	/**
	 * When all input is checked for syntax, check whether the newly suggested redirect could lead to too many
	 * redirects (e.g. with circular references). Only allow upto 5 redirects.
	 */
	public function withValidator($validator) {
		$validator->after(function ($validator) {
			if (!Redirect::checkAll($this->from, $this->to, true)) {
				$validator->errors()->add('to', 'Deze redirect leidt tot te veel opeenvolgende  redirects.');
			}
		});
	}
}
