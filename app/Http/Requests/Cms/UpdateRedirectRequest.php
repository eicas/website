<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Requests\Cms;

use Illuminate\Validation\Rule;

class UpdateRedirectRequest extends BaseRedirectRequest {
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'from' => [
				'required',
				'string',
				'starts_with:/',
				'between:2,255',
				Rule::unique('redirects')->ignore($this->route('redirect')->id),
			],
			'to' => 'required|string|starts_with:/|max:255',
			'comment' => 'nullable|string|max:255',
		];
	}
}
