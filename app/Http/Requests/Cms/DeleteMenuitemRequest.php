<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Http\Requests\Cms;

use Illuminate\Foundation\Http\FormRequest;

class DeleteMenuitemRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return $this->user()->can('delete', $this->route('menuitem'));
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [];
	}

	/**
	 * Make sure the menuitem is not the parent of others.
	 */
	public function withValidator($validator) {
		$validator->after(function ($validator) {
			if (count($this->menuitem->menuitems)) {
				$validator->errors()->add('menuitem', 'Not empty!');
			}
		});
	}
}
