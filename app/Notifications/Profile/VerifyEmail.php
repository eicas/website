<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Notifications\Profile;

use Illuminate\Auth\Notifications\VerifyEmail as BaseClass;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;

class VerifyEmail extends BaseClass {
	/**
	 * Get the verify email notification mail message for the given URL.
	 *
	 * @param string $url
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	protected function buildMailMessage($url) {
		return (new MailMessage)
			->subject('Bevestig e-mailadres')
			->line('Klik onderstaande knop om uw e-mailadres te bevestigen.')
			->action('Bevestig e-mailadres', $url)
			->line('Als u geen account heeft gemaakt, hoeft u geen actie te ondernemen.');
	}

	/**
	 * Get the verification URL for the given notifiable.
	 *
	 * @param  mixed  $notifiable
	 * @return string
	 */
	protected function verificationUrl($notifiable) {
		if (static::$createUrlCallback) {
			return call_user_func(static::$createUrlCallback, $notifiable);
		}

		return URL::temporarySignedRoute(
			'profile.verification.verify',
			Carbon::now()->addMinutes(Config::get('auth.verification.expire', 60)),
			[
				'id' => $notifiable->getKey(),
				'hash' => sha1($notifiable->getEmailForVerification()),
			]
		);
	}
}
