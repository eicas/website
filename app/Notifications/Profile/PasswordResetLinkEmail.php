<?php

// SPDX-FileCopyrightText: 2022 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Notifications\Profile;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;

/**
 * This class overrides some of the functions in the base class \Illuminate\Auth\Notifications\ResetPassword so that
 * the emails sent have the correct link and are in Dutch.
 */
class PasswordResetLinkEmail extends ResetPassword {
	/**
	 * Build the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable) {
		if (static::$toMailCallback) {
			return call_user_func(static::$toMailCallback, $notifiable, $this->token);
		}

		if (static::$createUrlCallback) {
			$url = call_user_func(static::$createUrlCallback, $notifiable, $this->token);
		} else {
			$url = url(route('profile.password.reset.edit', [
				'token' => $this->token,
				// 'email' => $notifiable->getEmailForPasswordReset(),
			], false));
		}

		return $this->buildMailMessage($url);
	}

	/**
	 * Get the reset password notification mail message for the given URL.
	 *
	 * @param  string  $url
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	protected function buildMailMessage($url) {
		return (new MailMessage)
			->subject(__('passwords.password-reset.subject'))
			->line(__('passwords.password-reset.introduction'))
			->action(__('passwords.password-reset.link'), $url)
			->line(__(
				'passwords.password-reset.description',
				['count' => config('auth.passwords.' . config('auth.defaults.passwords') . '.expire')]
			))
			->line(__('passwords.password-reset.disclaimer'));
	}
}
