<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Policies;

use App\Models\GuestForms\NewFriend;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class NewFriendPolicy {
	use HandlesAuthorization;

	/**
	 * Perform pre-authorization checks.
	 *
	 * @param \App\Models\User $user
	 * @param string $ability
	 * @return void|bool
	 */
	public function before(User $user, $ability) {
		logger("NewFriendPolicy::before");
		if ($user->hasRole('admin')) {
			return true;
		}
	}

	/**
	 * Determine whether the user can do administrative tasks on the resource.
	 *
	 * @param \App\Models\User $user
	 * @return mixed
	 */
	public function admin(User $user) {
		logger("NewFriendPolicy::admin");
		return $user->hasRole('friend-admin');
	}

	/**
	 * Determine whether the user can view any models.
	 *
	 * @param \App\Models\User $user
	 * @return mixed
	 */
	public function viewAny(User $user) {
		logger("NewFriendPolicy::viewAny");
		return $user->hasRole('friend-admin');
	}

	/**
	 * Determine whether the user can view the model.
	 *
	 * @param \App\Models\User $user
	 * @param \App\Models\NewFriend $newFriend
	 * @return mixed
	 */
	public function view(User $user, NewFriend $newFriend) {
		logger("NewFriendPolicy::view");
		return $user->hasRole('friend-admin');
	}

	/**
	 * Determine whether the user can create models.
	 *
	 * @param \App\Models\User $user
	 * @return mixed
	 */
	public function create(User $user = null) {
		logger("NewFriendPolicy::create");
		return true;
	}

	/**
	 * Determine whether the user can update the model.
	 *
	 * @param \App\Models\User $user
	 * @param \App\Models\NewFriend $newFriend
	 * @return mixed
	 */
	public function update(User $user, NewFriend $newFriend) {
		logger("NewFriendPolicy::update");
		return $user->hasRole('friend-admin');
	}

	/**
	 * Determine whether the user can delete the model.
	 *
	 * @param \App\Models\User $user
	 * @param \App\Models\NewFriend $newFriend
	 * @return mixed
	 */
	public function delete(User $user, NewFriend $newFriend) {
		logger("NewFriendPolicy::delete");
		return $user->hasRole('friend-admin');
	}

	/**
	 * Determine whether the user can restore the model.
	 *
	 * @param \App\Models\User $user
	 * @param \App\Models\NewFriend $newFriend
	 * @return mixed
	 */
	public function restore(User $user, NewFriend $newFriend) {
		logger("NewFriendPolicy::restore");
		return $user->hasRole('friend-admin');
	}

	/**
	 * Determine whether the user can permanently delete the model.
	 *
	 * @param \App\Models\User $user
	 * @param \App\Models\NewFriend $newFriend
	 * @return mixed
	 */
	public function forceDelete(User $user, NewFriend $newFriend) {
		logger("NewFriendPolicy::forceDelete");
		return false;
	}
}
