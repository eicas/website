<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Policies;

use App\Models\Download;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DownloadPolicy {
	use HandlesAuthorization;

	/**
	 * Perform pre-authorization checks.
	 *
	 * @param \App\Models\User $user
	 * @param string $ability
	 * @return void|bool
	 */
	public function before(User $user, $ability) {
		if ($user->hasRole('admin')) {
			return true;
		}
	}

	/**
	 * Determine whether the user can do administrative tasks on the resource.
	 *
	 * @param \App\Models\User $user
	 * @return mixed
	 */
	public function admin(User $user) {
		return $user->hasRole('text-admin');
	}

	/**
	 * Determine whether the user can view any models.
	 *
	 * @param \App\Models\User $user
	 * @return mixed
	 */
	public function viewAny(User $user) {
		return $user->hasRole('text-admin');
	}

	/**
	 * Determine whether the user can view the model.
	 *
	 * @param \App\Models\User $user
	 * @param \App\Models\Download $download
	 * @return mixed
	 */
	public function view(User $user, Download $download) {
		return $user->hasRole('text-admin');
	}

	/**
	 * Determine whether the user can create models.
	 *
	 * @param \App\Models\User $user
	 * @return mixed
	 */
	public function create(User $user) {
		return $user->hasRole('text-admin');
	}

	/**
	 * Determine whether the user can update the model.
	 *
	 * @param \App\Models\User $user
	 * @param \App\Models\Download $download
	 * @return mixed
	 */
	public function update(User $user, Download $download) {
		return $user->hasRole('text-admin');
	}

	/**
	 * Determine whether the user can delete the model.
	 *
	 * @param \App\Models\User $user
	 * @param \App\Models\Download $download
	 * @return mixed
	 */
	public function delete(User $user, Download $download) {
		return $user->hasRole('text-admin');
	}

	/**
	 * Determine whether the user can restore the model.
	 *
	 * @param \App\Models\User $user
	 * @param \App\Models\Download $download
	 * @return mixed
	 */
	public function restore(User $user, Download $download) {
		return $user->hasRole('text-admin');
	}

	/**
	 * Determine whether the user can permanently delete the model.
	 *
	 * @param \App\Models\User $user
	 * @param \App\Models\Download $download
	 * @return mixed
	 */
	public function forceDelete(User $user, Download $download) {
		return false;
	}
}
