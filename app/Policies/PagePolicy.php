<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Policies;

use App\Models\Page;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PagePolicy {
	use HandlesAuthorization;

	/**
	 * Perform pre-authorization checks.
	 *
	 * @param  \App\Models\User  $user
	 * @param  string  $ability
	 * @return void|bool
	 */
	public function before(User $user, $ability) {
		if ($user->hasRole('admin')) {
			return true;
		}
	}

	/**
	 * Determine whether the user can do administrative tasks on the resource.
	 *
	 * @param \App\Models\User $user
	 * @return mixed
	 */
	public function admin(User $user) {
		return $user->hasRole('text-admin');
	}

	/**
	 * Determine whether the user can view any models.
	 *
	 * @param \App\Models\User $user
	 * @return mixed
	 */
	public function viewAny(User $user) {
		return $user->hasRole('text-admin');
	}

	/**
	 * Determine whether the user can view the model.
	 *
	 * @param \App\Models\User $user
	 * @param \App\Models\Page $page
	 * @return mixed
	 */
	public function view(User $user, Page $page) {
		return $user->hasRole('text-admin');
	}

	/**
	 * Determine whether the user can create models.
	 *
	 * @param \App\Models\User $user
	 * @return mixed
	 */
	public function create(User $user) {
		return $user->hasRole('text-admin');
	}

	/**
	 * Determine whether the user can update the model.
	 *
	 * @param \App\Models\User $user
	 * @param \App\Models\Page $page
	 * @return mixed
	 */
	public function update(User $user, Page $page) {
		return $user->hasRole('text-admin');
	}

	/**
	 * Determine whether the user can delete the model.
	 *
	 * @param \App\Models\User $user
	 * @param \App\Models\Page $page
	 * @return mixed
	 */
	public function delete(User $user, Page $page) {
		return $user->hasRole('text-admin');
	}

	/**
	 * Determine whether the user can restore the model.
	 *
	 * @param \App\Models\User $user
	 * @param \App\Models\Page $page
	 * @return mixed
	 */
	public function restore(User $user, Page $page) {
		return $user->hasRole('text-admin');
	}

	/**
	 * Determine whether the user can permanently delete the model.
	 *
	 * @param \App\Models\User $user
	 * @param \App\Models\Page $page
	 * @return mixed
	 */
	public function forceDelete(User $user, Page $page) {
		return false;
	}
}
