<?php

// SPDX-FileCopyrightText: 2021 - 2022 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider {
	/**
	 * The path to the "home" route for your application.
	 *
	 * This is used by Laravel authentication to redirect users after login.
	 *
	 * @var string
	 */
	public const HOME = '/home';

	/**
	 * The controller namespace for the application.
	 *
	 * When present, controller route declarations will automatically be prefixed with this namespace.
	 *
	 * @var string|null
	 */
	// protected $namespace = 'App\\Http\\Controllers';

	/**
	 * Define your route model bindings, pattern filters, etc.
	 *
	 * @return void
	 */
	public function boot() {
		$this->configureRateLimiting();

		Route::resourceVerbs([
			'create' => 'nieuw',
			'edit' => 'aanpassen',
		]);

		$this->routes(function () {
			Route::prefix('api')
				->middleware('api')
				->namespace($this->namespace)
				->group(base_path('routes/api.php'));

			Route::middleware('web')
				->namespace($this->namespace)
				->group(base_path('routes/web.php'));
		});
	}

	/**
	 * Configure the rate limiters for the application.
	 *
	 * @return void
	 */
	protected function configureRateLimiting() {
		RateLimiter::for('global', function (Request $request) {
			// get the original client ip using the header as set by Cloudflare
			$ip = $request->header('CF-Connecting-IP', false);
			// if the CF original ip was not found, rate limit to 60 p.m. per ip
			if ($ip === false) {
				return Limit::perMinute(10)->by($request->ip());
			}
			// With the original ip (obtained from Cloudflare header), limit to 20 requests per minute.
			return Limit::perMinute(20)->by($ip);
		});
		RateLimiter::for('api', function (Request $request) {
			return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
		});
		RateLimiter::for('registration', function (Request $request) {
			return [
				Limit::perMinute(60),
				Limit::perMinute(5)->by($request->ip()),
			];
		});
		RateLimiter::for('login', function (Request $request) {
			return Limit::perMinute(5)->by($request->email . $request->ip());
		});
		RateLimiter::for('confirm', function (Request $request) {
			return Limit::perMinute(5);
		});
		RateLimiter::for('verify', function (Request $request) {
			return Limit::perMinute(5);
		});
		RateLimiter::for('mfa', function (Request $request) {
			return Limit::perMinute(5)->by($request->session()->get('login.id'));
		});
	}
}
