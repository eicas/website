<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Providers;

// use App\Models\Menuitem;
// use App\Models\Page;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class ViewServiceProvider extends ServiceProvider {

	protected $view_stack = [];

	/**
	 * Register services.
	 *
	 * @return void
	 */
	public function register() {
		//
	}

	/**
	 * Bootstrap services.
	 *
	 * @return void
	 */
	public function boot() {

		/**
		 * For the 'user' layout, provide two variables (only if there is a logged in user):
		 * 
		 * [user] The currently loged in user.
		 * [crumbs] A sersies of links / labels yielding a clickable bread crumb path of the current page.
		 */
		View::composer('layouts.user', function ($view) {
			if (Auth::check()) {
				$crumbs = [[
					'label' => self::getTitle('dashboard'),
					'link' => route('user.home'),
				]];
				$currentRouteName = Route::currentRouteName();
				// show more crumbs for routes starting with
				// - cms
				// - profiles
				if (preg_match('/^(?:cms|profiles)\./', $currentRouteName)) {
					$parts = explode(".", $currentRouteName);
					if (last($parts) === 'index') {
						array_pop($parts);
					}
					$count = count($parts);
					for ($i = 0; $i <= $count; $i++) {
						$routeNameBase = implode(".", array_slice($parts, 0, $i));
						foreach (['.index', '.edit'] as $action) {
							$routeName = $routeNameBase . $action;
							// logger("route name: '$routeName' (i=$i/$count)");
							if (Route::has($routeName)) {
								$label_key = $parts[$i - 1];
								if ($action === '.edit') {
									$label_key = Str::singular($label_key);
								}
								// logger("exists. Proposed label: '$label_key'");

								$expected_parameters = Route::getRoutes()->getByName($routeName)->parameterNames();
								$parameters = null;
								$add = true;
								if (count($expected_parameters)) {
									// logger("expected parameters: {" . implode("}, {", $expected_parameters) . "}");
									// logger("current route parameters: {" . implode(
									// 	"}, {",
									// 	array_keys(Route::current()->parameters())
									// ) . "}");
									$parameters = array_intersect_key(
										Route::current()->parameters(),
										array_flip($expected_parameters)
									);
									// logger("parameters: {" . implode("}, {", array_keys($parameters)) . "}");
									$add = count($expected_parameters) === count($parameters);
								}
								if ($add) {
									// logger("adding crumb");
									$crumbs[] = [
										'label' => self::getTitle($label_key),
										'link' => route($routeName, $parameters),
									];
								// } else {
								// 	logger("skipping crumb");
								}
							}
						}
					}
					unset($crumbs[count($crumbs) - 1]['link']);
				} else {
					// crumbs for specific routes
					switch ($currentRouteName) {
						case 'profile.show':
							$crumbs[] = ['label' => self::getTitle('profile')];
						break;
						case 'guest_forms.new_friend.index':
							$crumbs[] = ['label' => self::getTitle('new_friends')];
						break;
					}
				}
				$view->with([
					'user' => Auth::user(),
					'crumbs' => $crumbs,
				]);
			}
		});

		/**
		 * For the 'main' layout, provide three standard variables:
		 *
		 * [menuitems] All menu items for the current language.
		 * [language] The current language.
		 * [languages] All actually available languages. I.e. only languages that are 'accepted' (config) and actually
		 *   used.
		 */
		View::composer('layouts.guest', function ($view) {
			// $language = Route::getCurrentRoute()->parameter('language', null);

			// $languages = Page::select('language')->isPublic()->distinct()->get()->pluck('language')->all();
			// $languages = array_intersect_key(config('app.accepted_locales', []), array_combine($languages, $languages));
	
			// $view->with([
			// 	'menuitems' => Menuitem::forLanguage($language)->with('page')->orderBy('order')->get(),
			// 	'language' => $language,
			// 	'languages' => $languages,
			// ]);
		});

		/**
		 * For both layouts ('main' and 'admin'), provide one additional variable:
		 * 
		 * [css_classes] A space-separated list of class names which depend on the current route name. A route name
		 *   usually consists of a dot-separated collection of strings. These are converted into classes, such that
		 *   e.g. a route with name 'cms.pages.index' will yield the following classes:
		 *   - cms
		 *   - cms-pages
		 *   - cms-pages-index
		 *   These classes can be assigned to the 'main' html element, to provide a uniform way to apply page or page
		 *   group specific styles.
		 */
		View::composer(['layouts.user', 'layouts.guest'], function ($view) {
			$css_classes = [];
			if ($current_route_name = Route::currentRouteName()) {
				$route_parts = explode(".", $current_route_name);
				$count = count($route_parts);
				for ($i = 1; $i <= $count; $i++) {
					$css_classes[] = implode("-", array_slice($route_parts, 0, $i));
				}
			}
			$css_classes = implode(" ", $css_classes);
			$view->with('css_classes', $css_classes);
		});
	}

	/**
	 * Based on a key, return a translated title string. If a translation does not exist, return the key.
	 * Translations are searched for in the /resources/lang/<language>/crumbs.php files.
	 * 
	 * @param string $key Some translatable key from the 'crumbs' translation section.
	 * @return string
	 */
	protected static function getTitle(string $key): string {
		$translated = trans("crumbs.$key");
		if (Str::startsWith($translated, 'crumbs.')) {
			$translated = $key;
		}
		if ($key === 'profiles') {
			// add the set of users if known
		}
		return $translated;
	}
}
