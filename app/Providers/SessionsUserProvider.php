<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Providers;

use App\Models\User;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use Illuminate\Support\Facades\Auth;

class SessionsUserProvider extends EloquentUserProvider {
	/**
	 * Retrieve a user by their unique identifier and "remember me" token.
	 *
	 * This method is overridden from the default EloquentUserProvider. The current user implementation does not have
	 * a remember token anymore. Instead, a user instance can be associated with multiple UserSession records, which
	 * in turn can have a remember token. Thus, this method should not just look for a token in the user. Instead, it
	 * should find the proper session for the given token. Also, it is necessary to check whether that token is
	 * actually still valid.
	 *
	 * @param mixed $identifier
	 * @param string $token
	 * @return \Illuminate\Contracts\Auth\Authenticatable|null
	 */
	public function retrieveByToken($identifier, $token) {
		// logger("[SessionsUserProvider] retrieveByToken(id=$identifier, token='$token')");
		$user = User::with('userSessions')->find($identifier);

		// if the user wasn't found at all, just return null
		if (!$user) {
			// logger("[SessionsUserProvider] user not found by id");
			return;
		}

		// check whether the given token is actually a valid token. If so, the user model can be returned.
		// If not, return null.
		// To prevent timing attacks, it is necessary to actually traverse all active sessions of the user found and
		// compare the tokens, since we cannot rely on timing attack prevention in the string comparison of tokens in
		// e.g. the database.
		// Note that the current implementation relies on a shortcut for if a token is found. It is considered a non-
		// issue to "know" something about the number of sessions for this user.
		// Note also that "invalidating" a token can be done by simply soft-deleting the session with this token, as
		// this routine does not fetch soft-deleted models.
		// logger("[SessionsUserProvider] checking userSessions for matching token");
		// logger($user);
		foreach ($user->userSessions as $userSession) {
			// logger("[SessionsUserProvider] compare token to userSession token '{$userSession->remember_token}'");
			// not all userSessions will have tokens, skip those
			if (!$userSession->remember_token) {
				continue;
			}
			if (hash_equals($userSession->remember_token, $token)) {
				// logger("[SessionsUserProvider] token match for userSession id={$userSession->id}, return user.");
				$user->userSession = $userSession;
				return $user;
			}
		}
		// logger("[SessionsUserProvider] no matching userSession found, return no user.");
	}

	/**
	 * Update the "remember me" token for the given user in storage.
	 *
	 * This method is overridden from the default EloquentUserProvider. The current user implementation does not have
	 * a remember token anymore. Instead, a user instance can be associated with multiple UserSession records, which
	 * in turn can have a remember token. Thus, this method should not just set a token for a user. Instead, it should
	 * use the current session and set the given token there.
	 *
	 * Since the token is saved to a UserSession instance instead of on the User model itself, it is not necessary
	 * anymore to save the User instance (and prevent a timestamps update with that). Therefore, the only difference
	 * between the current and original implementation is that here, the user is not saved at all.
	 *
	 * @param  \Illuminate\Contracts\Auth\Authenticatable|\Illuminate\Database\Eloquent\Model  $user
	 * @param  string  $token
	 * @return void
	 */
	public function updateRememberToken(UserContract $user, $token) {
		// logger("[SessionsUserProvider] updateRememberToken(user:(id={$user->id}), token='$token')");
		$user->setRememberToken($token);
	}
}
