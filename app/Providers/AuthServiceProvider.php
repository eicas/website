<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use App\Providers\SessionsUserProvider;
use App\Services\Auth\UserSessionsGuard;
use Illuminate\Http\Request;

class AuthServiceProvider extends ServiceProvider {
	/**
	 * The policy mappings for the application.
	 *
	 * @var array
	 */
	protected $policies = [
		// 'App\Models\Model' => 'App\Policies\ModelPolicy',
	];

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register() {
		// make sure the application uses the SessionsUserProvider as user provider
		Auth::provider('sessionsuserprovider', function($app, array $config) {
			return new SessionsUserProvider($app['hash'], $config['model']);
		});
		// make sure the application uses the UserSessionsGuard as session guard
		Auth::extend('usersessionsguard', function($app, $name, array $config) {
			$guard = new UserSessionsGuard(
				$name,
				Auth::createUserProvider($config['provider']),
				$app['session.store'],
				$app->request
			);
			$guard->setCookieJar($app['cookie']);
			return $guard;
		});
		//
	}

	/**
	 * Register any authentication / authorization services.
	 *
	 * @return void
	 */
	public function boot() {
		$this->registerPolicies();

	}

	public static function userHome() {
		$user = Auth::user();
		if (!$user) {
			return '/';
		}
		// if this is a registered EICAS user, i.e. an involved user, go to the dashboard
		if (true) {
			return '/dashboard';
		}
		// else, i.e. user is some external person, e.g. a 'friend', go to user page
		return route('profile.show');
	}
}
