<!--
SPDX-FileCopyrightText: 2021 - 2022 Marten Koetsier <eicas@koetsierengineering.nl>
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>

SPDX-License-Identifier: MIT
-->

## About EICAS

European Institute for Contemporary Art and Science

See [https://www.eicas.nl](https://www.eicas.nl)

This project houses the main EICAS website.

### Prerequisites

* php-cli
* php-mysql
* nodejs / npm
* composer
* a mariadb/mysql database, either from LAMPP, XAMPP, in docker, or natively

### Running locally

Copy `.env.example` to `.env` and adapt it to your local needs.
In particular make sure you have a mysql/mariadb database running
and point the `DB_` variables to it.
Note: The `php artisan key:generate` command listed below creates a new (secret!) key for encrypting in the app. If the original key is lost, all encryted / hashed secrets will be lost as well. Thus, generating a new key (and with that overwriting the existing) will invalidate all user accounts. This command must only be run if that is intended (or if there is no key yet, for a completely new install, for example for a local test).

Building:

```
composer install
npm install
npm run prod
php artisan key:generate
php artisan migrate:fresh --seed

```

Running:

```
php artisan serve
```
### CMS

Log in to website's CMS by navigating to

```
http://localhost:8000/cms/
```

Login name: `admin@example.com`
Password: `hello`

If you have a sql database file available to import into your local site's database, the above generic user account will be wiped out. It is possible to restore this standard account by removing the `if`-statement from `/database/seeders/UserSeeder.php`. (You can comment out lines 22 – 23 – 24 and 33 and save the file.)

Run:

```
php artisan db:seed --class=UserSeeder
```

Restoring a database backup:

```
docker exec -i $(docker ps | grep mariadb | cut -d " " -f 1) mysql -u $(cat .env | grep DB_USERNAME | cut -d "=" -f 2) $(cat .env | grep DB_DATABASE | cut -d "=" -f 2) -p$(cat .env | grep DB_PASSWORD | cut -d "=" -f 2) < ../eicas_site1-db-20220403.sql
```
