// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
	.options({
		processCssUrls: false
	})
	.combine('resources/js/image.js', 'public/js/image.js')
	.copyDirectory('resources/fonts', 'public/fonts')
	.copyDirectory('resources/img', 'public/img')
	.copyDirectory('resources/favicon', 'public')
	.sass('resources/css/guest.scss', 'public/css')
	.sass('resources/css/user.scss', 'public/css')
	.copyDirectory('node_modules/ckeditor4', 'public/js/ckeditor4')
	.copyDirectory('resources/js/cms', 'public/js/cms')
	.sass('resources/css/ckeditor4-cms.scss', 'public/css')
	.version();
