<?php

// SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>
// SPDX-FileCopyrightText: 2022 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

/**
 * Creation of the required database tables:
 *
 * CREATE TABLE `suspends` (
 *   `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
 *   `starts_at` datetime NOT NULL,
 *   `ends_at` datetime NOT NULL,
 *   `comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
 *   PRIMARY KEY (`id`),
 *   KEY `period` (`starts_at`, `ends_at`)
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 *
 * CREATE TABLE `visits` (
 *   `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
 *   `visited_at` datetime NOT NULL,
 *   `ip` varchar(64) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
 *   `method` varchar(8) NOT NULL,
 *   `path` varchar(128) NOT NULL,
 *   `status` smallint(5) UNSIGNED NOT NULL,
 *   `size` int(10) UNSIGNED NOT NULL,
 *   `cache` int(10) UNSIGNED NOT NULL,
 *   PRIMARY KEY (`id`),
 *   KEY `visited_at` (`visited_at`)
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 *
 */

$starttime = microtime(true);

$log_output = __DIR__ . '/datalimit.log';
$output = [
	"datalimit version " . date('Ymd-His', filemtime(__FILE__)),
];

require '../datalimit_config.php';

$dsn = "mysql:host=localhost;dbname={$db_name};charset=UTF8";
$dso = [
	\PDO::ATTR_EMULATE_PREPARES => true,
	\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
];
$pdo = new \PDO($dsn, $db_user, $db_pass, $dso);
if ($pdo) {
	$output[] = "db ok";
}

$now = (new DateTime())->format('Y-m-d H:i:s');

// check whether the site is temporarily suspended
try {
	$st = $pdo->query(
		"select min(`starts_at`), max(`ends_at`), `comment` from `suspends` where '$now' between `starts_at` and `ends_at`",
		PDO::FETCH_NUM
	);
	$suspend = $st->fetch(PDO::FETCH_NUM);
	$st = null;
	if ($suspend && $suspend[0]) {
		$output[] = "suspend from {$suspend[0]} to {$suspend[1]} '{$suspend[2]}'";
		header('X-Response-Code: 429', true, 429);
		echo "Deze site is tijdelijk buiten gebruik. Gelieve binnenkort nog eens te proberen.<br>\n";
		echo "This site is temporarily out of order. Please try again soon.<br>\n";
		// the comment ($suspend[2]) may contain sensitive information on bytes sent and allowed data transfer
		// if ($suspend[2]) {
		// 	echo $suspend[2], "<br>\n";
		// }
		exit;
	}
	$output[] = "site is currently not suspended.";
} catch (Exception $e) {
	$output[] = "({$e->getCode()}) {$e->getMessage()} ({$e->getLine()})";
}

// everything is OK, prepare the output

$uri = $_SERVER['REQUEST_URI'];
$output[] = "$now $uri";
$path = explode("?", $uri, 2)[0];
$visit = [
	'visited_at' => (new DateTime())->format('Y-m-d H:i:s'),
	'ip' => $_SERVER['HTTP_CF_CONNECTING_IP'] ?? $_SERVER['REMOTE_ADDR'],
	'method' => $_SERVER['REQUEST_METHOD'],
	'path' => substr($path, 0, 128),
	'status' => 200,
	'size' => 0,
	'cache' => 0,
];
foreach ($visit as $k => $v) {
	$output[] = "$k: $v";
}

// prepare path to path to file to execute/output
if ($path === '/') {
	$path = '/index.php';
	$output[] = "found '/', pass to index";
}
$file = __DIR__ . $path;

if (!is_readable($file)) {
	$path = '/index.php';
	$file = __DIR__ . $path;
	$output[] = "file not found, pass to index";
}
if (is_dir($file)) {
	// for any requested dir, just redirect to root
	header('Location: /');
}

// add this header for debugging purposes (see if the script is actually used for a request)
header("X-KE-DL: " . date('YmdHis', filemtime(__FILE__)));

// handle the file: execute existing php-files, output others, discard some specific requests
if (preg_match('/.+\.php$/', $path) && is_readable($file)) {
	$output[] = "include php file '$file'";
	// use output buffering to fetch the number of bytes sent.
	// Note: some scripts (e.g. Laravel framework) make sure to disable all output buffering (under certain conditions).
	// Therefore, a normal flow of starting buffering and finally outputting the resulting buffer may not always work.
	// Instead, call ob_start with a callback so that buffering is forced to be effective.
	ob_start(function ($buffer) {
		global $visit, $output;
		// echo $buffer;
		$visit['size'] = strlen($buffer);
		$visit['status'] = http_response_code();
		insertVisit($visit);
		$output[] = "buffer has {$visit['size']} bytes.";
		$output[] = substr($buffer, 0, 1024) . "\n";
		writeLog();
		return false;
	});
	include $file;
	$output[] = "script finished, end-clean output buffer";
	if (ob_get_level() > 0) {
		ob_end_flush();
	}
	$output[] = "output buffer cleaned";
} elseif (preg_match('#/(wp-content|wp-includes|wordpress|wp|wp-admin|wp-login)/#i', $path)) {
	$output[] = "path to some wordpress endpoint, discard";
	$visit['status'] = 410;
	insertVisit($visit);
	writeLog(true);
	header(($_SERVER["SERVER_PROTOCOL"] ?? 'HTTP/1.1') . " 410 Gone");
} else {
	$output[] = "output file '$file'";

	$request_headers = apache_request_headers();
	if (!$request_headers) {
		$request_headers = [];
	}
	foreach ($request_headers as $k => $v) {
		$request_headers[strtolower($k)] = $v;
	}

	$content_type = getMimeType($file);
	header('Content-Type: ' . $content_type);
	//header("Access-Control-Allow-Origin: *");
	$output[] = "Content-Type set: '$content_type'";

	// allow caching on some resources.
	$cache_duration = 0;
	switch (pathinfo($file, PATHINFO_EXTENSION)) {
		case 'js':
		case 'css':
		case 'png':
		case 'jpg':
		case 'jpeg':
		case 'ico':
		case 'svg':
			$cache_duration = 2592000; // 30 days
			break;
		case 'json':
		case 'config':
		case 'webmanifest':
			$cache_duration = 300; // 5 minutes
			break;
		case 'woff':
		case 'woff2':
			$cache_duration = 31536000; // 365 days
			break;
	}
	if ($cache_duration > 0) {
		$scope = preg_match('#^/(' . implode("|", [
			'css/',
			'fonts/',
			'img/',
			'js/',
			'apple-touch-icon\.png',
			'favicon\.ico',
			'favicon.webmanifest',
			'icon(?:-\d\d\d)?\....'
		]) . ')#i', $path) ? 'public' : 'private';
		$header = "Cache-Control: $scope, max-age=$cache_duration";
		header($header);
		$output[] = "Cache-Control header set: $header";
	}

	$visit['size'] = filesize($file);
	$visit['cache'] = $cache_duration;
	header('Content-Length: ' . $visit['size']);
	if ($visit['path'] !== '/favicon.ico') {
		insertVisit($visit);
		writeLog(true);
	}
	readfile($file);
}


// end

function getMimeType($file): string {
	switch (strtolower(pathinfo($file, PATHINFO_EXTENSION))) {
		case 'css': return "text/css";
		case 'csv': return "text/csv";
		case 'eot': return "application/vnd.ms-fontobject";
		case 'gz': return "application/gzip";
		case 'htm': return "text/html";
		case 'html': return "text/html";
		case 'ics': return "text/calendar";
		case 'js': return "text/javascript";
		case 'json': return "application/json";
		case 'jsonld': return "application/ld+json";
		case 'mid': return "audio/midi";
		case 'midi': return "audio/midi";
		case 'mjs': return "text/javascript";
		case 'odp': return "application/vnd.oasis.opendocument.presentation";
		case 'ods': return "application/vnd.oasis.opendocument.spreadsheet";
		case 'odt': return "application/vnd.oasis.opendocument.text";
		case 'pdf': return "application/pdf";
		case 'rtf': return "application/rtf";
		case 'sh': return "application/x-sh";
		case 'svg': return "image/svg+xml";
		case 'tar': return "application/x-tar";
		case 'ttf': return "font/ttf";
		case 'txt': return "text/plain";
		case 'woff': return "font/woff";
		case 'woff2': return "font/woff2";
		case 'xhtml': return "application/xhtml+xml";
		case 'xml': return "application/xml";
		case 'zip': return "application/zip";
	}
	return mime_content_type($file);
}

function insertVisit($visit) {
	global $output, $pdo;
	try {
		$values = [
			$visit['visited_at'],
			$visit['ip'],
			$visit['method'],
			$visit['path'],
			$visit['status'],
			$visit['size'],
			$visit['cache'],
		];
		
		$st = $pdo->prepare("insert into visits (`visited_at`, `ip`, `method`, `path`, `status`, `size`, `cache`) values (?, ?, ?, ?, ?, ?, ?)");
		$st->execute($values);
		$output[] = "visit written: " . implode(" / ", $values);
	} catch (Exception $e) {
		// silent
		$output[] = "error writing visit: ({$e->getCode()}) {$e->getMessage()} (line {$e->getLine()})";
	}
	$st = null;
}

function writeLog(bool $append = false) {
	global $log_output, $output, $starttime;
	if (!$log_output || !$output) {
		return;
	}
	$output[] = sprintf("script took %.3fms", (microtime(true) - $starttime) * 1000);
	file_put_contents($log_output, implode("\n", $output), $append ? FILE_APPEND : 0);
}
