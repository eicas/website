<?php

// SPDX-FileCopyrightText: 2022 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

$log_output = __DIR__ . '/datalimit_check.log';
$output = [];

require dirname(__DIR__) . '/datalimit_config.php';

$now = date('Y-m-d H:i:s');
$start = date('Y-m-d H:i:s', time() - $period);
$output[] = substr($start, 0, 10);
$output[] = "v" . date('YmdHi', filemtime(__FILE__));
$output[] = sprintf("[%s - %s]", substr($start, 11, 8), substr($now, 11, 8));

$dsn = "mysql:host=localhost;dbname={$db_name};charset=UTF8";
$dso = [
	\PDO::ATTR_EMULATE_PREPARES => true,
	\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
];
$pdo = new \PDO($dsn, $db_user, $db_pass, $dso);
if (!$pdo) {
	$output[] = "could not connect to db";
}

// check how many bytes have been served during the past period
try {
	$st = $pdo->query(
		"select sum(`size`) from `visits` where `visited_at` between '$start' and '$now'",
		PDO::FETCH_NUM
	);
	$bytes = $st->fetch(PDO::FETCH_NUM);
	$st = null;
	if ($bytes && $bytes[0]) {
		$output[] = "[{$bytes[0]}] bytes";
		$format = 0;
		$format_bytes = $bytes[0];
		while ($format < 4 && $format_bytes > 1024) {
			$format++;
			$format_bytes = $format_bytes / 1024;
		}
		if ($format > 1) {
			$output[] = sprintf("(%.1f%sB)", $format_bytes, ['', 'K', 'M', 'G', 'T'][$format]);
		}
		// if the amount of bytes exceeds the maximum, the site should be suspended
		$bytes = intval($bytes[0], 10);
		if ($bytes > $max_bytes) {
			// check whether the site is suspended already and 
			$st = $pdo->query(
				"select min(`starts_at`), max(`ends_at`), `comment` from `suspends` where '$now' between `starts_at` and `ends_at`",
				PDO::FETCH_NUM
			);
			$suspend = $st->fetch(PDO::FETCH_NUM);
			$st = null;
			if ($suspend && $suspend[0]) {
				$output[] = "already suspended from {$suspend[0]} to {$suspend[1]} '{$suspend[2]}'";
			} else {
				$st = $pdo->prepare("insert into `suspends` (`starts_at`, `ends_at`, `comment`) values (?, ?, ?)");
				$st->execute([
					$now,
					date('Y-m-d H:i:s', time() + $suspend_duration),
					"data transfer ($bytes bytes) exceeds limit ($max_bytes bytes)"
				]);
				$output[] = sprintf(
					"site %s has been suspended for %ds after data transfer (%dB) exceeded limit (%dB)",
					$_SERVER['HTTP_HOST'] ?? $_SERVER['SERVER_NAME'] ?? $_SERVER['DOCUMENT_ROOT'] ?? __FILE__,
					$suspend_duration,
					$bytes,
					$max_bytes
				);
				mail('datalimit@koetsierengineering.nl', "datalimit reached", implode("\r\n", $output));
			}
		}
	}
} catch (Exception $e) {
	$output[] = "({$e->getCode()}) {$e->getMessage()} ({$e->getLine()})";
	mail('datalimit@koetsierengineering.nl', "datalimit error", implode("\r\n", $output));
}

file_put_contents($log_output, implode(" ", $output) . "\n", FILE_APPEND);
