<?php

// SPDX-FileCopyrightText: 2023 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

/**
 * This stand-alone script can be called (e.g. periodically using cron) to receive an email message
 * detailing the current status of the traffic-data used in relation to the limits.
 * 
 * Configuration:
 * A config file "../config/usage-mail.env" should exist with configuration options, as detailed
 * below. While reading this file, empty lines and lines staring with a hash-sign (#) or semi-colon
 * (;) are ignored. Lines starting with a term in square brackets ([ and ]) start "sections", used
 * for various part of the configuration.
 * 
 * [secret_token]
 * 
 * This section should contain one line with some random string that is used as a secret access
 * token. If the script is called without this token, it does nothing.
 * 
 * [email] Configuration of recipients
 * 
 * A section must exist named [email]. It must contain one or more email addresses to send this
 * message to. Each address on a separate line. Only valid email addresses are considered, checked
 * with regex:
 *     /^[a-z0-9]([_+.-]?[a-z0-9]+)*@[a-z0-9](-?[a-z0-9]+)*(\.[a-z0-9](-?[a-z0-9]+)*)*$/i
 * If no valid addresses are recognized, a warning message is sent to a hard-coded address.
 * 
 * [login-key] Configuration of the required login key
 * 
 * In order to be able to read the amount of traffic used, this script must be able to access the
 * DirectAdmin API via a login key. This section of the environment file should provide an account,
 * where the section must have two lines, containing a key=value-pair for username and password.
 * 
 * In DirectAdmin, a login key should be created with at least the following commands allowed:
 * CMD_API_SHOW_USER_USAGE
 * CMD_API_SHOW_USER_CONFIG
 * CMD_API_BANDWIDTH_BREAKDOWN
 */

define("CONFIG_FILE", __DIR__ . "/../config/usage-mail.env");
define("COMMANDS", [
	'CMD_API_SHOW_USER_USAGE',
	'CMD_API_SHOW_USER_CONFIG',
	'CMD_API_BANDWIDTH_BREAKDOWN',
]);

header('Content-Type: text/plain', true);

// if no secret token is given at all, just stop
// check for the right token later (after parsing the configuration file)
if ((!isset($_REQUEST) || !isset($_REQUEST['secret_token'])) && $argc < 2) {
	exit;
}

function exitWithErrorMail(string $message) {
	mail(
		'it@eicas.nl',
		'error usage-mail',
		$message
	);
	exit;
}

/**
 * Given a number of bytes, output the size with an appropriate prefix, e.g. KB, MB, etc.
 * 
 * The prefix is chosen such, that the resulting number is between 1 and 1024, except for very high
 * values (T is the highest prefix). If no prefix is needed, the number is output suffixed with
 * 'B ', where the extra space is added for allignment. Otherwise, the number is represented with
 * one decimal, followed by the prefix and the letter 'B'.
 * 
 * @param int $size
 * @return string
 */
function formatSize(int $size): string {
	$prefixes = ['', 'K', 'M', 'G', 'T'];
	$prefix = reset($prefixes);
	while ($size > 1024 && $prefix = next($prefixes)) {
		$size = $size / 1024;
	}
	if ($prefix === '') {
		return $size . "B "; // space for alignment
	}
	return sprintf("%.1f%sB", $size, $prefix);
}

// step 1: read the environment file
if (!is_readable(CONFIG_FILE)) {
	exitWithErrorMail("config file '" . CONFIG_FILE . "' is not readable");
}
$environment_file = file(CONFIG_FILE);
$environment = [];
$section = null;
foreach ($environment_file as $line) {
	$line = trim($line);
	if (!$line || in_array($line[0], ['#', ';'])) {
		continue;
	}
	if (preg_match('/^\[(.*)]$/i', $line, $matches)) {
		$section = $matches[1];
		continue;
	}
	switch ($section) {
		case 'secret_token':
			$environment[$section] = $line;
			break;
		case 'email':
			if (preg_match(
				'/^[a-z0-9]([_+.-]?[a-z0-9]+)*@[a-z0-9](-?[a-z0-9]+)*(\.[a-z0-9](-?[a-z0-9]+)*)*$/i',
				$line
			)) {
				$environment[$section][] = $line;
			}
			break;
		case 'login-key':
			$pair = explode("=", $line, 2);
			switch (strtolower(trim($pair[0]))) {
				case 'username':
					$environment[$section]['username'] = $pair[1] ?? '';
					break;
				case 'password':
					$environment[$section]['password'] = $pair[1] ?? '';
					break;
			}
	}
}

// step 2: scrutinize the environment
foreach (['secret_token', 'email', 'login-key'] as $key) {
	if (!array_key_exists($key, $environment)) {
		exitWithErrorMail("no $key setup");
	}
}
foreach (['username', 'password'] as $key) {
	if (!array_key_exists($key, $environment['login-key'])) {
		exitWithErrorMail("no login-key $key setup");
	}
}
if (count($environment['email']) === 0) {
	exitWithErrorMail("no email addresses setup");
}

// step 3: check the secret token
$secret_token = $_REQUEST['secret_token'] ?? false;
if (!$secret_token && $argc > 1) {
	for ($i = 1; $i < $argc; $i++) {
		$param = explode("=", $argv[$i], 2);
		if (count($param) > 1 && $param[0] === 'secret_token') {
			$secret_token = $param[1];
			break;
		}
	}
}
if ($secret_token !== $environment['secret_token']) {
	// simply exit with no further action
	exit;
}

// step 4: get the information using the DirectAdmin API
$statistics = [];
$ch = curl_init();
$url = "https://{$environment['login-key']['username']}:{$environment['login-key']['password']}@web0126.zxcs.nl:2222";
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
curl_setopt($ch, CURLOPT_TIMEOUT, 20);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

foreach (COMMANDS as $command) {
	curl_setopt($ch, CURLOPT_URL, "$url/$command");
	$answers = urldecode(curl_exec($ch));

	if ($command === 'CMD_API_BANDWIDTH_BREAKDOWN') {
		$day = null;
		foreach (explode("&", $answers) as $answer) {
			$pair = explode("=", $answer, 2);
			$k = $pair[0];
			$v = $pair[1] ?? null;
			if (strpos($v, "=") !== false) {
				$day = $k;
				$pair = explode("=", $v, 2);
				$k = $pair[0];
				$v = $pair[1] ?? null;
			}
			$statistics[$command][$day][$k] = $v;
		}
	} else {
		foreach (explode("&", $answers) as $answer) {
			$pair = explode("=", $answer, 2);
			$statistics[$command][$pair[0]] = $pair[1] ?? null;
		}
	}
}
curl_close($ch);

// step 5: parse the statistics into a (txt) message
$message = [];
$message[] = "update van bandbreedte-gebruik EICAS\n";
$message[] = "+" . str_repeat("-", 19) . "+" . str_repeat("-", 36) . "+";

$day = intval(date('j'), 10);
$days = intval(date('t'), 10);
$percent = 100 * $day / $days;
$message[] = sprintf(
	"| %-17s | %5.1f%%  %-26s |",
	"dag",
	$percent,
	sprintf("%d / %d", $day, $days)
);

$usage = floatval($statistics['CMD_API_SHOW_USER_USAGE']['bandwidth']);
$config = floatval($statistics['CMD_API_SHOW_USER_CONFIG']['bandwidth']);
$percent = 100 * $usage / $config;
$message[] = sprintf(
	"| %-17s | %5.1f%%  %-26s |",
	"bandbreedte (MB)",
	$percent,
	sprintf("%.1f / %.1f", $usage, $config)
);
$message[] = sprintf(
	"| %-17s |         %-26s |",
	"            (GB)",
	sprintf("%.1f / %.1f", $usage / 1024, $config / 1024)
);
$message[] = sprintf(
	"| %-17s |         %-26s |",
	"extrapolatie",
	""
);
$message[] = sprintf(
	"| %-17s |         %-26s |",
	" einde maand (GB)",
	sprintf("%.1f", ($usage / 1024) * $days / $day)
);

$usage = floatval($statistics['CMD_API_SHOW_USER_USAGE']['quota']);
$config = floatval($statistics['CMD_API_SHOW_USER_CONFIG']['quota']);
$percent = 100 * $usage / $config;
$message[] = sprintf(
	"| %-17s | %5.1f%%  %-26s |",
	"schijfruimte (MB)",
	$percent,
	sprintf("%.1f / %.1f", $usage, $config)
);

$message[] = "+" . str_repeat("-", 19) . "+" . str_repeat("-", 36) . "+\n";

$message[] = "\nBandbreedte per dag\n";

$message[] = "+" . str_repeat("-", 12) . "+" . str_repeat(str_repeat("-", 10) . "+", 4);
$message[] = sprintf(
	"| %-10s | %-8s | %-8s | %-8s | %-8s |",
	"dag",
	"http",
	"email",
	"overig",
	"totaal",
);
$message[] = "+" . str_repeat("-", 12) . "+" . str_repeat(str_repeat("-", 10) . "+", 4);
foreach ($statistics['CMD_API_BANDWIDTH_BREAKDOWN'] as $day => $usage) {
	if ($day === 'total') {
		$message[] = "+" . str_repeat("-", 12) . "+" . str_repeat(str_repeat("-", 10) . "+", 4);
		$day = "totaal";
	}
	$other = 0;
	foreach ($usage as $k => $v) {
		if (!in_array($k, ['http', 'email', 'total', 'simpletotal'])) {
			if (strpos($k, '_') === false) {
				$other += floatval($v);
			}
		}
	}
	$message[] = sprintf(
		"| %10s | %8s | %8s | %8s | %8s |",
		$day,
		formatSize($usage['http']),
		formatSize($usage['email']),
		formatSize($other),
		formatSize($usage['total']),
	);
}
$message[] = "+" . str_repeat("-", 12) . "+" . str_repeat(str_repeat("-", 10) . "+", 4);

// step 6: send the message
mail(
	implode(",", $environment['email']),
	'EICAS resource-usage report',
	implode("\n", $message)
);
