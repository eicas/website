<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('images', function (Blueprint $table) {
			$table->id();
			$table->string('original_filename');
			$table->string('filename');
			$table->string('alt');
			$table->text('heights');
			$table->string('title')->nullable();
			$table->string('artist')->nullable();
			$table->unsignedSmallInteger('year')->nullable();
			$table->text('info')->nullable();
			$table->foreignId('image_id')->nullable()->constrained();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('images');
	}
}
