<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImageImagesetTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('image_imageset', function (Blueprint $table) {
			$table->id();
			$table->foreignId('image_id')->constrained();
			$table->foreignId('imageset_id')->constrained();
			$table->unsignedInteger('order')->default(0);
			$table->string('href')->nullable();
			$table->string('alt')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('image_imageset');
	}
}
