<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuitemsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('menuitems', function (Blueprint $table) {
			$table->id();
			$table->foreignId('menuitem_id')->nullable()->constrained();
			$table->unsignedInteger('order');
			$table->string('label');
			$table->string('href')->nullable();
			$table->timestamps();

			$table->unique(['menuitem_id', 'order']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('menuitems');
	}
}
