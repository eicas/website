<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewFriendsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('new_friends', function (Blueprint $table) {
			$table->id();
			$table->string('name');
			$table->string('email');
			$table->string('telephone')->nullable();
			$table->string('address')->nullable();
			$table->string('postalcode')->nullable();
			$table->string('city')->nullable();
			$table->string('country')->nullable();
			$table->decimal('amount', 10, 2);
			$table->boolean('payment_test')->default(0); // whether payment was done with 'test' api key
			$table->string('payment_method');
			$table->string('payment_reference');
			$table->string('payment_id');
			$table->timestamp('paid_at')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('new_friends');
	}
}
