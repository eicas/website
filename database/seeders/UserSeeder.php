<?php

// SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>
//
// SPDX-License-Identifier: MIT

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		if (!User::whereHas('roles', function ($q) {
				$q->where('key','=','admin');
		})->get()->count()) {
			$adminUser = User::create([
				'name' => 'admin',
				'email' => 'admin@example.com',
				'password' => Hash::make('hello'),
				'email_verified_at' => Date::now(),
			]);
			$adminRole = Role::where('key', '=', 'admin')->get();
			$adminUser->roles()->sync($adminRole);
		}
	}
}
