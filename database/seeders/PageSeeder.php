<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace Database\Seeders;

use App\Models\Page;
use Illuminate\Database\Seeder;

class PageSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		if (!Page::where([
			['language', '=', 'nl'],
			['slug', '=', '/'],
		])->count()) {
			Page::create([
				'language' => 'nl',
				'slug' => '/',
				'title' => 'Home',
				'template' => 'home',
				'visibility' => 2,
			]);
		}
	}
}
