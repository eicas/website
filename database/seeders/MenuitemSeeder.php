<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace Database\Seeders;

use App\Models\Menuitem;
use Illuminate\Database\Seeder;

class MenuitemSeeder extends Seeder {
	const MENU = [
		'EICAS' => [
			'Het Hegius' => '#',
			'Organisatie' => [
				'Bestuur & medewerkers' => '#',
				'ANBI' => '#',
			],
			'Contact' => '#',
			'Vacatures' => '#',
		],
		'Museum EICAS preview' => [
			'Nu te zien' => '#',
			'Vaste collectie' => '#',
			'Bezoek' => [
				'Openingstijden' => '#',
				'Prijzen' => '#',
				'Adres' => '#',
				'Reserveren' => '#',
			],
		],
		'Tickets' => '#',
		'Steun EICAS' => [
			'Word vriend' => '#',
			'Connected Partners' => '#',
		],
		'Archief' => [
			'Nieuwsbrief' => '#',
			'ZERO' => '#',
		],
	];

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$order = 0;
		foreach (self::MENU as $label => $item) {
			$this->addMenuitem($label, $item, null, ++$order);
		}
	}

	protected function addMenuitem($label, $item, $parent_id, $order) {
		if (is_array($item)) {
			logger("added submenu '$label' in parent " . ($parent_id ?? 'root'));
			$menuitem = Menuitem::create([
				'menuitem_id' => $parent_id,
				'order' => $order,
				'label' => $label,
				'href' => null,
			]);
			logger($menuitem);

			$order = 0;
			foreach ($item as $sublabel => $subitem) {
				$this->addMenuitem($sublabel, $subitem, $menuitem->id, ++$order);
			}
		} else {
			Menuitem::create([
				'menuitem_id' => $parent_id,
				'order' => $order,
				'label' => $label,
				'href' => $item,
			]);
		}
	}
}
