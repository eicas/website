<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
// SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>
//
// SPDX-License-Identifier: MIT

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run() {
		$this->call([
			RoleSeeder::class,
			UserSeeder::class,
			PageSeeder::class,
			// do not add the MenuitemSeeder as it inserts blindly
		]);
	}
}
