<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		app()->setLocale(config('app.fallback_locale'));
		foreach (array_keys(__('role')) as $key) {
			Role::updateOrCreate(['key' => $key]);
		}
	}
}
