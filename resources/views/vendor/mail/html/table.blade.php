{{--
SPDX-FileCopyrightText: 2022 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

<div class="table">
{{ Illuminate\Mail\Markdown::parse($slot) }}
</div>
