{{--
SPDX-FileCopyrightText: 2022 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

{{ $slot }}
