{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.guest')

@section('body-class')class="no-template-layout"@endsection

@section('main')
@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif
<h2>Gelieve uw wachtwoord nogmaals in te voeren.</h2>
<p>
	De actie die u wilt uitvoeren is extra gevoelig en vereist daarom een extra controle.
</p>
<form method="POST" action="{{ route('profile.password.confirm.store')}}">
	@csrf
	<div>
		<label for="password">Wachtwoord</label>
		<input type="password" id="password" name="password" value="" required>
	</div>
	<div>
		<label for="submit"></label>
		<button type="submit" id="submit">Bevestigen</button>
	</div>
</form>
@endsection
