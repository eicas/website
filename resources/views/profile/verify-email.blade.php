{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.user')

@section('main')
@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif
<h2>Controle E-mailadres</h2>
<p>
	Om gebruik te maken van de functionaliteiten in dit account, dient u eerst uw e-mailadres te bevestigen.
	Er is een bericht verzonden naar het door u opgegeven e-mailadres met daarin een bevestigingslink.
	Door op de bevestigingslink te klikken, weten wij dat uw e-mailadres geldig is.
</p>
<p>
	Heeft u geen bericht ontvangen?
	<form method="POST" action="{{ route('profile.verification.send') }}">
		@csrf
		<button type="submit">Klik dan hier om een nieuw bericht te verzenden</button>
	</form>
</p>
@endsection
