{{--
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>
SPDX-FileCopyrightText: 2022 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.user')

@section('main')
<form method="POST" action="{{ route('profile.password.reset.update')}}">
	@csrf
	<input type="hidden" name="token" value="{{ $token }}">
	<h2>Wachtwoord opnieuw instellen</h2>
	<div>
		<label for="email">Emailadres</label>
		<input type="text" id="email" name="email" value="{{ old('email') }}" required autofocus>
	</div>
	<div>
		<label for="password">Wachtwoord</label>
		<input type="password" id="password" name="password" value="" required>
	</div>
	<div>
		<label for="password_confirmation">Bevestig wachtwoord</label>
		<input type="password" id="password_confirmation" name="password_confirmation" value="" required>
	</div>
	<div>
		<label for="submit"></label>
		<button type="submit" id="submit">Wachtwoord instellen</button>
	</div>
</form>
@endsection
