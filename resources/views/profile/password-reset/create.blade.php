{{--
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>
SPDX-FileCopyrightText: 2022 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.user')

@section('main')
<form method="POST" action="{{ route('profile.password.reset.store')}}">
	@csrf
	<h2>Wachtwoord opnieuw instellen</h2>
	<p>
		Voer uw emailadres in. Wij zenden u dan een eenmalige link om het wachtwoord opnieuw in te stellen.
	</p>
	<div>
		<label for="email">Emailadres</label>
		<input type="text" id="email" name="email" value="{{ old('email') }}" required autofocus>
	</div>
	<div>
		<label for="submit"></label>
		<button type="submit" id="submit">Link wachtwoordherstel aanvragen</button>
	</div>
</form>
@endsection
