{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.user')

@section('main')
<h2>Profiel: {{ $user->name }}</h2>
<table>
	<tbody>
		<tr>
			<th>Naam</th>
			<td>{{ $user->name }}</td>
		</tr>
		<tr>
			<th>E-mail (en inlognaam)</th>
			<td>{{ $user->email }}</td>
		</tr>
		<tr>
			<th>Registratie</th>
			<td>{{ $user->created_at }}</td>
		</tr>
		@if ($user->hasAnyRole(['admin', 'user-admin', 'text-admin', 'art-admin', 'menu-admin']))
		<tr>
			<th>Laatste wijziging</th>
			<td>{{ $user->updated_at }}</td>
		</tr>
		@endif
		@if (count($user->roles))
		<tr>
			<th>U bent geregistreerd als:</th>
			<td>
				<ul class="roles">
					@foreach ($user->roles as $role)
					<li>{{ $role->translatedName }}</li>
					@endforeach
				</ul>
			</td>
		</tr>
		@endif
	</tbody>
</table>

{{-- <h2>MultiFactor Authoriation (MFA)</h2> --}}

<h2>Wachtwoord wijzigen</h2>
<form action="{{ route('profile.password.update') }}" method="post">
	@csrf
	@method('PUT')
	<label for="pw-1">Huidige wachtwoord</label>
	<input id="pw-1" type="password" name="current_password" required class="small-20em">

	<label for="pw-2">Nieuw wachtwoord</label>
	<input id="pw-2" type="password" name="password" required class="small-20em">

	<label for="pw-3">Herhaal wachtwoord</label>
	<input id="pw-3" type="password" name="password_confirmation" required class="small-20em">

	<button type="submit">wachtwoord wijzigen</button>
</form>

@if (count($user->userSessions) > 0)
<h2>Inlogsessies</h2>
<table>
	<thead>
		<tr>
			<th>Start</th>
			<th>Duur</th>
			<th>Activiteit</th>
			<th>ip-adres</th>
			<th>Browser</th>
			<th>Status</th>
			<th>Permanent</th>
			<th>Acties</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($user->userSessions as $userSession)
		<tr{!! $userSession->id === $user->userSession->id ? ' style="font-style:italic"' : '' !!}>
			<td>{{ $userSession->created_at }}</td>
			<td>{{ \App\Helpers\Misc::timeFromSeconds($userSession->updated_at->diffInSeconds($userSession->created_at)) }}</td>
			<td>{{ $userSession->number_of_visits }}</td>
			<td>{{ $userSession->ip }}</td>
			<td title="{{ $userSession->user_agent }}">{{ \App\Helpers\Misc::shortUserAgent($userSession->user_agent) }}</td>
			<td>{{ $userSession->expired ? "verlopen (" . \App\Helpers\Misc::timeFromSeconds($userSession->expired) . ")" : 'actief' }}</td>
			<td>{{ $userSession->canRemember ? 'ja' : 'nee' }}</td>
			<td>
				@if ($userSession->id === $user->userSession->id)
				huidige sessie
				@elseif ($userSession->canRemember)
				<form class="no-layout" id="form_{{ $userSession->id }}" method="POST" action="{{
					route('profile.session.delete', ['user' => $user, 'userSession' => $userSession])
				}}">
					@csrf
					@method('DELETE')
					<button type="submit">uitloggen</button>
				</form>
				@endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
@endif

<h2>Profiel verwijderen</h2>
<p>
	Hieronder kunt u uw profiel verwijderen.
	Deze actie is definitief en u kunt dan niet meer inloggen met dit account.
	Om uw profiel te verwijderen, moet u nogmaals uw wachtwoord ingeven.
</p>
@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif
<form id="delete_profile" method="POST" action="{{ 
	route('profile.delete')
}}" onsubmit="return confirm('Zeker weten? Uw profiel wordt definitief verwijderd!');">
	@csrf
	@method('DELETE')
	<div>
		<label for="password">Wachtwoord</label>
		<input type="password" id="password" name="password" required>
	</div>
	<div>
		<label for="submit"></label>
		<button type="submit" id="submit">Profiel verwijderen</button>
	</div>
</form>
@endsection
