{{--
SPDX-FileCopyrightText: 2021 - 2022 Marten Koetsier <eicas@koetsierengineering.nl>
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.guest')

@section('body-class')class="no-template-layout"@endsection

@section('main')
@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif
<form method="POST" action="{{ route('profile.login')}}">
	@csrf
	<div>
		<label for="email">Emailadres</label>
		<input type="text" id="email" name="email" value="{{ old('email') }}" required>
	</div>
	<div>
		<label for="password">Wachtwoord</label>
		<input type="password" id="password" name="password" value="" required>
	</div>
	<div>
		<label for="remember_me"></label>
		<label><input type="checkbox" id="remember_me" name="remember_me" value="1">Ingelogd blijven</label>
	</div>
	<div>
		<label for="submit"></label>
		<button type="submit" id="submit">Inloggen</button>
	</div>
</form>
<p>
	Als u uw wachtwoord opnieuw wilt instellen, klik dan <a href={{
		route('profile.password.reset.create')
	}}>hier</a>.
</p>
@endsection
