{{--
SPDX-FileCopyrightText: 2021 - 2022 Marten Koetsier <eicas@koetsierengineering.nl>
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.user')

@section('main')
@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif
<form method="POST" action="{{ route('profile.register.store')}}">
	@csrf
	<div>
		<label for="name">Naam</label>
		<input type="text" id="name" name="name" value="{{ old('name') }}" required focus>
	</div>
	<div>
		<label for="email">Emailadres</label>
		<input type="text" id="email" name="email" value="{{ old('email') }}" required>
	</div>
	<div>
		<label for="password">Wachtwoord</label>
		<input type="password" id="password" name="password" value="" required>
	</div>
	<div>
		<label for="password_confirmation">Bevestig wachtwoord</label>
		<input type="password" id="password_confirmation" name="password_confirmation" value="" required>
	</div>
	<div>
		<label for="submit"></label>
		<button type="submit" id="submit">Aanmelden</button>
	</div>
</form>
@endsection
