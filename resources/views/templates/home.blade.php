{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.guest')

@section('body-class')class="home-layout"@endsection

@section('main')
@section('main-menu')
<x-menu />
@show
<main id="gallery-home">
	<div>
		<div>
			@if (count($images))
			@foreach (['left', 'right'] as $column)
			<div id="zero-{{ $column }}">
				@foreach ($images as $image)
				@php
					$imageimagesets_list = "imageimagesets_$column";
				@endphp
				@continue(!in_array($image->pivot->id, $$imageimagesets_list))
				@php
					$options = [];
					$options['embed'] = $loop->index < 6;
					if ($image->pivot->href && $image->pivot->alt) {
						if (str_starts_with($image->pivot->href, '/') || 
							str_starts_with($image->pivot->href, 'https://www.eicas.nl') || 
							str_starts_with($image->pivot->href, 'mailto')) {
							// Open link in same browser tab
							$options['image-before'] = "<div><a href=\"{$image->pivot->href}\"><div>{$image->pivot->alt}</div></a></div>";
						} else if (str_starts_with(strtolower($image->pivot->href), 'https://eicas.nl')) {
							// Specific case which changes 'https://eicas.nl' to 'https://www.eicas.nl'
							$pos = strpos(strtolower($image->pivot->href), 'eicas.nl');
							// The last argument, the 0, in substr_replace() means the string 'www.' is inserted instead 
							// of replacing other characters
    						$updated_url = substr_replace($image->pivot->href, 'www.', $pos, 0);
							// Open link in same browser tab
							$options['image-before'] = "<div><a href=\"{$updated_url}\"><div>{$image->pivot->alt}</div></a></div>";				
						} else {
							// External link to be opened in new browser tab
							$options['image-before'] = "<div><a href=\"{$image->pivot->href}\" target=\"_blank\"><div>{$image->pivot->alt}</div></a></div>";			
						}
					} else {
						$title = $image->pivot->alt ?? implode(", ", array_filter([$image->title, $image->artist, $image->year]));
						$title = $title ? "<div>$title</div>" : "";
						$options['image-before'] = "<div><a href=\"javascript:;\">$title</a></div>";
						$options['image-popup-art'] = true;
						$options['image-popup-art-parent'] = [
							'tagName' => 'div',
							'className' => 'image-preview-parent',
						];
					}
				@endphp
				<div class="image-preview-parent">
					<x-image :image="$image" :options="$options"/>
				</div>
				@endforeach
			</div>
			@endforeach
			@endif
		</div>
	</div>
</main>
@if (count($images))
@include('includes.popup-art')
<section id="gallery-home-small">
	@foreach (['top', 'bottom'] as $row)
	<div class="strip strip-{{ $row }}">
		@foreach ($images as $image)
		@php
			$imageimagesets_list = "imageimagesets_$row";
		@endphp
		@continue(!in_array($image->pivot->id, $$imageimagesets_list))
		@php
			$options = [];
			$options['embed'] = $loop->index < 4;
			if ($image->pivot->href && $image->pivot->alt) {
				$options['image-before'] = "<div><a href=\"{$image->pivot->href}\"><div>{$image->pivot->alt}</div></a></div>";
			} else {
				$options['image-popup-art'] = true;
			}
		@endphp
		<div>
			<x-image :image="$image" :options="$options"/>
		</div>
		@endforeach
	</div>
	@endforeach
</section>
@endif
<x-footer show-user="1"/>
@endsection
