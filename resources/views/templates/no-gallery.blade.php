{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.guest')

@section('body-class')class="no-gallery-layout"@endsection

@section('main')
@section('main-menu')
<x-menu />
@show
<div id="no-gallery-story">
	<main>
		<article>
			<header>
				<h1>{!! $page->title !!}</h1>
			</header>
			@php
				$options = [
					'image-before' => '<div class="content-image">',
					'image-after' => '</div>',
				];
			@endphp
			@foreach ($page->contents as $content)
			<section class="{{ $content->type }}">
				<x-content :content="$content" :options="$options"/>
			</section>
			@endforeach

		</article>
	</main>
	<div id="footers">
		<x-footer />
	</div>
</div>
@endsection
