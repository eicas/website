{{--
SPDX-FileCopyrightText: 2021 - 2022 Marten Koetsier <eicas@koetsierengineering.nl>
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.user')

@section('main')
	<h2>Downloads</h2>
	<form class="no-layout space" id="create" method="POST" action="{{
		route('cms.downloads.store')
	}}" enctype="multipart/form-data">
		@csrf
		<input type="hidden" name="MAX_FILE_SIZE" value="33554432">
		<input type="file" name="file">
		<button type="submit">Opslaan</button> (max. 32MB)
		<p style="margin-bottom:1em">
			Toegestane bestandsformaten: {{ implode(", ", Arr::flatten(\App\Models\Download::MIMETYPES)) }}
		</p>
	</form>

	<h3>Gebruik</h3>
	<p style="margin-bottom:1em">
		Voor het aanbieden van een download-link, gebruik de tekst: <code>[DOWNLOAD:&lt;ID&gt;]</code>.
		Dus:
			blokhaak openen,
			DOWNLOAD (hoofdletters),
			dubbele punt,
			hieronder vermelde ID,
			en tenslotte blokhaak sluiten.
		Deze code wordt automatisch vervangen door de juiste link naar een download.
	</p>

	<table>
		<thead>
			<tr>
				<th>ID</th>
				<th>Bestandsnaam</th>
				<th>Grootte</th>
				<th>Upload</th>
				<th>Laatst gewijzigd</th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@php
				$updated_id = session()->get('updated_id', 0);
			@endphp
			@foreach ($downloads as $download)
			@php
				$id = $download->id;
			@endphp
			<tr{!! $id === $updated_id ? ' class="updated"' : '' !!}>
				<td>{{ $id}}</td>
				<td>
					<form class="no-layout" id="edit-{{ $id }}" method="POST" action="{{
						route('cms.downloads.update', ['download' => $download])
					}}">
						@csrf
						@method('PATCH')
						<input type="text" name="filename" value="{{ $download->filename
						}}" required style="width:20em">
						<button type="submit">🖫</button>
					</form>
				</td>
				<td>{{ $download->formatted_size }}</td>
				<td>{{ $download->created_at->format('Y-m-d H:i:s') }}</td>
				<td>{{ $download->updated_at->format('Y-m-d H:i:s') }}</td>
				<td>
					<a class="button" href="{{ route('cms.downloads.show', ['download' => $download])
						}}" title="Download">⭳</a>
				</td>
				<td>
					<form class="no-layout" id="delete-{{ $id }}" method="POST" action="{{
						route('cms.downloads.destroy', ['download' => $download])
					}}" onsubmit="return confirm('Zeker weten? Actie is definitief!');">
						@csrf
						@method('DELETE')
						<button type="submit" title="Verwijderen">🗙</button>
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@endsection
