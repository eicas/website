{{--
SPDX-FileCopyrightText: 2021 - 2022 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.user')

@section('main')
	<h2>Dashboard</h2>

	<nav>
		@if (Auth::user()->can('admin', \App\Models\Page::class))
		<a href="{{ route('cms.pages.index') }}">
			<h3>Pagina's</h3>
			<p>
				Beheer de {{ $pages_count }} pagina's
			</p>
		</a>
		@endif
		@if (Auth::user()->can('admin', \App\Models\Menuitem::class))
		<a href="{{ route('cms.menuitems.index') }}">
			<h3>Menu</h3>
			<p>
				Beheer het Menu
			</p>
		</a>
		@endif
		@if (Auth::user()->can('admin', \App\Models\Image::class))
		<a href="{{ route('cms.images.index') }}">
			<h3>Beelden</h3>
			<p>
				Beheer de {{ $images_count }} beelden
			</p>
		</a>
		@endif
		@if (Auth::user()->can('admin', \App\Models\Imageset::class))
		<a href="{{ route('cms.imagesets.index') }}">
			<h3>Beeldlijsten</h3>
			<p>
				Beheer de {{ $imagesets_count }} beeld-lijsten
			</p>
		</a>
		@endif
		@if (Auth::user()->can('admin', \App\Models\Download::class))
		<a href="{{ route('cms.downloads.index') }}">
			<h3>Downloads</h3>
			<p>
				Beheer de {{ $downloads_count }} downloads
			</p>
		</a>
		@endif
		@if (Auth::user()->can('admin', \App\Models\Redirect::class))
		<a href="{{ route('cms.redirects.index') }}">
			<h3>Redirects</h3>
			<p>
				Beheer de {{ $redirects_count }} redirects
			</p>
		</a>
		@endif
		@if (Auth::user()->hasRole('admin'))
		<a href="{{ route('cms.datalimit') }}">
			<h3>Datalimit</h3>
			<p>
				Bekijk de hoeveelheid uitgaande data
			</p>
		</a>
		@endif
	</nav>
@endsection
