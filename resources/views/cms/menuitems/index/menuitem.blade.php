{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

@if($subitem)
<form method="POST" id="update_{{ $subitem->id }}" class="update level-{{ $level }}" action="{{
	route('cms.menuitems.update', ['menuitem' => $subitem]) }}">
	@csrf
	@method('PATCH')
	<input type="text" name="label" value="{!! $subitem->label !!}" required maxlength="32">
	<input type="{{
		$subitems && count($subitems) ? 'hidden' : 'text'
	}}" name="href" value="{!! $subitem->href !!}" maxlength="255">
	<button type="submit" class="btn-save">opslaan</button>
</form>
@endif

@if ($subitem && isset($previous_subitem))
<form method="POST" id="swap_{{ $subitem->id }}" class="swap" action="{{
	route('cms.menuitems.swap', ['menuitem1' => $subitem, 'menuitem2' => $previous_subitem]) }}">
	@csrf
	<button type="submit" class="btn-up">omhoog</button>
</form>
@endif

@if (!$subitems || count($subitems) === 0)
<form method="POST" id="delete_{{ $subitem->id }}" class="delete" action="{{
	route('cms.menuitems.destroy', ['menuitem' => $subitem]) }}">
	@csrf
	@method('DELETE')
	<button type="submit" class="btn-delete">verwijderen</button>
</form>
@endif

@php
$previous_menuitem = null;
foreach ($subitems as $menuitem) {
	$included = [
		'subitem' => $menuitem,
		'subitems' => null,
		'level' => $level + 1
	];
	if ($menuitem->menuitems) {
		$included['subitems'] = $menuitem->menuitems;
	}
	if ($previous_menuitem) {
		$included['previous_subitem'] = $previous_menuitem;
	}
	@endphp
	@include('cms.menuitems.index.menuitem', $included)
	@php
	$previous_subitem = $menuitem;
}
@endphp

@if ($level < \App\Models\Menuitem::DEEPEST_LEVEL)
<form method="POST" id="create_{{ $subitem->id ?? 'root' }}" class="create level-{{ $level + 1 }}" action="{{
	route('cms.menuitems.store') }}">
	@csrf
	<input type="hidden" name="menuitem_id" value="{!! $subitem->id ?? '' !!}">
	<input type="text" name="label" value="" required maxlength="32">
	<input type="text" name="href" value="" maxlength="255">
	<button type="submit" class="btn-save">nieuw</button>
</form>
@endif
