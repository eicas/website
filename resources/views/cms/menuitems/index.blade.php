{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.user')

@section('main')
<h2>Menu</h2>

<section id="menu-forms">
@include('cms.menuitems.index.menuitem', ['subitem' => null, 'subitems' => $menu, 'level' => -1])
</section>
@endsection
