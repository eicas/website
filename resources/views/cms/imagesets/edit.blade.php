{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.user')

@section('main')
	<h2>Beeld-lijst '{{ $imageset->name }}' wijzigen</h2>
	<section id="images">
		@php
			$previous_pivot_id = 0;
		@endphp
		@foreach ($imageset->images as $image)
			<div>
				<img src="{{
					Storage::disk('images')->has($image->getPath($image->getSmallestWidth()))
						? Image::make(
							Storage::disk('images')->path($image->getPath($image->getSmallestWidth()))
						)->stream('data-url')
						: ''
				}}" alt={{ $image->alt }}>

				@if (!$loop->first)
				<form class="no-layout" id="shift-{{ $image->id }}" method="POST" action="{{
					route('cms.imagesets.imageimagesets.swap', [
						'imageset' => $imageset, 'imageimageset1' => $image->pivot->id, 'imageimageset2' => $previous_pivot_id
					])
				}}">
					@csrf
					<button type=submit class="shift">⬅</button>
				</form>
				@endif
				<form class="no-layout" id="delete-{{ $image->id }}" method="POST" action="{{
					route('cms.imagesets.imageimagesets.destroy', ['imageset' => $imageset, 'imageimageset' => $image->pivot])
				}}">
					@csrf
					@method('DELETE')
					<button type=submit class="delete">⨯</button>
				</form>
				@if ($image->pivot->href)
				<div class="link">⫘</div>
				@endif
				<a class="edit" href="{{ route('cms.imagesets.imageimagesets.edit', ['imageset' => $imageset, 'imageimageset' => $image->pivot]) }}">🖉</a>
			</div>
		@php
			$previous_pivot_id = $image->pivot->id;
		@endphp
		@endforeach
	</section>
	<hr>
	<form class="" id="add-images" method="POST" action="{{ route('cms.imagesets.imageimagesets.store', ['imageset' => $imageset]) }}">
		@csrf
		<input type="hidden" name="redirect_url" value="{{ route('cms.imagesets.edit', ['imageset' => $imageset]) }}">
		<label>Voeg afbeeldingen toe</label>
		<x-multi-image-picker name="image_ids" value="" :id="Str::uuid()" />
		<button type="submit">toevoegen</button>
	</form>

	{{-- <p><a href="{{ route('cms.imagesets.imageimagesets.index', ['imageset' => $imageset])}}">Beelden</a></p> --}}
@endsection
