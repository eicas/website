{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.user')

@section('main')
	<h2>Beeld-lijsten</h2>

	<table>
		<thead>
			<tr>
				<th>Naam</th>
				<th>#</th>
				<th>Beelden</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			@php
				$updated_id = session()->get('updated_id', 0);
			@endphp
			@foreach ($imagesets as $imageset)
			<tr{!! $imageset->id === $updated_id ? ' class="updated"' : '' !!}>
				<td>
					<form id="edit-{{ $imageset->id }}" method="POST" action="{{
							route('cms.imagesets.update', ['imageset' => $imageset])
						}}" class="no-layout">
						@csrf
						@method('PATCH')
						<input type="text" name="name" value="{{
							$imageset->id === $updated_id ? old('name', $imageset->name) : $imageset->name
						}}"{!! $imageset->id === $updated_id ? ' autofocus' : '' !!} required>
						<button type="submit">wijzigen</button>
					</form>
				</td>
				<td>{{ $imageset->images_count }} beelden</td>
				<td>
					<a class="button" href="{{ route('cms.imagesets.edit', ['imageset' => $imageset])}}">Lijst bewerken</a>
					{{-- <a class="button" href="{{ route('cms.imagesets.edit', ['imageset' => $imageset]) }}">Bewerken</a> --}}
				</td>
				<td>
					@if ($imageset->pages_count > 0)
					gebruikt op {{ $imageset->pages_count }} pagina{{ $imageset->pages_count > 1 ? "'s" : "" }}
					@else
					<form class="no-layout" id="delete_{{ $imageset->id }}" method="POST" action="{{
						route('cms.imagesets.destroy', ['imageset' => $imageset])
					}}" onsubmit="return confirm('Zeker weten? Actie is definitief!');">
						@csrf
						@method('DELETE')
						<button type="submit">Lijst verwijderen</button>
					</form>
					@endif
				</td>
			</tr>
			@endforeach
			<tr><th colspan="4">Nieuwe beeld-lijst</th></tr>
			<tr>
				<td>
					<form class="no-layout" id="create" method="POST" action="{{ route('cms.imagesets.store') }}">
						@csrf
						<input form="create" type="text" name="name" value="{{
							$updated_id === 0 ? old('name') : ''
						}}"{!! $updated_id === 0 ? ' autofocus' : '' !!} required>
						<button form="create" type="submit">Opslaan</button>
					</form>
				</td>
				<td colspan="3"></td>
			</tr>
		</tbody>
	</table>

	<h3>Uitleg</h3>
	<p>
		Beeld-lijsten zijn verzamelingen van beelden die op pagina's getoond kunnen worden.
		Elke pagina die van een lijst van beelden voorzien moet worden, verwijst naar een beeldlijst.
		Als een beeld-lijst wordt aangepast, worden alle pagina's die die lijst gebruiken meteen geüpdate.
	</p>
	<p>
		De beelden in de lijst hebben een specifieke volgorde, die aangepast kan worden.
	</p>
	<p>
		Elk beeld in de lijst kan gekoppeld zijn aan een url, maar dat hoeft niet.
	</p>
@endsection
