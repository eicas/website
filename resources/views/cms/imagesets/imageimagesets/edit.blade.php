{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.user')

@section('main')
	<form id="edit" method="POST" action="{{
		route('cms.imagesets.imageimagesets.update', ['imageset' => $imageset, 'imageimageset' => $imageImageset])
	}}">
		@csrf
		@method('PATCH')

		<h2>Actie voor dit beeld</h2>
		<img src="{{
			Storage::disk('images')->has($imageImageset->image->getPath($imageImageset->image->getSmallestWidth()))
				? Image::make(
					Storage::disk('images')->path($imageImageset->image->getPath($imageImageset->image->getSmallestWidth()))
				)->stream('data-url')
				: ''
		}}" alt={{ $imageImageset->image->alt }}>

		<label>Normale weergave</label>
		<span>{!! implode("<br>", array_filter([
				implode(", ", array_filter([
					$imageImageset->image->title,
					$imageImageset->image->artist,
					$imageImageset->image->year,
				])),
				$imageImageset->image->info,
			]))
		!!}</span>
		<span>Laat onderstaande leeg voor bovenstaande weergave</span>

		<label>Link</label>
		<input type="text" name="href" value="{{ old('href', $imageImageset->href) }}">

		<label>Tekst</label>
		<input type="text" name="alt" value="{{ old('alt', $imageImageset->alt) }}">

		<button type="submit">opslaan</button>
	</form>
@endsection
