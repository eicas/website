{{--
SPDX-FileCopyrightText: 2022 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.user')

@section('main')
	<h2>Datalimit</h2>

	<div>
		Instellingen:
		@php
			$labels = [
				'period' => 'periode',
				'max_bytes' => 'max. bytes',
				'suspend_duration' => 'opschortingsduur',
			];
		@endphp
		@foreach ($settings as $key => $value)
			<strong style="display:inline-block;margin-left:1em">{{ $labels[$key] ?? $key }}:</strong>
			{{
				($key === 'max_bytes')
				? sprintf("%.1fMB", ($value / 1048576))
				: \App\Helpers\Misc::timeFromSeconds($value)
			}}
		@endforeach
	</div>
	<p>
		De data-limiet wordt gehandhaafd door elke <strong>periode</strong> te kijken naar het totaal aantal uitgaande
		bytes in die periode.
		Als dit het maximum (<strong>max. bytes</strong>) overstijgt, wordt de site voor de periode
		<strong>opschortingsduur</strong> opgeschort.
		Bezoekers zien dan een zeer korte tekst (in het Nederlands en het Engels) dat ze het later nog eens moeten
		proberen.
		Die tekst gaat vergezeld van een technische foutcode die voor clients aangeeft wat er aan de hand is (http
		status code 429 Too Many Requests).
	</p>

	<h3>Opgeschort</h3>
	<p>De periodes van opschorting in de afgelopen 6 maanden.</p>
	<table style="border-collapse:collapse">
		<thead><tr><th>van</th><th>tot</th><th>opmerking</th></tr></thead>
		<tbody>
			@forelse ($suspends as $suspend)
			<tr>
				<td>{{ $suspend->starts_at }}</td>
				<td>{{ $suspend->ends_at }}</td>
				<td>{{ $suspend->comment }}</td>
			</tr>
			@empty
			<tr>
				<td colspan="3">Geen opschortingen bekend</td>
			</tr>
			@endforelse
		</tbody>
	</table>

@php
	$max_d = $settings['max_bytes'];
	foreach ($visits as $set) {
		$max_d = max(collect($set)->pluck('d')->max(), $max_d);
	}
@endphp
	<h3>Data-gebruik</h3>
	<p>Het data-gebruik per "period" van de afgelopen 2 weken (per dag).</p>
	@foreach($visits as $visits_day)
	<h4>{{ substr($visits_day[0]->p, 0, 10) }}</h4>
	<div class="visits">
		<div class="max-d" style="height:{{ 100 * $settings['max_bytes'] / $max_d }}%"></div>
		<div class="y-axis left-axis"></div>
		<div class="y-axis right-axis"></div>
		<div class="y-tick left-tick" style="height:0"></div>
		<div class="y-tickmark left-tickmark" style="height:0">0</div>
		<div class="y-tick left-tick" style="height:100%"></div>
		<div class="y-tickmark left-tickmark" style="height:100%">{{ sprintf("%.1f", $max_d / 1048576) }}</div>
		<div class="y-tick left-tick" style="height:{{ 100 * $settings['max_bytes'] / $max_d }}%"></div>
		<div class="y-tickmark left-tickmark" style="height:{{ 100 * $settings['max_bytes'] / $max_d }}%">{{
			sprintf("%.1f", $settings['max_bytes'] / 1048576)
		}}</div>
		<div class="x-axis"></div>
		@for ($x = 4; $x <= 100; $x += 4)
		<div class="x-tick" style="grid-column-start:{{ $x }}">{{ $x < 100 ? sprintf("%02d", $x / 4 - 1) : '' }}</div>
		@endfor

		@foreach($visits_day as $visit)
		@php
		$pos = 3 + (int) floor((60 * intval(substr($visit->p, 11, 2), 10) + intval(substr($visit->p, 14, 2), 10)) / 15);
		@endphp
		<div style="height:{{
			100 * $visit->d / $max_d
		}}%{{
			$visit->d >= $settings['max_bytes'] ? ';background-color:rgba(255, 127, 127, 0.5)' : ''
		}};grid-column-start:{{ $pos }}" title="{{
			sprintf("%.2fMB", $visit->d / 1048576)
		}}"></div>
		@endforeach
	</div>
	@endforeach
@endsection
