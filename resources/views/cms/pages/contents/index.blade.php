{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.user')

@section('main')
<h2>Inhoud op pagina {{ $page->title }}</h2>
<p class="more-space">
	<a class="button" href="{{ route('cms.pages.edit', ['page' => $page]) }}">Terug naar de pagina.</a>
</p>
<p>
	Hier beheert u de inhoud van de pagina.
	De inhoud is opgedeeld in blokken van een bepaald "type":
</p>
<table class="key-value">
	<tbody>
		@foreach (App\Models\Content::TYPES as $type)
		<tr>
			<th>{!! __("content.$type.label") !!}</th>
			<td>{!! __("content.$type.description") !!}</td>
		</tr>
		@endforeach
	</tbody>
</table>
<p>
	Elk blok dient apart opgeslagen te worden.
</p>
<p>
	De blokken kunnen in volgorde veranderd worden met de ↟-knoppen.
</p>

<section class="blocks">
	@php
	$previous_content = null;
	@endphp
	@forelse ($page->contents as $content)
	@php
		$show_save_button = true;
	@endphp
	<section>
		@if (!$loop->first)
		<form class="out-of-flow" id="shift-{{ $content->id }}" method="POST" action="{{
			route('cms.pages.contents.swap', [
				'page' => $page, 'content1' => $content, 'content2' => $previous_content
			])
		}}">
			@csrf
		</form>
		@endif
		<form class="out-of-flow" id="delete-{{ $content->id }}" method="POST" action="{{
			route('cms.pages.contents.destroy', ['page' => $page, 'content' => $content])
		}}">
			@csrf
			@method('DELETE')
		</form>
		<form class="" id="update-{{ $content->id }}" method="POST" action="{{
			route('cms.pages.contents.update', ['page' => $page, 'content' => $content])
		}}">
			@csrf
		@method('PATCH')
		@switch($content->type)
		@case('text')
			<label>Tekst</label>
			<textarea name="content" class="editor">{!! $content->content !!}</textarea>
		@break
		@case('image')
			<label>Foto</label>
			<x-image-picker name="content" value="{{ $content->content }}"/>
		@break
		@case('youtube')
			<label>Youtube-ID</label>
			<input type="text" name="content" value="{{ $content->content }}">
		@break
		@case('bandcamp')
			<label>Bandcamp artiest, album en cover-url, gescheiden met komma</label>
			<input type="text" name="content" value="{{ $content->content }}">
		@break
		@case('lvp-tickets')
			<label>LVP-details</label>
			<input type="text" name="content" value="{{ $content->content }}">
			<span>vul in: serverId, contentholderId, en productionId, gescheiden met komma, in deze vaste volgorde.</span>
		@break
		@case('mc-newsletterform')
			<label>MailChimp-details</label>
			<input type="text" name="content" value="{{ $content->content }}">
			<div class="right">
				vul in, gescheiden met komma, in deze vaste volgorde: server, user, id</br>
				https://<strong>server</strong>.list-manage.com/subscribe/post?u=<strong>user</strong>&amp;id=<strong>id</strong>
			</div>
		@break
		@case('form-new-friend')
			<span><em>formulier 'word vriend'</em></span>
			<input type="hidden" name="content" value="">
			@php
				$show_save_button = false;
			@endphp
		@break
		@case('shop-item')
			@php
				$image_id = 0;
				$text = "";
				if (preg_match('/^\[(.*)\](.*)$/s', $content->content, $matches)) {
					$image_id = intval($matches[1], 10);
					$text = $matches[2];
				}
			@endphp
			<label>Foto</label>
			<x-image-picker name="other" value="{{ $image_id ? $image_id : '' }}"/>
			<label>Tekst</label>
			<textarea name="content" class="editor">{!! $text !!}</textarea>
		@break
		@default
			<p>ONBEKEND TYPE!</p>
		@endswitch
		@if (!$loop->first)
		<button form="shift-{{ $content->id }}" type="submit" class="left shift">↟ omhoog</button>
		@endif
		<div class="multiple multiple-2">
			@if ($show_save_button)
			<button form="update-{{ $content->id }}" type="submit">opslaan</button>
			@else
			<button disabled="disabled">opslaan</button>
			@endif
			<button form="delete-{{ $content->id }}" type="submit" class="right-align delete">⨯ blok verwijderen</button>
		</div>
		</form>
	</section>
	@php
	$previous_content = $content;
	@endphp
	@empty
	<p>
		Er zijn nog geen blokken opgeslagen.
	</p>
	@endforelse

	<section>
		<h3>Inhoud (blok) toevoegen</h3>
		<form class="" id="add-content" method="POST" action="{{ route('cms.pages.contents.store', ['page' => $page]) }}">
			@csrf
			<label>Type</label>
			<select name="type">
				@foreach (\App\Models\Content::TYPES as $type)
				<option value="{{ $type }}">{{ trans("content.$type.label") }}</option>
				@endforeach
			</select>
			<button type="submit">toevoegen</button>
		</form>
		@if (count($page->trashedContents))
		<h3>Eerder verwijderde blokken</h3>
		<p>
			Verwijderde blokken worden na een week automatisch definitief verwijderd!
		</p>
		<ul>
		@foreach ($page->trashedContents as $content)
			<li>
				<form class="no-layout" method="POST" action="{{
					route('cms.pages.contents.restore', ['page' => $page, 'content' => $content])
				}}">
					@csrf
					@method('PATCH')
					Blok "{{ trans("content.{$content->type}.label") }}", verwijderd op {{ $content->deleted_at }}
					<button type="submit">Terugzetten</button>
				</form>
			</li>
		@endforeach
		</ul>
		@endif
	</section>
</section>
@endsection
@once
@push('bottom_scripts')
<script src="/js/ckeditor4/ckeditor.js"></script>
<script>
	(function(){
		CKEDITOR.replaceAll(function (textarea, config) {
			config.customConfig = '/js/cms/ckeditor4/config-contents.js';
			return true;
		});
	})();
</script>
@endpush
@endonce
