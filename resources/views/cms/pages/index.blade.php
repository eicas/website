{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.user')

@section('main')
	<h2>Pagina's</h2>
	<form class="no-layout" id="create" method="POST" action="{{ route('cms.pages.store') }}">
		@csrf
		<input form="create" type="hidden" name="language" value="{{ config('app.locale') }}">
		<input form="create" type="hidden" name="visibility" value="0">
	</form>

	<table>
		<thead>
			<tr>
				<th>Url</th>
				<th>Titel</th>
				<th>Template</th>
				<th>Beeld-lijst</th>
				<th>Zichtbaar</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			@php
				$updated_id = session()->get('updated_id', 0);
			@endphp
			@foreach ($pages as $page)
			<tr{!! $page->id === $updated_id ? ' class="updated"' : '' !!}>
				<td>{{ $page->relative_url }}</td>
				<td>{{ $page->title }}</td>
				<td>{{ $page->template }}</td>
				<td>{{ $page->imageset ? $page->imageset->name : '-' }}</td>
				<td>{{ $page->visibility_label }}</td>
				<td>
					<div>
						<a class="button" href="{{ route('cms.pages.edit', ['page' => $page]) }}">Bewerken</a>
						<form class="no-layout" id="delete_{{ $page->id }}" method="POST" action="{{
							route('cms.pages.destroy', ['page' => $page])
						}}" onsubmit="return confirm('Zeker weten? Actie is definitief!');">
							@csrf
							@method('DELETE')
							<button type="submit">Verwijderen</button>
						</form>
					</div>
				</td>
			</tr>
			@endforeach
			<tr><th colspan="6">Nieuwe pagina</th></tr>
			<tr>
				<td><input form="create" type="text" name="slug" value="{{ old('slug') }}"></td>
				<td><input form="create" type="text" name="title" value="{{ old('title') }}" required></td>
				<td>
					<select form="create" name="template">
						@foreach (App\Models\Page::getAvailableTemplates() as $template)
						<option value="{{ $template }}"{!!
							$template === 'story' ? ' selected' : ''
						!!}>{{ $template }}</option>
						@endforeach
					</select>
				</td>
				<td>
					<select form="create" name="imageset_id">
						<option value="" selected>-</option>
						@foreach ($imagesets as $imageset)
						<option value="{{ $imageset->id }}">{{ $imageset->name }}</option>
						@endforeach
					</select>
				</td>
				<td>
					Omschrijving:
				</td>
				<td>
					<input form="create" type="text" name="description" value="{{ old('description') }}">
				</td>
			</tr>
			<tr>
				<td colspan="5"></td>
				<td><button form="create" type="submit">Opslaan</button></td>
			</tr>
		</tbody>
	</table>
@endsection

