{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.user')

@section('main')
	<form id="edit" method="POST" action="{{ route('cms.pages.update', ['page' => $page]) }}">
		@csrf
		@method('PATCH')

		<h2>Pagina wijzigen</h2>

		<input type="hidden" name="language" value="{{$page->language}}">

		<label>Url</label>
		<input type="text" name="slug" value="{{ old('slug', $page->slug) }}" required>

		<label>Titel</label>
		<input type="text" name="title" value="{{ old('title', $page->title) }}">

		<label>Omschrijving</label>
		<input type="text" name="description" value="{{ old('description', $page->description) }}">

		<label>Template</label>
		<select name="template">
			@foreach (App\Models\Page::getAvailableTemplates() as $template)
			<option value="{{ $template }}"{!!
				$template === $page->template ? ' selected' : ''
			!!}>{{ $template }}</option>
			@endforeach
		</select>

		<label>Beeld-lijst</label>
		<select name="imageset_id">
			<option value=""{!! !$page->imageset_id ? ' selected' : '' !!}>-</option>
			@foreach ($imagesets as $imageset)
			<option value="{{ $imageset->id }}"{!!
				$page->imageset_id === $imageset->id ? ' selected' : ''
			!!}>{{ $imageset->name }} ({{ $imageset->images_count }} beelden)</option>
			@endforeach
		</select>

		<label>Zichtbaarheid</label>
		<select name="visibility">
			@foreach (App\Models\Page::VISIBILITY as $index => $label)
			<option value="{{ $index }}"{!!
				$index === $page->visibility ? ' selected' : ''
			!!}>{{ $label }}</option>
			@endforeach
		</select>

		<button type="submit">opslaan</button>
	</form>
	<hr>
	<p><a class="button" href="{{ route('cms.pages.contents.index', ['page' => $page])}}">Pagina-inhoud aanpassen</a></p>
@endsection
