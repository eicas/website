{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.user')

@section('main')
	<form id="edit" method="POST" action="{{ route('cms.images.update', ['image' => $image]) }}">
		@csrf
		@method('PATCH')

		<h2>Omschrijving van beeld wijzigen</h2>

		<label>Beeld</label>
		<div class="multiple multiple-150px-rest">
			<img src="{{
				Storage::disk('images')->has($image->getPath($image->getSmallestWidth()))
					? Image::make(
						Storage::disk('images')->path($image->getPath($image->getSmallestWidth()))
					)->stream('data-url')
					: ''
			}}" alt={{ $image->alt }}>
			<table class="key-value">
				<tbody>
					<tr>
						<th>naam</th>
						<td class="code">{{ $image->filename }}</td>
					</tr>
					<tr>
						<th class="small">naam bij upload</th>
						<td class="small code em">{{ $image->original_filename }}</td>
					</tr>
					<tr>
						<th class="small">opgeslagen formaten (w×h)</th>
						<td class="small code">
						<a href="{{ $image->getUrl() }}">full</a>
						@foreach (collect($image->heights)->sortKeys() as $width => $height)
							<a href="{{ $image->getUrl($width) }}">{{ $width }}×{{ $height }}</a>
						@endforeach
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<label>Omschrijving (alt-text)</label>
		<input type="text" name="alt" value="{{ old('alt', $image->alt) }}" required>

		<hr>
		<fieldset>
			<label>Titel</label>
			<input type="text" name="title" value="{{ old('title', $image->title) }}">

			<label>Artiest</label>
			<input type="text" name="artist" value="{{ old('artist', $image->artist) }}">

			<label>Jaar</label>
			<input type="number" step="1" min="1900" max="3000" name="year" value="{{ old('year', $image->year) }}">

			<label>Extra informatie</label>
			<textarea name="info">{!! old('info', $image->info) !!}</textarea>
		</fieldset>
		<div>OF (als een excerpt is gedefinieerd, worden daarvan de titel, artiest, jaar en extra informatie overgenomen.)</div>
		<fieldset>
			<label>Excerpt van</label>
			<x-image-picker name="image_id" value="{{ old('image_id', $image->image_id) }}" />
		</fieldset>

		<button type="submit">opslaan</button>
	</form>
@endsection
