{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.user')

@section('main')
	<h2>Beelden</h2>
	<form class="no-layout space" id="create" method="GET" action="{{ route('cms.images.create') }}">
		<button type="submit">Nieuwe afbeelding</button>
	</form>

	<table>
		<thead>
			<tr>
				<th>Beeld</th>
				<th>Excerpt van</th>
				<th>Excerpten</th>
				<th>Gebruik</th>
			</tr>
		</thead>
		<tbody>
			@php
				$updated_id = session()->get('updated_id', 0);
			@endphp
			@foreach ($images as $image)
			<tr{!! $image->id === $updated_id ? ' class="updated"' : '' !!}>
				<td>
					<img src="{{
						Storage::disk('images')->has($image->getPath($image->getSmallestWidth()))
							? Image::make(
								Storage::disk('images')->path($image->getPath($image->getSmallestWidth()))
							)->stream('data-url')
							: ''
					}}" alt={{ $image->alt }}>
				</td>
				<td class="excerpt-of">
					@if ($image->large)
					<img src="{{
						Storage::disk('images')->has($image->large->getPath($image->large->getSmallestWidth()))
							? Image::make(
								Storage::disk('images')->path($image->large->getPath($image->large->getSmallestWidth()))
							)->stream('data-url')
							: ''
					}}" alt={{ $image->large->alt }}>
					@endif
				</td>
				<td class="excerpts">
					<div>
						@foreach ($image->small as $excerpt)
						<img src="{{
							Storage::disk('images')->has($excerpt->getPath($excerpt->getSmallestWidth()))
								? Image::make(
									Storage::disk('images')->path($excerpt->getPath($excerpt->getSmallestWidth()))
								)->stream('data-url')
								: ''
						}}" alt={{ $excerpt->alt }}>
						@endforeach
					</div>
				</td>
				<td class="usage">
					<div>
						<a class="button" href="{{ route('cms.images.edit', ['image' => $image]) }}">Bewerken</a>
						{{-- @php
						$counts = [];
						if ($image->contents_count > 0) {
							$counts[] = "{$image->contents_count}× in teksten";
						}
						if ($image->profiles_count > 0) {
							$counts[] = "{$image->profiles_count}× in profielen";
						}
						@endphp
						@if (count($counts) > 0)
						{!! implode("<br>", $counts) !!}
						@else --}}
						<form class="no-layout" id="delete_{{ $image->id }}" method="POST" action="{{
							route('cms.images.destroy', ['image' => $image])
						}}" onsubmit="return confirm('Zeker weten? Actie is definitief!');">
							@csrf
							@method('DELETE')
							<button type="submit">Verwijderen</button>
						</form>
					</div>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@endsection
