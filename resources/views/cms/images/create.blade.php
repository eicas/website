{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.user')

@section('main')
	<form id="create" method="POST" action="{{ route('cms.images.store') }}" enctype="multipart/form-data">
		@csrf

		<h2>Nieuw beeld</h2>

		<label>Beeld</label>
		<input type="file" name="image" required focus>

		<label>Omschrijving</label>
		<input type="text" name="alt" value="{{ old('alt') }}" required>

		<button type="submit">opslaan</button>
	</form>
@endsection
