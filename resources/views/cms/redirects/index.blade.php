{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.user')

@section('main')
	<h2>Redirects (verwijzingen van oude pagina's)</h2>

	<table>
		<thead>
			<tr>
				<th>From</th>
				<th></th>
				<th>To</th>
				<th>Opmerking</th>
				<th>Gebruik</th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@php
				$updated_id = intval(old('id', session()->get('updated_id', 0)));
			@endphp
			@foreach ($redirects as $redirect)
			@php
				$id = $redirect->id;
			@endphp
			<tr{!! $id === $updated_id ? ' class="updated"' : '' !!}>
				<td>
					<form class="no-layout" id="update-{{ $id }}" method="POST" action="{{
						route('cms.redirects.update', ['redirect' => $redirect])
					}}">
						@csrf
						@method('PATCH')
						<input type="hidden" name="id" value="{{ $id }}">
					</form>
					<input form="update-{{ $id }}" type="text" name="from" value="{{
						$id === $updated_id
						? old('from', $redirect->from)
						: $redirect->from
					}}" required{!!
						($id === $updated_id) && $errors->has('from')
						? ' class="invalid"'
						: ''
					!!} style="width:15em">
				</td>
				<td>&gt;</td>
				<td>
					<input form="update-{{ $id }}" type="text" name="to" value="{{
						$id === $updated_id
						? old('to', $redirect->to)
						: $redirect->to
					}}" required{!!
						($id === $updated_id) && $errors->has('to')
						? ' class="invalid"'
						: ''
					!!} style="width:15em">
				</td>
				<td>
					<input form="update-{{ $id }}" type="text" name="comment" value="{{
						$id === $updated_id
						? old('comment', $redirect->comment)
						: $redirect->comment
					}}" style="width:15em">
				</td>
				<td>{{ $redirect->usage }}</td>
				<td>
					<button form="update-{{ $id }}" type="submit" title="Opslaan">🖫</button>
				</td>
				<td>
					<form class="no-layout" id="delete-{{ $id }}" method="POST" action="{{
						route('cms.redirects.destroy', ['redirect' => $redirect])
					}}" onsubmit="return confirm('Zeker weten? Actie is definitief!');">
						@csrf
						@method('DELETE')
						<button type="submit" title="Verwijderen">🗙</button>
					</form>
				</td>
			</tr>
			@endforeach
			<tr><th colspan="7">Nieuwe redirect</th></tr>
			<tr{!! $updated_id === 0 ? ' class="updated"' : '' !!}>
				<td>
					<form class="no-layout" id="create" method="POST" action="{{ route('cms.redirects.store') }}">
						@csrf
						<input type="hidden" name="id" value="0">
					</form>
					<input form="create" type="text" name="from" value="{{
						$updated_id === 0
						? old('from')
						: ''
					}}" required{!!
						($updated_id === 0) && $errors->has('from')
						? ' class="invalid" style="width:15em"'
						: ' style="width:15em;background-color:#fff"'
					!!}>
				</td>
				<td>&gt;</td>
				<td>
					<input form="create" type="text" name="to" value="{{
						$updated_id === 0
						? old('to')
						: ''
					}}" required{!!
						($updated_id === 0) && $errors->has('to')
						? ' class="invalid" style="width:15em"'
						: ' style="width:15em;background-color:#fff"'
					!!}>
				</td>
				<td>
					<input form="create" type="text" name="comment" value="" style="width:15em;background-color:#fff">
				</td>
				<td>
				</td>
				<td>
					<button form="create" type="submit" title="Opslaan">🖫</button>
				</td>
				<td>
				</td>
			</tr>
		</tbody>
	</table>

	<h3>Gebruik</h3>
	<p style="margin-bottom:1em">
		Redirects zorgen ervoor dat "oude", niet meer bestaande pagina's niet meteen een "niet gevonden" foutmelding
		geven.
		De linker kolom "from" bevat deze oude urls.
		De rechter kolom "to" bevat de urls van de pagina's waarheen verwezen wordt.
		Omdat het hier alleen om redirects gaat binnen de EICAS website, wordt de domeinnaam weggelaten en beginnen
		alle url's met '/'.
	</p>
	<p style="margin-bottom:1em">
		Als van een pagina de url wordt aangepast, wordt er automatisch een redirect gemaakt.
	</p>
	<p style="margin-bottom:1em">
		Als een bezoeker een url van EICAS probeert, zal er eerst gezocht worden naar een pagina op dat adres.
		Mocht die niet gevonden worden, dan wordt deze redirect-tabel geraadpleegd en wordt de bezoeker zo mogelijk
		doorverwezen.
	</p>
	<p style="margin-bottom:1em">
		Let op: het is mogelijk om een redirect te maken naar een url die een andere redirect is.
		Dat is op zich mogelijk, maar te veel redirects (meer dan {{
			\App\Models\Redirect::MAX_REDIRECTS
		}}) zijn niet toegestaan.
	</p>
@endsection

