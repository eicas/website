{{--
SPDX-FileCopyrightText: 2021 - 2022 Marten Koetsier <eicas@koetsierengineering.nl>
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>

SPDX-License-Identifier: MIT
--}}

@if ($showUser)
<footer id="main-user">
	@guest
		{{-- <a href="{{ route('profile.login') }}">Inloggen</a>
		<a href="{{ route('profile.register') }}">Aanmelden</a> --}}
	@endguest
	@auth
		<a class="profile" href="{{
			route('profile.show')
		}}" title="Ingelogd als {{ Auth::user()->name }} &lt;{{ Auth::user()->email }}&gt;">
			Ingelogd als {{ Auth::user()->name }} &lt;{{ Auth::user()->email }}&gt;
		</a>
		{{-- <form class="no-layout" id="header-logout-form" method="POST" action="{{ route('profile.logout') }}">
			@csrf
			@method('DELETE')
			<button type="submit">Uitloggen</button>
		</form> --}}
	@endauth
</footer>
@endif
@if ($showMain)
<footer id="main-foot">
	<a href="https://www.facebook.com/EICAS.Deventer/" target="_blank" class="social-logo facebook">Facebook</a>
	<a href="https://www.instagram.com/museumeicas/" target="_blank" class="social-logo instagram">Instagram</a>
	<a rel="me" href="https://mastodon.art/@EICAS" target="_blank" class="social-logo mastodon">Mastodon</a>
	<a href="https://www.linkedin.com/company/eicas" target="_blank" class="social-logo linkedin">LinkedIn</a>
</footer>
@endif
