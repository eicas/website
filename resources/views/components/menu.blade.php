{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

<nav id="main-menu">
	<label for="menu-top" id="hamburger"><div>menu<div></div></div></label>
	<input id="menu-top" type="checkbox">
	<ul id="main-menu-base">
		@include('components.menu.menuitem', ['subitem' => null, 'subitems' => $menu])
	</ul>
</nav>
@once
@push('bottom_scripts')
<script>(function(){
'use strict';
const ms=document.getElementById('main-menu').querySelectorAll('li.submenu');
function par(li){let p;if(li&&(p=li.parentElement)&&(p=p.parentElement)&&p.classList.contains('submenu')){return p;}return null;}
function handle(li){var nc=[li.id],p=li;while(p=par(p)){nc.push(p.id)}closeExcept(nc);}
function closeExcept(nc){ms.forEach(function(li){if(nc.includes(li.id)){return;}li.querySelector(':scope>input[type=checkbox]').checked=false;});}
ms.forEach(function(li){li.querySelector(':scope>label').addEventListener('click',function(e){handle(e.target.parentElement);});});
})();</script>
@endpush
@endonce
