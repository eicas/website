{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

<li data-image_id="{{ -1 }}">annuleren</li>
<li data-image_id="{{ 0 }}">OK</li>
@foreach ($images as $image)
<li data-image_id="{{ $image->id }}" @class([
	'selected' => in_array($image->id, $selected_image_ids),
])>
	<img src="{{
		Storage::disk('images')->has($image->getPath($image->getSmallestWidth()))
			? Image::make(
				Storage::disk('images')->path($image->getPath($image->getSmallestWidth()))
			)->stream('data-url')
			: ''
	}}" alt={{ $image->alt }} id="multi-image-picker-item-{{ $image->id }}" data-path="{{ $image->getPath() }}">
</li>
@endforeach