{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

<li data-image_id="{{ -1 }}">annuleren</li>
<li data-image_id="{{ 0 }}">geen afbeelding</li>
@foreach ($images as $image)
<li data-image_id="{{ $image->id }}">
	<img src="{{
		Storage::disk('images')->has($image->getPath($image->getSmallestWidth()))
			? Image::make(
				Storage::disk('images')->path($image->getPath($image->getSmallestWidth()))
			)->stream('data-url')
			: ''
	}}" alt={{ $image->alt }} id="image-picker-item-{{ $image->id }}" data-path="{{ $image->getPath() }}">
</li>
@endforeach