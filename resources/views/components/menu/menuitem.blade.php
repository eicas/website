{{--
SPDX-FileCopyrightText: 2021 - 2022 Marten Koetsier <eicas@koetsierengineering.nl>
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>

SPDX-License-Identifier: MIT
--}}

@if($subitems && count($subitems))
	@if($subitem)
	<li class="submenu" id="menuitem-{{ $subitem->id }}">
		<label for="menu-{{ $subitem->id }}">{!! $subitem->label !!}</label>
		<input id="menu-{{ $subitem->id }}" type="checkbox">
		<ul>
	@endif
	@foreach ($subitems as $menuitem)
		@if ($menuitem->menuitems)
			@include('components.menu.menuitem', ['subitem' => $menuitem, 'subitems' => $menuitem->menuitems])
		@else
			@include('components.menu.menuitem', ['subitem' => $menuitem, 'subitems' => null])
		@endif
	@endforeach
	@if($subitem)
		</ul>
	</li>
	@endif
@else
	@if ($subitem)
	<li><a href="{{ $subitem->href }}">{!! $subitem->label !!}</a></li>
	@endif
@endif
