{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>

SPDX-License-Identifier: MIT
--}}

<div class="image-picker" id="{{ $image_picker_id }}">
	<div class="preview">
		@isset($image)
		<img src="{{
			Storage::disk('images')->has($image->getPath($image->getSmallestWidth()))
				? Image::make(
					Storage::disk('images')->path($image->getPath($image->getSmallestWidth()))
				)->stream('data-url')
				: ''
		}}" alt="{{ $image->alt }}" id="image-picker-item-{{ $image->id }}">
		@endisset
	</div>
	<label>id</label><span>{{ $image ? $image->id : '–' }}</span>
	<label>bestand</label><span>{{ $image ? $image->getPath() : '–' }}</span>
</div>
<input type="hidden" name="{{ $name }}" value="{{ $image_id ?? '' }}" id="image-picker-input-{{ $image_picker_id }}">
@once
@push('bottom_scripts')
<div id="image-picker">
	<h4>Kies een afbeelding</h4>
	<ul id="image-picker-list">
		<li data-image_id="{{ -1 }}">annuleren</li>
		<li data-image_id="{{ 0 }}">geen afbeelding</li>
	</ul>
</div>
<script>
var
	image_picker = document.getElementById('image-picker'),
	image_picker_list = document.getElementById('image-picker-list'),
	image_picker_id;
(function () {
	var
		image_pickers = document.getElementsByClassName('image-picker'),
		l = image_pickers.length,
		i,
		ip,
		preview;
	for (i = 0; i < l; i++) {
		ip = image_pickers[i];
		preview = ip.getElementsByClassName('preview')[0];
		preview.addEventListener('click', function (e) {
			var xhr = new XMLHttpRequest();
			if (e.target.tagName == 'IMG')
				image_picker_id = e.target.parentNode.parentNode.id;
			else
				image_picker_id = e.target.parentNode.id;
			xhr.addEventListener('load', imagePickerLoad);
			xhr.open('GET', '{{ route('cms.imagepicker.index') }}?image_picker_id=' + image_picker_id);
			xhr.send();
		});
	}
})();
imagePickerLoad = function (event) {
	image_picker_list.innerHTML = event.target.response;
	image_picker.style.display = 'block';{{--
	/*
	TODO: check that the list was actually loaded and act if this is not the case.
	Background: this normally should work. The page script sends a GET request (XMLHttpRequest) to the
	'cms.imagepicker.index' route, also providing the 'image_picker_id' as url query string. The controller
	for that route returns a html-like string containing some <li>-items, which are placed inside the
	'image_picker_list' <ul> element.
	However, if something goes wrong and something else is returned, a warning should be given.
	Some idea: check whether the string only contains li elements
	*/
--}}
};
handleImageClick = function (event) {
	var form_control, preview, span_id, span_path, input, li, image_id, img;
	if (event.target.tagName === 'LI' || event.target.tagName === 'IMG') {
		form_control = document.getElementById(image_picker_id);
		preview = form_control.getElementsByClassName('preview')[0];
		span_id = form_control.getElementsByTagName('SPAN')[0];
		span_path = form_control.getElementsByTagName('SPAN')[1];
		input = document.getElementById('image-picker-input-' + image_picker_id);

		if (event.target.tagName === 'LI') {
			li = event.target;
		} else {
			li = event.target.parentNode;
		}
		image_id = li.dataset.image_id;
		if (image_id >= 0) {
			preview.innerHTML = '';
			span_id.innerHTML = '';
			span_path.innerHTML = '';

			if (image_id > 0) {
				img = document.getElementById('image-picker-item-' + image_id);
				img.id = '';
				preview.appendChild(img);
				span_id.innerHTML = image_id;
				span_path.innerHTML = img.dataset.path;
			} else {
				span_id.innerHTML = '–';
				span_path.innerHTML = '–';
			}

			input.value = image_id;
		}
		image_picker_list.innerHTML = '';
		image_picker.style.display = 'none';
		image_picker_id = null;
	} else {

	}
}
image_picker.addEventListener('click', handleImageClick);
</script>

@endpush
@endonce
