{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

<div class="multi-image-picker" id="{{ $multi_image_picker_id }}" data-selected_image_ids="{{ implode(",", $image_ids) }}">
	<div class="javascript-warning">
		Voor deze functionaliteit is javascript vereist.
	</div>
	<div class="preview">
		@foreach ($images as $image)
		<img src="{{
			Storage::disk('images')->has($image->getPath($image->getSmallestWidth()))
				? Image::make(
					Storage::disk('images')->path($image->getPath($image->getSmallestWidth()))
				)->stream('data-url')
				: ''
		}}" alt={{ $image->alt }} id="image-picker-item-{{ $image->id }}">
		@endforeach
	</div>
</div>
<input type="hidden" name="{{ $name }}" value="{{ implode(",", $image_ids ?? [])
	}}" id="multi-image-picker-input-{{ $multi_image_picker_id }}">
@once
@push('bottom_scripts')
<div id="multi-image-picker">
	<h4>Selecteer afbeeldingen</h4>
	<ul id="multi-image-picker-list">
		<li data-image_id="{{ -1 }}">annuleren</li>
		<li data-image_id="{{ 0 }}">OK</li>
	</ul>
</div>
<script>
var
	multi_image_picker = document.getElementById('multi-image-picker'),
	multi_image_picker_list = document.getElementById('multi-image-picker-list'),
	multi_image_picker_id;
(function () {
	var
		multi_image_pickers = document.getElementsByClassName('multi-image-picker'),
		l = multi_image_pickers.length,
		i,
		mip,
		preview;
	for (i = 0; i < l; i++) {
		mip = multi_image_pickers[i];
		mip.querySelector('.javascript-warning').style.display = 'none';
		preview = mip.querySelector('.preview');
		preview.addEventListener('click', multiImagePickerClick);
	}
})();
function multiImagePickerClick(event) {
	var mip = event.target.parentNode, xhr = new XMLHttpRequest();
	if (mip.classList.contains('preview')) {
		mip = mip.parentNode;
	}
	multi_image_picker_id = mip.id;
	xhr.addEventListener('load', multiImagePickerLoad);
	xhr.open(
		'GET',
		'{{ route('cms.multiimagepicker.index') }}?' +
		[
			'multi_image_picker_id=' + mip.id,
			'selected_image_ids=' + mip.dataset.selected_image_ids
		].join('&')
	);
	xhr.send();
}
function multiImagePickerLoad(event) {
	multi_image_picker_list.innerHTML = event.target.response;
	multi_image_picker.style.display = 'block';{{--
	/*
	TODO: check that the list was actually loaded and act if this is not the case.
	Background: this normally should work. The page script sends a GET request (XMLHttpRequest) to the
	'cms.multiimagepicker.index' route, also providing the 'multi_image_picker_id' as url query string.
	The controller for that route returns a html-like string containing some <li>-items, which are placed
	inside the 'multi_image_picker_list' <ul> element.
	However, if something goes wrong and something else is returned, a warning should be given.
	Some idea: check whether the string only contains li elements
	*/
--}}
};
handleImageClick = function (event) {
	var li, image_id;
	if (event.target.tagName === 'LI' || event.target.tagName === 'IMG') {
		if (event.target.tagName === 'LI') {
			li = event.target;
		} else {
			li = event.target.parentNode;
		}
		image_id = parseInt(li.dataset.image_id);
		switch (image_id) {
			case -1:
				closeMultiImagePicker(false);
				break;
			case 0:
				closeMultiImagePicker(true);

				break;
			default:
				li.classList.toggle('selected');
		}
	} else {

	}
}
function closeMultiImagePicker(apply) {
	var form_control, preview, lis, image_ids, img;
	if (apply) {
		form_control = document.getElementById(multi_image_picker_id);
		preview = form_control.querySelector('.preview');

		preview.innerHTML = '';
		image_ids = [];

		multi_image_picker_list.querySelectorAll('li.selected').forEach(function (li) {
			var img = li.querySelector('img');
			image_ids.push(parseInt(img.id.split('-').pop(), 10));
			preview.appendChild(img);
		});
		document.getElementById('multi-image-picker-input-' + multi_image_picker_id).value = image_ids.join(',');
		document.getElementById(multi_image_picker_id).dataset.selected_image_ids = image_ids.join(',');
	}

	multi_image_picker_list.innerHTML = '';
	multi_image_picker.style.display = 'none';
	multi_image_picker_id = null;
}
multi_image_picker.addEventListener('click', handleImageClick);
</script>
@endpush
@endonce