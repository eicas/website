{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

@php
	$unique_id = \App\Helpers\Misc::nextInteger();
@endphp
@if (isset($extra['error']))
<div>{!! $extra['error'] !!}</div>
@else
<div id="TrsWebShopEmbedded-{!! $unique_id !!}"></div>
@once
@push('bottom_scripts')
<script>  
	!function () {
		var t,w;
		window.TrsWebWidget = "tww", "tww" in window || (window.tww = function () { window.tww.q.push(arguments) }, window.tww.q = []);
		t = document.createElement("script");
		t.src = "//secure.ticketunie.com/Widgets/Embed/trswebwidgets.js?d=" + (new Date()).getDay(), t.async = 1;
		w = document.getElementsByTagName("script")[0];
		w.parentNode.insertBefore(t, w);
	}();
</script>
@endpush
@endonce
@push('bottom_scripts')
@php
	echo "<script>\n";
	foreach ([
		[
			"Create",
			$extra['server_id'],
			$extra['contentholder_id'],
			substr(App::currentLocale(), -2),
			"TrsWebShopEmbedded-$unique_id",
		], [
			"ProductionsByDateTime",
			json_encode([
				"ProductionID" => $extra['production_id'],
				"ViewType" => "Calendar",
				"ViewColumns" => "Location",
			], JSON_FORCE_OBJECT + JSON_NUMERIC_CHECK),
		]
	] as $call) {
		foreach ($call as &$param) {
			if (!is_numeric($param)) {
				$param = "'$param'";
			}
		}
		echo "\ttww(", implode(",", $call), ");\n";
	}
@endphp
</script>
@endpush
@endif
