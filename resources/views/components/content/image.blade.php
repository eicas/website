{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>

SPDX-License-Identifier: MIT
--}}
@if ($extra['image'])
@php
	if (isset($extra['embed']) && $extra['embed']) {
		$imgSrc = Storage::disk('images')->has($extra['image']->getPath($extra['image']->getSmallestWidth()))
				? Image::make(
					Storage::disk('images')->path($extra['image']->getPath($extra['image']->getSmallestWidth()))
				)->stream('data-url')
				: '';
	} else {
		$imgSrc = '';
	}
	$attributes = [
		'src="' . $imgSrc . '"',
		"alt=\"{$extra['image']->alt}\"",
		'class="' . implode(" ", $extra['image-classes'] ?? []) . '"',
		"data-image-pt=\"{$extra['image']->getUrlTemplate()}\"",
		'data-image-hs="' .
			str_replace('"', "'", json_encode($extra['image']->heights, JSON_FORCE_OBJECT + JSON_NUMERIC_CHECK))
			. '"',
	];
	if (isset($extra['image-popup-art'])) {
		foreach ($extra['image-popup-art'] as $key => $value) {
			$attributes[] = "data-image-popup-art-{$key}=\"" . str_replace('"', '&quot;', $value ?? '') . "\"";
		}
	}
	$attributes = implode(" ", $attributes);
@endphp
{!! $extra['image-before'] ?? '' !!}<img {!! $attributes !!}>{!! $extra['image-after'] ?? '' !!}
@once
@push('bottom_scripts')
<script src="/js/image.js"></script>
@endpush
@endonce
@else
{!! $extra['image-before'] ?? '' !!}Afbeelding kan niet geladen worden.{!! $extra['image-after'] ?? '' !!}
@endif
