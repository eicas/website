{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

@php
	$attributes = implode(" ", [
		'frameborder="0"',
		'scrolling="no"',
		'marginheight="0"',
		'marginwidth="0"',
		'type="text/html"',
	]);
	$parameters = implode("&", [
		'autoplay=0',
		'fs=1',
		'iv_load_policy=3',
		'showinfo=0',
		'rel=0',
		'cc_load_policy=0',
		'start=0',
		'end=0',
		'origin=https://www.eicas.nl/'
	]);
	$src = "https://www.youtube.com/embed/{$extra['youtube_id']}?$parameters";
@endphp
<div class="youtube-embed" id="youtube-embed-{{
	\App\Helpers\Misc::nextInteger()
}}" data-youtube-id="{{ $extra['youtube_id'] }}">
{{-- <iframe {!! $attributes !!} src="{!! $src !!}"></iframe> --}}
	<a href="https://www.youtube.com/watch?v={{ $extra['youtube_id'] }}" target="_blank">
		<img src="/img/youtube_preview/{{ $extra['youtube_id'] }}.jpg" alt="preview voor Youtube video '{{ $extra['youtube_id'] }}'">
	</a>
</div>