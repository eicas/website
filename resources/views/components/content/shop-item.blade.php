{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

@php
	$options = [
		'image-popup-art' => true,
	];
@endphp
@isset($extra['image'])
<aside><x-image :image="$extra['image']" :options="$options"/></aside>
@endisset
<article>{!! $getTextContent() !!}</article>
