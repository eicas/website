{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

<div id="mc_embed_signup">
	<form action="{!! $extra['action'] !!}" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
		<label for="mce-EMAIL">E-mailadres:</label>
		<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
		<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
		<input type="text" name="b_de5639e6a254e294e58dd4158_4bbddd7a44" tabindex="-1" value="">
		<button type="submit" value="Aanmelden" name="subscribe" id="mc-embedded-subscribe">Aanmelden</button>
	</form>
</div>