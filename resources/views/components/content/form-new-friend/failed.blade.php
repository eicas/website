{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

<h2>Mislukt</h2>
<p>
	De betaling van uw donatie is helaas niet aangekomen.
</p>
@if (session()->has('error'))
<p>{!! session()->get('error') !!}</p>
@endif