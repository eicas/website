{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>

SPDX-License-Identifier: MIT
--}}

<form method="POST" action="{{ route('guest_forms.new_friend.store') }}">
	@csrf
	<input type="hidden" name="redirect_url" value={{ request()->url() }}>

	<label for="name">Naam*</label>
	<input type="text" id="name" name="name" value="{{ old('name') }}" required maxlength="255">
	@error('name')
	<span class="warning">{{ $message }}</span>
	@enderror

	<label for="email">E-mailadres*</label>
	<input type="email" id="email" name="email" value="{{ old('email') }}" required maxlength="255">
	@error('email')
	<span class="warning">{{ $message }}</span>
	@enderror

	@foreach ([
		'telephone' => 'Telefoonnummer',
		'address' => 'Adres',
		'postalcode' => 'Postcode',
		'city' => 'Plaats',
		'country' => 'Land',
	] as $field => $label)
	<label for="{{ $field }}">{{ $label }}</label>
	<input type="text" id="{{ $field }}" name="{{ $field }}" value="{{ old($field) }}" maxlength="255">
	@error($field)
	<span class="warning">{{ $message }}</span>
	@enderror
	@endforeach

	<label for="amount">Bedrag (€)*</label>
	<select id="amount" name="amount">
		@foreach (\App\Models\GuestForms\NewFriend::AMOUNTS as $amount)
		<option value="{{ $amount }}"{!!
			old('amount') === $amount ? ' selected' : ''
		!!}>€ {{ $amount / 100 }}</option>
		@endforeach
	</select>
	@error('amount')
	<span class="warning">{{ $message }}</span>
	@enderror

	<select name="payment_method">
		@foreach (\App\Models\GuestForms\NewFriend::getPaymentMethods() as $method => $label)
		<option value="{{ $method }}"{!!
			old('payment_method') === $method ? ' selected' : ''
		!!}>{{ $label }}</option>
		@endforeach
	</select>
	@error('payment_method')
	<span class="warning">{{ $message }}</span>
	@enderror

	<button type="submit">Doneren</button>
</form>
