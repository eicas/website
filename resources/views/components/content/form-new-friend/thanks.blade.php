{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

<h2>Dank u</h2>
<p>
	Hartelijk dank voor uw donatie.
</p>