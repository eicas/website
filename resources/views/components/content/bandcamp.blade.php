{{--
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>

SPDX-License-Identifier: MIT
--}}

<div class="bandcamp-embed" id="bandcamp-embed-{{ $extra['album'] }}">
  <a href="https://{{ $extra['artist'] }}.bandcamp.com/album/{{ $extra['album'] }}" target="_blank">
      <img src="/img/bandcamp_preview/{{ $extra['album'] }}.jpg" alt="preview voor Bandcamp-album {{ $extra['album'] }}">
  </a>
</div>
