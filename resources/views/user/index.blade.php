{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.user')

@section('main')
	<h2>Dashboard</h2>

	<nav>
		<a href="{{ route('profile.show') }}">
			<h3>Uw profiel</h3>
			<p>
				Beheer uw profiel
			</p>
		</a>
		@if (Auth::user()->hasAnyRole(['admin', 'user-admin']))
		<a href="{{ route('profiles.index', ['admin' => 1]) }}">
			<h3>Gebruikersbeheer</h3>
			<p>
				Beheer de {{ $internal_users_count }} EICAS-profielen
			</p>
		</a>
		<a href="{{ route('profiles.index', ['admin' => 0]) }}">
			<h3>Gastenbeheer</h3>
			<p>
				Beheer de {{ $external_users_count }} gastgebruikers
			</p>
		</a>
		@endif
		@if (Auth::user()->hasAnyRole(['admin', 'text-admin', 'art-admin', 'menu-admin']))
		<a href="{{ route('cms.index') }}">
			<h3>CMS</h3>
			<p>Content Management System</p>
		</a>
		@endif
		@if (Auth::user()->hasAnyRole(['admin', 'friend-admin']))
		<a href="{{ route('guest_forms.new_friend.index') }}">
			<h3>Vrienden</h3>
			<p>Vrienden en hun donaties</p>
		</a>
		@endif
	</nav>
@endsection
