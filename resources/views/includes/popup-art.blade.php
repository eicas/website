{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>

SPDX-License-Identifier: MIT
--}}

<div id="popup-art">
	<figure>
		<div id="popup-art-img-holder">
		</div>
		<figcaption>
			<section id="popup-art-title"></section>
			<section id="popup-art-extra"></section>
		</figcaption>
		<div id="popup-art-close"></div>
	</figure>
</div>
@once
@push('bottom_scripts')
<script>
(function () {
	const p = document.getElementById('popup-art');
	var aw,ah;

	function getOrCreateImg(){
		const existingImg = p.querySelector('#popup-art-img');
		if (existingImg) {
			return existingImg;
		} else {
			const img = document.createElement('img');
			img.id = 'popup-art-img';

			const holder = p.querySelector('#popup-art-img-holder');
			holder.appendChild(img);

			function setCaptionMargin(){p.querySelector('figcaption').style.margin='0 '+window.getComputedStyle(img).marginLeft;}
			img.addEventListener('load',function(){setCaptionMargin();window.setTimeout(function(){setCaptionMargin();},1);});

			return img;
		}
	}

	p.style.visibility = 'hidden';p.style.display = 'block';
	aw=p.querySelector('figure').offsetWidth;ah=p.querySelector('figure').offsetHeight;
	p.style.display = 'none';p.style.visibility = 'visible';

	function popupArtShow(src, alt, title, extra) {
		const img = getOrCreateImg();
		p.style.display = 'block';
		img.src = src;img.alt = alt;
		p.querySelector('#popup-art-title').innerHTML = title;
		p.querySelector('#popup-art-extra').innerHTML = extra.replace('&quot;', '"');
	}
	document.getElementById('popup-art-close').addEventListener('click',function(){p.style.display='none';});
	document.querySelectorAll('.image-preview.image-popup-art').forEach(function (img) {
		function parentWithId(e, id) {
			while(e&&e.id!==id)e=e.parentElement;
			return e;
		}
		function parentWithClassAndTag(img) {
			const t=img.dataset.imagePopupArtParentTagname.toUpperCase();
			const c=img.dataset.imagePopupArtParentClassname;
			var classMatches;
			do{classMatches=!c||c.split(' ').every(function(a){return img.classList.contains(a)});
				if (!classMatches||t&&img.tagName!==t){img=img.parentElement;}else{break;}
			}while(img);
			return img;
		}
		function findTarget(img) {
			if(img.dataset.imagePopupArtParentSelector) {
				const id=img.dataset.imagePopupArtParentId;

				const target = id ? parentWithId(img, id) : parentWithClassAndTag(img);
				return target ? target : img;
			} else {
				return img;
			}
		}
		const target = findTarget(img)
		target.classList.add('p-target-marker')

		target.addEventListener('click', function (e) {
			var img=e.target,d,h,ws,i,l,ow,oh;
			if(!img.classList.contains('image-popup-art')){
				while(!img.classList.contains('p-target-marker'))img=img.parentElement;
				img=img.querySelector('img.image-popup-art');
			}
			d=img.dataset;h=JSON.parse(d.imagePopupArtHs.replaceAll("'",'"'));
			ws=Object.keys(h).sort(function(a,b){return a-b});l=ws.length;
			for(i=0;i<l;i++){ow=ws[i];oh=h[ow];if(aw<ow||ah<oh){i=l+1}}
			popupArtShow(
				d.imagePopupArtPt.replace('%w',ow).replace('%h',oh),
				d.imagePopupArtAlt,
				d.imagePopupArtTitle,
				d.imagePopupArtExtra
			);
		});
	});
})();
</script>
@endpush
@endonce
