{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.user')

@php
	// This view is used both for the show action and the edit action. If the show action was called,
	// the variable $show will be set to true.
	// Here, make sure that the variable exists and default it to false if not.
	$show = isset($show) ? $show : false;
@endphp
@section('main')
<h2>Details gebruiker {{ $user->name }}</h2>

@if ($show && $user->id === Auth::id())
<div class="alert"><p>U kunt uw eigen gebruikersprofiel niet aanpassen.</p></div>
@elseif ($show)
<div class="alert"><p>U kunt het profiel van deze gebruiker niet aanpassen.</p></div>
@endif

<form id="update" method="POST" action="{{ $show ? '' : route('profiles.update', ['user' => $user]) }}"{!!
	$show ? ' disabled="disabled"' : ''
!!}>
	@csrf
	@method('PATCH')

	<label for="name">Naam</label>
	<input type="text" id="name" name="name" value="{{ old('name', $user->name) }}" required{!!
		$show ? ' readonly="true"' : ''
	!!}>

	<label for="email">E-mail (login)</label>
	<input type="email" id="email" name="email" value="{{ old('email', $user->email) }}" required{!!
		$show ? ' readonly="true"' : ''
	!!}>

	<label>Rollen</label>
	<div class="multiple multiple-auto-rest">
		@foreach ($roles as $role)
		<label>
			<input type="checkbox" name="roles[]" value="{{ $role->key }}"{!!
				$user->hasRole($role->key) ? ' checked' : ''
			!!}{!! $show ? ' disabled' : '' !!}>
			{{ $role->translatedName }}
		</label>
		<span>{{ $role->translatedDescription }}</span>
		@endforeach
	</div>

	@unless($show)
	<button type="submit">🖫 Opslaan</button>
	@endunless
</form>

<h3>Data</h3>
<table>
	<thead>
		<tr>
			<th>Aangemeld</th>
			<th>E-mail geverifieerd</th>
			<th>Laatst gewijzigd</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>{{ $user->created_at }}</td>
			<td>{{ $user->email_verified_at }}</td>
			<td>{{ $user->updated_at }}</td>
		</tr>
	</tbody>
</table>

<h3>Inlogsessies</h3>
<table>
	<thead>
		<tr>
			<th>Start</th>
			<th>Duur</th>
			<th>Activiteit</th>
			<th>ip-adres</th>
			<th>Browser</th>
			<th>Status</th>
			<th>Permanent</th>
			<th>Acties</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($user->userSessions as $userSession)
		<tr{!! $userSession->id === ($user->userSession->id ?? 0) ? ' style="font-style:italic"' : '' !!}>
			<td>{{ $userSession->created_at }}</td>
			<td>{{ \App\Helpers\Misc::timeFromSeconds($userSession->updated_at->diffInSeconds($userSession->created_at)) }}</td>
			<td>{{ $userSession->number_of_visits }}</td>
			<td>{{ $userSession->ip }}</td>
			<td title="{{ $userSession->user_agent }}">{{ \App\Helpers\Misc::shortUserAgent($userSession->user_agent) }}</td>
			<td>{{ $userSession->expired ? "verlopen (" . \App\Helpers\Misc::timeFromSeconds($userSession->expired) . ")" : 'actief' }}</td>
			<td>{{ $userSession->canRemember ? 'ja' : 'nee' }}</td>
			<td>
				@if ($userSession->id === ($user->userSession->id ?? 0))
				huidige sessie
				@elseif ($userSession->canRemember && !$userSession->expired && (!$show || $user->id === Auth::id()))
				<form class="no-layout" id="form-user-session-{{ $userSession->id }}" method="POST" action="{{
					route('profile.session.delete', ['user' => $user, 'userSession' => $userSession])
				}}">
					@csrf
					@method('DELETE')
					<button type="submit">uitloggen</button>
				</form>
				@endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

<h3>Account verwijderen</h3>
<form id="delete" method="POST" action="{{ route('profiles.destroy', ['user' => $user])
}}" onsubmit="return confirm('Zeker weten?')" class="no-layout">
	@csrf
	@method('DELETE')
	<button type="submit">Account verwijderen</button>
</form>
@endsection
