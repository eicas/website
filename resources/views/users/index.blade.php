{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.user')

@section('main')
<h2>Gebruikersbeheer</h2>
<table>
	<thead>
		<tr>
			<th>Naam</th>
			<th>Email (loginnaam)</th>
			{{-- <th>MFA</th> --}}
			<th>Aangemeld</th>
			<th>Gewijzigd</th>
			<th>Rollen</th>
			<th title="aantal Actieve Sessies">#AS*</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		@foreach ($users as $user)
		<tr>
			<td>{{ $user->name }}</td>
			<td style="{!!
				is_null($user->email_verified_at) ? 'font-style:italic' : 'font-weight:bold'
			!!}">{{ $user->email }}</td>
			{{-- <td>{{ is_null($user->mfa_confirmed_at) ? 'nee' : 'ja' }}</td> --}}
			<td style="white-space:nowrap">{{ $user->created_at->format('Y-m-d H:i') }}</td>
			<td style="white-space:nowrap">{{ $user->updated_at->format('Y-m-d H:i') }}</td>
			<td>
				<ul class="roles">
					@foreach ($user->roles as $role)
					<li>{{ $role->translatedName }}</li>
					@endforeach
				</ul>
			</td>
			<td>{{ $user->user_sessions_count }}</td>
			<td>
				<form class="no-layout" method="GET" action="{{
					route('profiles.' . (Auth::user()->can('update', $user) ? 'edit' : 'show'), ['user' => $user])
				}}">
					<button type="submit">details</button>
				</form>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
<p>
	* #AS = aantal Actieve Sessies.
</p>
@endsection
