{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>

SPDX-License-Identifier: MIT
--}}

@extends('layouts.user')

@section('main')
	<h2>Vrienden en hun donaties</h2>
	<table>
		<thead>
			<tr>
				<th>Datum / tijd</th>
				<th>Naam</th>
				<th>Contact</th>
				<th>Adres</th>
				<th>Bedrag</th>
				<th>Methode</th>
				<th>Referentie</th>
				<th>Mollie</th>
				<th>Status</th>
				<th>Betaald</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($new_friends as $new_friend)
			<tr{!! $new_friend->payment_test ? ' style="color:#666;font-style:italic"' : '' !!}>
				<td>{{ $new_friend->created_at->format('Y-m-d H:i:s') }}</td>
				<td>{{ $new_friend->name }}</td>
				<td>
					{{ $new_friend->email }}<br>
					{{ $new_friend->telephone }}
				</td>
				<td>
					{{ $new_friend->address }}<br>
					{!! $new_friend->payment_test
						? '<em style="font-size:80%;text-decoration:underline">testcode</em>'
						: e($new_friend->postalcode)
					!!} {{ $new_friend->city }}<br>
					{{ $new_friend->country }}
				</td>
				<td>{{ $new_friend->amount }}</td>
				<td>{!! $new_friend->payment_method . ($new_friend->payment_test ? "<br>TEST" : "") !!}</td>
				<td>{{ $new_friend->payment_reference }}</td>
				<td>{{ $new_friend->payment_id }}</td>
				<td>{{ $new_friend->payment_status }}</td>
				<td>
					@if ($new_friend->paid_at)
					{{ $new_friend->paid_at }}
					@else
					<form method="post" action="{!!
						route('guest_forms.new_friend.webhook', [
							'id' => $new_friend->payment_id,
							'redirect_url' => route('guest_forms.new_friend.index'),
						])
					!!}" class="no-layout">
						@csrf
						<button type="submit" class="btn-refresh">check nu</button>
					</form>
					@endif
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@endsection

