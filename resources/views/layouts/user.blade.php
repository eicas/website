{{--
SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>

SPDX-License-Identifier: MIT
--}}

<!doctype html>
<html lang="{{ str_replace('_', '-', App::currentLocale()) }}">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Beheer: EICAS - {{ $title ?? 'European Institute for Contemporary Art and Science' }}</title>
		<link rel="stylesheet" href="{{ mix('/css/user.css') }}" type="text/css" media="screen">
		<link rel="icon" href="/favicon.ico" sizes="any">
		<link rel="icon" href="/icon.svg" type="image/svg+xml">
		<link rel="apple-touch-icon" href="/apple-touch-icon.png">
	</head>
	<body>
		<header>
			<h1><a href="{{ route('cms.index') }}"><img src="{{ \App\Helpers\Misc::getCacheablePublicPath('/img/logo-eicas.png') }}" alt="logo EICAS"></a></h1>
			<section id="user-info">
				@guest
					<a href="{{ route('profile.login') }}">Inloggen</a>
					<a href="{{ route('profile.register') }}">Aanmelden</a>
				@endguest
				@auth
				<a href="{{ route('profile.show') }}">Ingelogd als {{ $user->name }} &lt;{{ $user->email }}&gt;</a>
				<form class="no-layout" id="header-logout-form" method="POST" action="{{ route('profile.logout') }}">
					@csrf
					@method('DELETE')
					<button type="submit">Uitloggen</button>
				</form>
			</section>
			@if (count($crumbs))
			<section id="crumbs">
				<ul>
					@foreach ($crumbs as $crumb)
					<li>
						@if (isset($crumb['link']))
						<a href="{{ $crumb['link'] }}">{{ $crumb['label'] }}</a>
						@else
						<h4>{{ $crumb['label'] }}</h4>
						@endif
					</li>
					@endforeach
				</ul>
			</section>
			@endif
			@endauth
		</header>
		@if ($errors->any())
		<div class="alert alert-danger">
			<h2>{{ trans_choice('general.error-present', count($errors)) }}</h2>
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		@if (session('status'))
		<div class="alert alert-success">
			<h2>{{ __('general.status-present') }}</h2>
			{{ session('status') }}
		</div>
		@endif
		<main class="{{ $css_classes }}">
			@yield('main')
		</main>
		@yield('body_end')
		@stack('bottom_scripts')
	</body>
</html>
