{{--
SPDX-FileCopyrightText: 2021 - 2022 Marten Koetsier <eicas@koetsierengineering.nl>
SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>

SPDX-License-Identifier: MIT
--}}

<!doctype html>
<html lang="{{ str_replace('_', '-', App::currentLocale()) }}" prefix="og: https://ogp.me/ns#">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>EICAS - {{ $page->title ?? 'European Institute for Contemporary Art and Science' }}</title>
		<meta name="description" content="{{ $page->description ?? 'Nederlands museum voor hedendaagse kunst rondom de rebelse Nul- en ZERO-beweging' }}">
		<link rel="canonical" href="{{ $page->url ?? '' }}">
		<link rel="stylesheet" href="{{ mix('/css/guest.css') }}" type="text/css" media="screen">
		<link rel="icon" href="/favicon.ico" sizes="any">
		<link rel="icon" href="/icon.svg" type="image/svg+xml">
		<link rel="apple-touch-icon" href="/apple-touch-icon.png">

		<meta name="twitter:card" content="summary_large_image">

		<meta property="og:title" content="EICAS - {{ $page->title ?? 'European Institute for Contemporary Art and Science' }}">
		<meta property="og:description" content="{{ $page->description ?? 'We maken kunst vanaf de jaren 60 toegankelijk. Vertrekpunt: de Nul-beweging.' }}">
		<meta property="og:type" content="website">
		<meta property="og:url" content="{{ $page->url ?? '' }}">
		<meta property="og:image" content="{{ $social_image_url ?? \App\Helpers\Misc::getCacheablePublicPath('/img/logo-eicas.png') }}">
		<meta property="og:image:width" content="{{ $social_image_width ?? '3761' }}">
		<meta property="og:image:height" content="{{ $social_image_height ?? '1590' }}">
		<meta property="og:image:alt" content="{{ $social_image_alt ?? 'EICAS' }}">
		<meta property="og:image:type" content="{{ $social_image_type ?? 'image/png' }}">
	</head>
	<body @yield('body-class')>
		@section('main-logo')
		<header id="main-logo">
			<a href="/">
				<img src="{{ \App\Helpers\Misc::getCacheablePublicPath('/img/logo-eicas.png') }}" alt="logo EICAS: European Institute for Contemporary Art and Science">
			</a>
		</header>
		@show
		@if ($errors->any())
		<div class="alert alert-danger">
			<h2>{{ trans_choice('general.error-present', count($errors)) }}</h2>
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		@if (session('status'))
		<div class="alert alert-success">
			<h2>{{ __('general.status-present') }}</h2>
			{{ session('status') }}
		</div>
		@endif
		@yield('main')
		@stack('bottom_scripts')
		@if ($errors->any() || session('status'))
		<script>{{--
--}}(function(){{{--
--}}Array.from(document.getElementsByClassName('alert')).forEach(function(a){{{--
--}}var b=document.createElement('div');{{--
--}}b.className='close-button';{{--
--}}b.addEventListener('click',function(e){e.target.parentElement.style.display='none';});{{--
--}}a.appendChild(b);{{--
--}}});})();{{--
--}}</script>
		@endif
		<script>
		var _paq=window._paq=window._paq||[];_paq.push(['trackPageView']);_paq.push(['enableLinkTracking']);
		(function() {
			var u="https://piwik.koetsierengineering.nl/";_paq.push(['setTrackerUrl',u+'matomo.php']);_paq.push(['setSiteId','26']);
			var d=document,g=d.createElement('script'),s=d.getElementsByTagName('script')[0];
			g.async=true;g.src=u+'matomo.js';s.parentNode.insertBefore(g,s);
		})();
		</script>
	</body>
</html>
