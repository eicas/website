<?php

// SPDX-FileCopyrightText: 2021 - 2022 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

return [

	/*
	|--------------------------------------------------------------------------
	| Password Reset Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	'reset' => 'Your password has been reset!',
	'sent' => 'We have emailed your password reset link!',
	'throttled' => 'Please wait before retrying.',
	'token' => 'This password reset token is invalid.',
	'user' => "We can't find a user with that email address.",

	'password-reset' => [
		'subject' => 'Reset Password Notification',
		'greeting' => 'Hello,',
		'introduction' => 'You are receiving this email because we received a password reset request for your account.',
		'link' => 'Reset Password',
		'description' => 'This password reset link will expire in :count minutes.',
		'disclaimer' => 'If you did not request a password reset, no further action is required.',
		'salutation' => 'Regards',
		'subcopy' => 'If you\'re having trouble clicking the ":actionText" button, copy and paste the URL below into your web browser:',
		'rights' => 'All rights reserved.',
	],
];
