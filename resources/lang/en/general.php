<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

return [
	'error-present' => 'An error occurred|Some errors occurred',
	'status-present' => 'System message',

	'Login' => 'Login',
	'Forgot password' => 'Forgot password',

	'update' => [
		'status' => [
			0 => 'draft',
			1 => 'under review',
			2 => 'published',
			3 => 'shared',
			'other' => 'unknown status',
		],
	],
];
