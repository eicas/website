<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

return [
	'admin' => [
		'name' => 'administrator',
		'description' => 'can perform all actions',
	],
	'user-admin' => [
		'name' => 'user administrator',
		'description' => 'can manage users',
	],
	'text-admin' => [
		'name' => 'editor',
		'description' => 'can work on website texts',
	],
	'menu-admin' => [
		'name' => 'menu administrator',
		'description' => 'can work on the main website menu',
	],
	'art-admin' => [
		'name' => 'art administrator',
		'description' => 'can manage the art images on the website',
	],
	'friend-admin' => [
		'name' => 'EICAS friend administrator',
		'description' => 'can manage the EICAS friends and payments',
	],
	'friend' => [
		'name' => 'EICAS friend',
		'description' => 'is a friend of EICAS',
	],
	'subscriber' => [
		'name' => 'EICAS newsletter subscriber',
		'description' => 'receives newsletters',
	],
];
