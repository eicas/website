<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

return [
	'admin' => [
		'name' => 'beheerder',
		'description' => 'kan alle acties uitvoeren',
	],
	'user-admin' => [
		'name' => 'gebruikersbeheerder',
		'description' => 'kan gebruikers beheren',
	],
	'text-admin' => [
		'name' => 'redacteur',
		'description' => 'kan de website-teksten beheren',
	],
	'menu-admin' => [
		'name' => 'menubeheerder',
		'description' => 'kan het hoofdmenu beheren',
	],
	'art-admin' => [
		'name' => 'kunstbeheerder',
		'description' => 'kan de kunst-beelden op de website beheren',
	],
	'friend-admin' => [
		'name' => 'EICAS vriendenbeheerder',
		'description' => 'kan de EICAS vrienden en donaties beheren',
	],
	'friend' => [
		'name' => 'EICAS vriend',
		'description' => 'is een vriend van EICAS',
	],
	'subscriber' => [
		'name' => 'EICAS nieuwsbriefontvanger',
		'description' => 'ontvangt nieuwsbrieven',
	],
];
