<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

/**
 * Add labels for the breadcrumbs line
 */

return [
	'cms' => 'CMS',
	'dashboard' => 'Dashboard',
	'index' => 'Overzicht',
	'create' => 'Nieuw',
	'edit' => 'Bewerken',
	'show' => 'Toon',

	'profiles' => 'Profielen' . (
		session()->has('user-admin.admin')
		? (session()->get('user-admin.admin') === 1 ? ' (intern)' : ' (vrienden)')
		: ''
	),
	'profile' => 'Profiel',
	'page' => 'Pagina',
	'pages' => 'Pagina\'s',
	'contents' => 'Teksten',
	'image' => 'Beeld',
	'images' => 'Beelden',
	'imageset' => 'Beeld-lijst',
	'imagesets' => 'Beeld-lijsten',
	'imageimageset' => 'Beeld-actie',
	'menuitems' => 'Menu',
	'users' => 'Gebruikers',
	'new_friends' => 'Vrienden (donaties)',
];
