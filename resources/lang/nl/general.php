<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

return [
	'error-present' => 'Er is een fout opgetreden|Er zijn fouten opgetreden',
	'status-present' => 'Systeembericht',

	'Login' => 'Inloggen',
	'Forgot password' => 'Wachtwoord vergeten',

	'update' => [
		'status' => [
			0 => 'klad',
			1 => 'ter review',
			2 => 'gepubliceerd',
			3 => 'gedeeld',
			'other' => 'onbekende status',
		],
	],
];
