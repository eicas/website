<?php

// SPDX-FileCopyrightText: 2021 Marten Koetsier <eicas@koetsierengineering.nl>
// SPDX-FileCopyrightText: 2022 Arnout Engelen <arnout@bzzt.net>
//
// SPDX-License-Identifier: MIT

/**
 * Note: these content types are also defined in \App\Models\Content
 */
return [
	'text' => [
		'label' => 'tekst',
		'description' => 'Opgemaakte tekst.',
	],
	'image' => [
		'label' => 'foto',
		'description' => 'Een foto of afbeelding (zie ook Dashboard > CMS > Afbeeldingen).',
	],
	'youtube' => [
		'label' => 'Youtube-video',
		'description' => 'Toon een video op de site m.b.v. Youtube.',
	],
	'bandcamp' => [
		'label' => 'Bandcamp-album',
		'description' => 'Link naar een album op Bandcamp.',
	],
	'lvp-tickets' => [
		'label' => 'tickets (LVP)',
		'description' => 'Toon de ticket-plugin van LVP',
	],
	'mc-newsletterform' => [
		'label' => 'nieuwsbrief-formulier (MailChimp)',
		'description' => 'Toon het formulier voor inschrijving voor de MailChimp nieuwsbrief.',
	],
	'form-new-friend' => [
		'label' => 'formulier: word vriend',
		'description' => 'Toon het formulier \'word vriend\', inclusief iDeal betaalopties.',
	],
	'shop-item' => [
		'label' => 'shop-item',
		'description' => 'Toon een product in de shop, met foto en vrije tekst.',
	],
];
