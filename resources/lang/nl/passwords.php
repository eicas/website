<?php

// SPDX-FileCopyrightText: 2021 - 2022 Marten Koetsier <eicas@koetsierengineering.nl>
//
// SPDX-License-Identifier: MIT

return [

	/*
	|--------------------------------------------------------------------------
	| Password Reset Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	'reset' => 'Het wachtwoord van uw account is gewijzigd.',
	'sent' => 'We hebben een e-mail verstuurd met instructies om een nieuw wachtwoord in te stellen.',
	'throttled' => 'Gelieve even te wachten voor u het opnieuw probeert.',
	'token' => 'Dit wachtwoordhersteltoken is niet geldig.',
	'user' => 'Geen gebruiker bekend met het e-mailadres.',

	'password-reset' => [
		'subject' => 'Een nieuw wachtwoord voor EICAS',
		'greeting' => 'Hallo,',
		'introduction' => 'U ontvangt dit bericht omdat we een verzoek hebben ontvangen voor een link om het wachtwoord voor de website aan te passen.',
		'link' => 'Stel het wachtwoord opnieuw in',
		'description' => 'Deze link is :count minuten geldig.',
		'disclaimer' => 'Als u geen link voor een nieuw wachtwoord heeft aangevraagd, kunt u dit bericht negeren.',
		'salutation' => 'Met vriendelijke groet',
		'subcopy' => 'Als u problemen ondervindt met het activeren van de ":actionText" knop, kopieer en plak dan onderstaande URL in uw web-browser:',
		'rights' => 'Alle rechten voorbehouden.',
	],
];
