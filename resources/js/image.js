// Polyfill for String.prototype.replaceAll
// This is necessary at least for Safari < 13.1
if (!String.prototype.replaceAll) {
	String.prototype.replaceAll = function (str, newStr) {
		if (Object.prototype.toString.call(str).toLowerCase() === '[object regexp]') {
			return this.replace(str, newStr);
		}
		return this.replace(new RegExp(str, 'g'), newStr);
	};
}
// The following javascript should be loaded once on a page where at least one image is present.
// It will make sure to replace the image preview with an appropriately sized image. Steps:
// - loop over all images with a class 'image-preview';
// - if the image is not visible: skip it and continue to next;
// - determine the width in pixels the preview image takes on the page;
// - determine which existing optimized image would be a good choice;
// - if the image is outside of the viewport, wait a bit (some random time between 0.3s and 1.5s);
// - replace the image by changing the src attribute by the relevant url.
(function() {
	// determine the viewport dimensions
	const vw = window.innerWidth || document.documentElement.clientWidth
	, vh = window.innerHeight || document.documentElement.clientHeight;
	// function d (delay): given HtmlElement e, return the delay time (ms) after which the image should load or -1 if
	// the image should not load at all.
	function d(e) {
		const r = e.getBoundingClientRect();
		// return -1 if there is no widht or height
		if (r.width <= 0 || r.height <= 0) return -1;
		return (r.top <= vh && r.bottom >= 0 && r.left <= vw && r.right >= 0)
		? 1
		: Math.floor(Math.random() * 1200 + 300);
	};
	// loop over all 'image-preview' elements, called 'p' in the function
	Array.from(document.getElementsByClassName('image-preview')).forEach(p => {
		// t: the wait time for an image before its optimal dimension version is loaded. If the image is (partly) in
		// view, the d function returns 1 (ms). If it is not in view, a wait time is returned. If the image is not
		// visible at all (display: none, bounding box with no surface area, etc.) the wait time is -1.
		const t = d(p);
		if (t < 1) return; // do not load
		// determine the width available to the image
		const w = p.parentElement.clientWidth
		// and a list of height-keyed widths available for this image
		, h = JSON.parse(p.dataset.imageHs.replaceAll("'",'"'))
		// and a sorted (small to large) list of heights
		, ws = Object.keys(h).sort(function(a,b){ return a-b; })
		// and the length of this list
		, l = ws.length;
		// calculate the optimal width and height and put the final image source in s
		let ow, oh, s;
		// starting from the lowest resolution, work up until the the available width is filled
		for (let i = 0; i < l; i++) {
			ow = ws[i];
			oh = h[ow];
			if (w < ow) {
				i = l + 1; // break
			}
		}
		// determine the url to the optimal image
		s=p.dataset.imagePt.replace('%w',ow).replace('%h',oh);
		// after the wait time, set the image source
		window.setTimeout((p,s)=>{p.src=s},t,p,s);
	});
})();
